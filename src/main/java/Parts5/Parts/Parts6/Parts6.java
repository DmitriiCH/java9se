package Parts5.Parts.Parts6;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class Parts6 {

    public static void main(String[] args) throws IOException {
//        BufferedReader in = null;
//        try {
//            in = Files.newBufferedReader(path, StandardCharsets.UTF_8);
//        } catch (IOException ex) {
//            System.err.println("Caught IOException: " + ex.getMessage());
//        } finally {
//            if (in != null) {
//                in.close(); // ВНИМАНИЕ: может быть сгенерировано исключение!
//            }
//        }
        /////////////////////////////////
        Path path = null;
        try (BufferedReader in = Files.newBufferedReader(path, StandardCharsets.UTF_8);) {
        } catch (IOException ex) {
            System.err.println("Caught IOException: " + ex.getMessage());
        }
        /////////////////////////////////
        BufferedReader in2 = null;
        Path path2 = null;
        try {
            in2 = Files.newBufferedReader(path2, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            System.err.println("Caught IOException: " + ex.getMessage());
        } finally {
            try {
                if (in2 != null) {
                    in2.close(); // ВНИМАНИЕ: может быть сгенерировано исключение!
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        /////////////////////////////////
        BufferedReader in3 = null;
        Path path3 = null;
        try {
            in3 = Files.newBufferedReader(path3, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            try {
                if (in3 != null) {
                    in3.close(); // ВНИМАНИЕ: может быть сгенерировано исключение!
                }
            } finally {
                System.err.println("Caught IOException: " + ex.getMessage());
            }
        }
    }
}
//В разделе 5.1.6 приведен пример ошибочного внедрения блока операторов catch и finally в блок кода оператора try.
// Исправьте этот код, во-первых, перехватив исключение в операторе finally, во-вторых, внедрив блок операторов try/finally в блок операторов try/catch, и
// в-третьих, применив оператор try с ресурсами вместе с оператором catch.