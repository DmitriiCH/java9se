package Parts5.Parts.Parts1;

import java.io.*;
import java.util.ArrayList;

public class Parts1 {

    public static void main(String[] args) throws Exception {
        ArrayList<Double> listValues = new Parts1().readValues("C:\\Users\\dmch1019\\Desktop\\Java\\javarush.txt");
    }

    public ArrayList<Double> readValues(String filename) throws Exception {
        ArrayList<Double> list = new ArrayList<>();
        try {
            File file = new File(filename);
            BufferedReader bf = new BufferedReader(new FileReader(file));
            while (bf.ready()) {
                double number = Double.parseDouble(bf.readLine());
                list.add(number);
            }
        } catch (NumberFormatException e) {
            throw new NumberFormatException();
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException();
        }
        return list;
    }
}
//Напишите метод public ArrayList<Double> readValues (String filename) throws ..., читающий числа с плавающей точкой из файла.
//Сгенерируйте подходящие исключения, если файл не удается открыть или же если введены данные, не относящиеся к числам с плавающей точкой.