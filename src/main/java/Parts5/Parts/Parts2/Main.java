package Parts5.Parts.Parts2;

import Parts5.Parts.Parts1.Parts1;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws Exception {
        System.out.println(new Main().sumOfValues("C:\\Users\\dmch1019\\Desktop\\Java\\javarush.txt"));
    }

    public double sumOfValues(String filename) throws Exception {
        ArrayList<Double> listValues = new Parts1().readValues(filename);
        double result = 0;
        for (double d : listValues) {
            result += d;
        }
        return result;
    }
}
//Напишите метод public double sumOfValues (String filename) throws . . ., вызывающий метод из предыдущего упражнения и возвращающий сумму значений в файле.
//Организуйте распространение любых исключений вызывающему коду.
