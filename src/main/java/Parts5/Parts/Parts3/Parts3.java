package Parts5.Parts.Parts3;

import Parts5.Parts.Parts2.Main;

import java.io.FileNotFoundException;

public class Parts3 {

    public static void main(String[] args) {
        printSumOfValues();
    }

    public static void printSumOfValues() {
        try {
            System.out.println(new Main().sumOfValues("C:\\Users\\dmch1019\\Desktop\\Java\\javarush.txt"));
        } catch (FileNotFoundException e) {
            System.out.println("Ошибка файл не существует");
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println("Ошибка в файле содеражатся заначения отличные от double");
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Неизвестная ошибка");
            e.printStackTrace();
        }
    }
}
//Напишите программу, вызывающую метод из предыдущего упражнения и выводящую полученный результат.
// Организуйте перехват исключений и предоставьте ответную реакцию на действия пользователя в виде сообщений о любых ошибочных условиях.