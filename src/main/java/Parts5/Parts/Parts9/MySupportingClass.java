package Parts5.Parts.Parts9;

import java.util.concurrent.locks.ReentrantLock;

public class MySupportingClass implements AutoCloseable {

    private ReentrantLock lock;

    public MySupportingClass(ReentrantLock lock) {
        this.lock = lock;
    }

    @Override
    public void close() {
        lock.unlock();
    }
}
