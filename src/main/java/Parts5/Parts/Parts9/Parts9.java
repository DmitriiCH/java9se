package Parts5.Parts.Parts9;

import java.io.*;
import java.util.concurrent.locks.ReentrantLock;

/*
Разработайте вспомогательный метод, чтобы воспользоваться классом ReentrantLook в операторе try
с ресурсами. Вызовите метод lock() и возвратите объект класса, который реализует интерфейс AutoCloseable
и в методе которого close() вызывается метод unlock(), но не генерируется никаких исключений.
 */
public class Parts9 {
    private final static ReentrantLock lock = new ReentrantLock();

    public static void main(String[] args) {
        Thread thread1 = new Thread(Parts9::myMethod);
        thread1.start();
        Thread thread2 = new Thread(Parts9::myMethod);
        thread2.start();
    }

    public static MySupportingClass myMethod() {
        lock.lock();
        try (MySupportingClass myClass = new MySupportingClass(lock)) {
            for (int i = 0; i < 5; i++) {
                System.out.printf("%s { i=%s }\n", Thread.currentThread().getName(), i);
            }
            return myClass;
        }
    }
}
