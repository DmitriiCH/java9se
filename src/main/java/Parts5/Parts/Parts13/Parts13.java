package Parts5.Parts.Parts13;

import java.util.Arrays;

public class Parts13 {

    public static void main(String[] args) {
        int[] ints = {1, 2, 3, -12, 1000, -1000};
       rezultTime(ints); // Первая часть задания
        ////////////
        int[] ints2 = intInits(200000000); //Вторая часть задания
        rezultTime(ints2);
    }

    public static int min(int[] values) {
        int min = Arrays.stream(values).min().getAsInt();
        assert Arrays.stream(values).allMatch(x -> x >= min);
        return min;
    }

    public static void rezultTime(int[] ints) {
        Long startTime = System.currentTimeMillis();
        int x = min(ints);
        Long endTime = System.currentTimeMillis();
        Long rezult = endTime - startTime;
        System.out.printf("Время выполнения: %s, минимальное число %s\n", rezult, x);
    }

    public static int[] intInits(int size) {
        int[] ints = new int[size];
        for (int i = 0; i < ints.length; i++) {
            ints[i] = i;
        }
        return ints;
    }
}
/*
Напишите метод int min(int[] values), в котором перед возвратом наименьшего
значения утверждается, что оно действительно не больше всех остальных
значений в массиве. Воспользуйтесь вспомогательным методом Stream().allMatch,
если вы уже просматривали главу 8. Организуйте повторный вызов данного метода
для обработки крупного массива и определите время выполнения кода при разрешении,
запрете и исключении утверждений.

Для разрешения или запрета утверждений я использовал в параметре запуска строку
-ea. Для исключения - коментировал соответствующую строчку кода. Соответственно если я
запрещаю или исключаю утверждения, время выполнения уменьшается.
 */