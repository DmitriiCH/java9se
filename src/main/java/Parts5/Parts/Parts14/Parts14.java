package Parts5.Parts.Parts14;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Filter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Parts14 {

    public static void main(String[] args) throws IOException {
        Logger logger = getMyLogger("my.logger");
        setMyFilter(logger, "секс", "наркотики", "C++");
        logger.finer("секс");
        logger.finer("наркотики");
        logger.finer("C++");
        logger.finer("наркотssdsdики");
    }

    public static void setMyFilter(Logger logger, String... filterStr) {
        Filter filter = (logRecord) -> {
            String record = logRecord.getMessage();
            for (String str : filterStr) {
                if (record.contains(str)) return false;
            }
            return true;
        };
        logger.setFilter(filter);
    }

    public static Logger getMyLogger(String nameLogger) {
        Logger logger = Logger.getLogger(nameLogger);
        logger.setLevel(Level.ALL);
        logger.setUseParentHandlers(false);
        ConsoleHandler handler = new ConsoleHandler();
        handler.setLevel(Level.ALL);
        logger.addHandler(handler);
        return logger;
    }
}
//Реализуйте и испытайте фильтр протокольных записей, содержащий такие неприличные слова, как секс, наркотики и C++.