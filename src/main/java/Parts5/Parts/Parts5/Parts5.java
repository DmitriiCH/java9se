package Parts5.Parts.Parts5;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Parts5 {

    public static void main(String[] args) {
        writeToFile("C:\\Users\\dmch1019\\Desktop\\Java\\file.txt");
    }

    public static void writeToFile(String fileName) {
        Scanner scanner = null;
        PrintWriter printWriter = null;
        try {
            scanner = new Scanner(System.in);
            printWriter = new PrintWriter(fileName);
            while (scanner.hasNext()) {
                printWriter.println(scanner.next().toLowerCase());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                scanner.close();
                printWriter.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
//Реализуйте метод, содержащий код, где применяются классы Scanner и Print Writer (см. раздел 5.1.5). Но вместо оператора try с ресурсами воспользуйтесь
// оператором catch. Непременно закройте оба объекта, при условии, что они построены надлежащим образом. Для этого вам придется принять во внимание следующие
// условия.
//• Конструктор класса Scanner генерирует исключение.
//• Конструктор класса PrintWriter генерирует исключение.
//• Метод hasNext (), next () или println () генерирует исключение.
//• Метод out. close () генерирует исключение.
//• Метод in. close () генерирует исключение.