package Parts5.Parts.Parts4;

import Parts5.Parts.Parts2.Main;

public class Parts4 {

    public static void main(String[] args) throws Exception {
        printSumOfValues();
    }

    public static void printSumOfValues() throws Exception {
        System.out.println(new Main().sumOfValues("C:\\Users\\dmch1019\\Desktop\\Java\\javarush.txt"));
    }
}
//Повторите предыдущее упражнение, но на этот раз не пользуйтесь исключениями.
// Вместо этого организуйте возврат кодов ошибок из методов readValues () и sumOfValues().