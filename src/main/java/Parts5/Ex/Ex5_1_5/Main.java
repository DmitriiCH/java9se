package Parts5.Ex.Ex5_1_5;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
//        ArrayList<String> lines = new ArrayList<>();
//        PrintWriter out = new PrintWriter("output.txt");
//        for (String line : lines) {
//            out.println(line.toLowerCase());
//        }
//        out.close();
        /////////////////////////////////////////
        ArrayList<String> lines = new ArrayList<>();
        try (PrintWriter out = new PrintWriter("output.txt")) {
            for (String line : lines) {
                out.println(line.toLowerCase());
            }
        }
        /////////////////////////////////////////
        try (Scanner in = new Scanner(Paths.get("/usr/share/sict/world"));
             PrintWriter out = new PrintWriter("output.txt")) {
            while (in.hasNext()) {
                out.println(in.next().toLowerCase());
            }
        } catch (IOException ex) {
            Throwable[] secondaryExceptions = ex.getSuppressed();
        }
    }
}
