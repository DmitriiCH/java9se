package Parts5.Ex.Ex5_1_2;

import java.io.IOException;

public class FileFormatException extends IOException {

    public FileFormatException() {
    }

    public FileFormatException(String message) {
        super(message);
    }
}
