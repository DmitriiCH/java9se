package Parts5.Ex.Ex5_3_3;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {
        Logger logger = Logger.getGlobal();
        logger.setLevel(Level.FINE);
        ///
        logger.setLevel(Level.ALL);
        logger.setLevel(Level.OFF);
        ///
        logger.warning("message");
        logger.fine("message");
        ///
        Level level = Level.INFO;
        logger.log(level,"message");
//        logger.entering();
//        logger.exiting();
    }
}
