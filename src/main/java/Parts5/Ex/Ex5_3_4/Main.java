package Parts5.Ex.Ex5_3_4;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static final Logger logger = Logger.getGlobal();

    public static void main(String[] args) {
        logger.log(Level.INFO,"message");
        logger.info("message");
    }
}
