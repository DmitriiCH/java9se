package Parts5.Ex.Ex5_1_4;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
            //операторы
        } catch (ArrayIndexOutOfBoundsException e) {
            //обработчик исключения
            e.printStackTrace();
        } catch (ArrayStoreException e) {
            //обработчик исключения
            e.printStackTrace();
        } catch (ArithmeticException e) {
            //обработчик исключения
            e.printStackTrace();
        }
        /////////////////////////////////////
        try {
            //операторы
        } catch (ArrayIndexOutOfBoundsException | ArrayStoreException | ArithmeticException e) {
            //обработчик исключения
            e.printStackTrace();
        }
    }
}
