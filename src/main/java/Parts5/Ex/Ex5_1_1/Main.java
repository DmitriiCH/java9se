package Parts5.Ex.Ex5_1_1;

import java.util.Random;

public class Main {

    public static Random generator = new Random();

    public static void main(String[] args) {
        System.out.println(randInt(10, 5));
    }

    public static int randInt(int low, int high) {
        if (low > high) {
            throw new IllegalArgumentException(String.format("low should be <= high low is %d and high  is %d", low, high));
        }
        return low + (int) (generator.nextDouble() * (high - low + 1));
    }
}
