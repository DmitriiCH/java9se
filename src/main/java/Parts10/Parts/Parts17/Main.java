package Parts10.Parts.Parts17;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/*
Напишите программу для обхода дерева каталогов с формированием отдельного потока
исполнения для каждого файла. Подсчитайте в потоках исполнения кол-во слов в
файлах и, не прибегая к блокировкам, обновите общий счетчик, который объявляется
следующим образом:

public static Long count = 0;

Выполните эту программу неоднократно. Что при этом происходит и почему?

При выполнении этой программы происходит состояние гонок - каждый поток перезатирает
операцию сложения, в результате выводится разный результат.
 */
public class Main {

    public static Long count = 0L;

    public static void main(String[] args) throws IOException, InterruptedException {
        List<Path> paths = Files.walk(Paths.get("src\\main\\java\\Parts10\\Parts\\Parts3\\Folder")).filter(f -> f.toFile().isFile()).collect(Collectors.toList());
        for (Path p : paths) {
            new Thread(getRunnable(p.toFile())).start();
        }
        Thread.sleep(1000);
        System.out.println(count);
    }

    private static Runnable getRunnable (File file) {
        Runnable run = () -> {
            try {
                int x = 0;
                List<String> list = Files.readAllLines(file.toPath());
                for (String s: list) {
                    String[] strings = s.split("\\PL+");
                    x += strings.length;
                }
                count += x;
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
        return run;
    }
}
