package Parts10.Parts.Parts28;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/*
Метод

static CompletableFuture<Void> CompletableFuture.allOf(CompletableFuture<?>... cfs )

не выдает никаких результатов обработки его аргументов, что затрудняет его применение.
Реализуйте приведенный ниже метод, где объединяются однотипные будущие действия.

static <T> CompletableFuture<List<T>> allOf(List<CompletableFuture<T>> cfs)

Имейте в виду, что у этого метода имеется параметр типа List, поскольку переменные аргументы
не могут относиться к обобщенному типу
 */
public class Main {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CompletableFuture<Integer> com = new CompletableFuture<>();
        //com.completeAsync(() -> 1);
        CompletableFuture<Integer> com2 = new CompletableFuture<>();
        //com2.completeAsync(() -> 2);
        List<CompletableFuture<Integer>> list = new ArrayList<>();
        list.add(com);
        list.add(com2);
        CompletableFuture<List<Integer>> res = allOf(list);
    }

    static <T> CompletableFuture<List<T>> allOf(List<CompletableFuture<T>> cfs) throws ExecutionException, InterruptedException {
        CompletableFuture<List<T>> res = new CompletableFuture<>();
        List<T> list = new ArrayList<>();
        for (CompletableFuture<T> c : cfs) {
            list.add(c.get());
        }
        res.obtrudeValue(list);
        return res;
    }

}

