package Parts10.Parts.Parts22;

/*
Найдите ошибку в следующем фрагменте кода:

public class Stack {
    private Object myLock = "LOCK";

    public void push (Object newValue) {
        synchronized (myLock) {
        ...
        }
    }
    ...
}

В данном примере кода, синхронизация отрабатывает, т.к. объект хранится внутри
экземпляра класса Stack и  все тело метода будет храниться в блоке
synchronized (myLock) {
        ...
}
Но целесообразней не использовать такой объект, а объявить метод
public synchronized void push (Object newValue) {...}

Пример использования в классе Main.
 */
public class Stack {

    class Node {
        Object value;
        Node next;
    }

    private int size = 0;

    private Node top;

    private Object myLock = "LOCK";

    public void push(Object newValue) {
        synchronized (myLock) {
            Node n = new Node();
            n.value = newValue;
            n.next = top;
            top = n;
            size++;
        }
    }

    public int getSize() {
        return size;
    }
}
