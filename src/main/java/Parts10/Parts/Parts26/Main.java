package Parts10.Parts.Parts26;


import java.net.PasswordAuthentication;
import java.util.Scanner;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;
import java.util.function.Supplier;

/*
Напишите следующий метод:

    public static <T> CompletableFuture<T> repeat (Supplier<T> action, Predicate<T> until) {

    }

Этот метод должен асинхронно повторять заданное действие до тех пор, пока не будет получено значение,
принимаемое функцией until(), которая также должна выполняться асихронно. Проверьте этот метод с
помощью одной функции, вводящей объект типа java.net.PasswordAuthentication с консоли, и другой функции,
имитирующей проверку достоверности, ожидая в течение секунды и затем проверяя пароль "secret".
Подсказка: воспользуйтесь рекурсией.

    public static <T> CompletableFuture<T> repeat (Supplier<T> action, Predicate<T> until) {

    }

 */
public class Main {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        repeat(Main::getUserAndPassword, (t) -> new String(t.getPassword()).equals("secret"));
    }

    public static <T> CompletableFuture<T> repeat(Supplier<T> action, Predicate<T> until) throws ExecutionException, InterruptedException {
        CompletableFuture<T> comp = new CompletableFuture<>();
        comp = comp.completeAsync(action);
        if (!comp.thenApplyAsync(until::test).get()) {
            System.out.println("Неверный логин или пароль");
            repeat(action, until);
        } else System.out.println("Пароль - верный");
        return comp;
    }

    private static PasswordAuthentication getUserAndPassword () {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите логин: ");
        String login = scan.nextLine();
        System.out.println("Введите пароль: ");
        String password = scan.nextLine();
        return new PasswordAuthentication(login, password.toCharArray());
    }
}

