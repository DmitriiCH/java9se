package Parts10.Parts.Parts9;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;

/*
Сформируйте 1000 потоков исполнения, в которых счетчик инкрементируется 100000 раз.
Сравните производительность при использовании классов AtomicLong и longAdder.

LongAdder выполняет операции инкрементирования производительнее, чем AtomicLong.
 */
public class Main {

    public static void main(String[] args) {
        AtomicLong atomicLong = new AtomicLong();
        LongAdder longAdder = new LongAdder();

        System.out.println("AtomicLong: " + getTime(atomicLong));
        System.out.println("LongAdder: " + getTime(longAdder));
    }

    public static Long getTime(AtomicLong atomicLong) {
        Long startTime = System.currentTimeMillis();
        executeThreads(1000, increments(100000, atomicLong));
        Long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }

    public static Runnable increments(int count, AtomicLong atomicLong) {
        Runnable run = () -> {
            for (int i = 0; i < count; i++) {
                atomicLong.incrementAndGet();
            }
        };
        return run;
    }

    public static Long getTime(LongAdder longAdder) {
        Long startTime = System.currentTimeMillis();
        executeThreads(1000, increments(100000, longAdder));
        Long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }

    public static Runnable increments(int count, LongAdder longAdder) {
        Runnable run = () -> {
            for (int i = 0; i < count; i++) {
                longAdder.increment();
            }
        };
        return run;
    }

    public static void executeThreads(int count, Runnable run) {
        for (int i = 0; i < count; i++) {
            new Thread(run).start();
        }
    }
}
