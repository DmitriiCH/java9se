package Parts10.Parts.Parts10;

import java.util.concurrent.atomic.LongAccumulator;
import java.util.function.LongBinaryOperator;

/*
Воспользуйтесь классом LongAccumulator для вычисления максимального и минимального
накапливаемых элементов.

LongAccumulator в конструкторе принимает дефолное значение identity, которое так же
берется в расчет при использовании функционального интрефейса LongBinaryOperator.
 */
public class Main {
    public static void main(String[] args) {
        LongAccumulator accumulator = new LongAccumulator(myLongBinaryOperatorMaxValue(), 0);
        Long[] longs = {100L, 10000L, 1L, 1000000L};
        for (Long x : longs) {
            accumulator.accumulate(x);
        }
        System.out.println(accumulator.longValue());
    }


    public static LongBinaryOperator myLongBinaryOperatorMinValue() {
        return (L, R) -> {
            if (L > R) {
                return R;
            } else return L;
        };
    }

    public static LongBinaryOperator myLongBinaryOperatorMaxValue() {
        return (L, R) -> {
            if (L < R) {
                return R;
            } else return L;
        };
    }
}
