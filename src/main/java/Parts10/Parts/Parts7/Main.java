package Parts10.Parts.Parts7;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/*
Повторите предыдущее упражнение, но на этот раз воспользуйтесь методом
computeIfAbsent(). В чем преимущество такого подхода?

computeIfAbsent() - действует следующим образом, если в HashMap имеется ключ, то
ничего не происходит, если ключа нет, то выполняется функциональный интерфейс.

Метод есть смысл использовать если нужно найти каждое уникальное слово в файле.
Поэтому смысла использовать метод computeIfAbsent(), т.к. ключи задаются изначально при поиске
слов.
 */
public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        ExecutorService exec = Executors.newFixedThreadPool(3); // создаем пул с фиксированным количеством потоков исполнения
        ConcurrentHashMap<String, Set<File>> map = new ConcurrentHashMap<>();
        Set<File> files = getFiles(Paths.get("src\\main\\java\\Parts10\\Parts\\Parts7\\Folder"));
        map.put("saas", files);
        map.put("zaas", files);
        for (Map.Entry<String, Set<File>> pair : map.entrySet()) {
            exec.execute(getRunnable(pair.getKey(), map));
//            getRunnable(pair.getKey(), map).run();
        }
        Thread.sleep(1000);
        map.forEach((K, V) -> V.forEach(System.out::println));
        exec.shutdown();
    }

    public static Runnable getRunnable(String word, ConcurrentHashMap<String, Set<File>> map) throws IOException {
        Runnable run = () -> map.merge(word, new HashSet<>(), (set1, set2) -> {
            Set<File> files = getNewSet(set1, word);
            return files;
        });
        return run;
    }

    private static Set<File> getFiles(Path root) throws IOException {
        return Files.walk(root).filter(f -> f.toFile().isFile()).map(Path::toFile).collect(Collectors.toSet());
        // возвращаем все файлы из рутовой папки
    }

        // модифицируем множество
    private static Set<File> getNewSet(Set<File> set, String word) {
        Set<File> result = set.stream().filter(f -> {
            try {
                return Files.readAllLines(f.toPath()).contains(word);

            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }).collect(Collectors.toSet());
        return result;
    }
}

