package Parts10.Parts.Parts20;

/*
Рассмотрите следующую реализацию стека:

Опишите два разных пути, приводящих к тому, что эта структура данных
может содержать неверные элементы.
 */
public class Stack {

    class Node {

        Object value;
        Node next;
    }

    private Node top;

    public synchronized void push(Object newValue) {
        Node n = new Node();
        n.value = newValue;
        n.next = top;
        top = n;
    }

    public Object pop() {
        if (top == null) return null;
        Node n = top;
        top = n.next;
        return n.value;
    }
}
