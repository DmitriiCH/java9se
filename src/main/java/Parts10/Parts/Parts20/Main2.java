package Parts10.Parts.Parts20;

/*
Вторым путь, на мой взгляд заключается в том, что стек может содержать разные объекты,
т.е. разные потоки могу добавлять в него разные объекты
*/
public class Main2 {
    public static void main(String[] args) throws InterruptedException {
        Stack stack = new Stack();
        pushInStack(stack);
        Thread.sleep(1000);
        for (int i = 0; i < 100; i++) {
            System.out.println(stack.pop());
        }
    }

    private static void pushInStack(Stack stack) throws InterruptedException {
        Thread thread1 = new Thread(run1(stack));
        Thread thread2 = new Thread(run2(stack));
        thread1.start();
        thread2.start();
    }

    private static Runnable run2(Stack stack) {
        Runnable run = () -> {
            for (int i = 50; i < 101; i++) {
                stack.push(i + " run2");
            }
        };
        return run;
    }

    private static Runnable run1(Stack stack) {
        Runnable run = () -> {
            for (int i = 0; i < 50; i++) {
                stack.push(i);
            }
        };
        return run;
    }
}
