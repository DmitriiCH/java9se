package Parts10.Parts.Parts20;

import java.util.IdentityHashMap;

/*
Первый путь заключается в том что, при добавлении элементов, я использую 2 потока.
В результате при выполнении метода push, класса Stack, оба потока заходят в него
и перезатирают данные. В следствии это объекты могут потеряться. Выход - объявить
метод push синхронизированным.
 */
public class Main1 {
    public static void main(String[] args) throws InterruptedException {
        Stack stack = new Stack();
        pushInStack(stack);
        for (int i = 0; i < 100; i++) {
            System.out.println(stack.pop());
        }
    }

    private static void pushInStack(Stack stack) throws InterruptedException {
        Thread thread1 = new Thread(run1(stack));
        Thread thread2 = new Thread(run2(stack));

        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
    }

    private static Runnable run2(Stack stack) {
        Runnable run = () -> {
            for (int i = 50; i < 101; i++) {
                stack.push(i + " run2");
            }
        };
        return run;
    }

    private static Runnable run1(Stack stack) {
        Runnable run = () -> {
            for (int i = 0; i < 50; i++) {
                stack.push(i + " run1");
            }
        };
        return run;
    }
}
