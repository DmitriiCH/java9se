package Parts10.Parts.Parts8;

import java.util.Map;
import java.util.concurrent.*;
import java.util.function.BiFunction;

/*
Найдите в отображении типа ConcurrentHashMap<String, Long> ключ
с максимальным значением, произвольно отбрасывая лишнее. Подсказска:
воспользуйтесь методом reduceEntries().

reduceEntries() - возвращает результат сбора всех записей с использованием указанной функции
 */
public class Main {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ConcurrentHashMap<String, Long> map = new ConcurrentHashMap<>();
        map.put("один", 1L);
        map.put("два", 2L);
        map.put("три", 3L);
        map.put("четыре", 10L);
        map.put("пять", 0L);
        map.put("шесть", 6L);
        ExecutorService exec = Executors.newFixedThreadPool(3);
        Future result = exec.submit(getRunnable(map));
        Long x = ((Map.Entry<String, Long>) result.get()).getValue();
        exec.shutdown();
        System.out.println("Key: " + ((Map.Entry<String, Long>) result.get()).getKey() + ", Value: " + x);
    }

    private static Callable getRunnable(ConcurrentHashMap<String, Long> map) {
        Callable run = () -> {
            Map.Entry<String, Long> entry = null;
            for (Map.Entry<String, Long> entries : map.entrySet()) {
                entry = map.reduceEntries(3, getMyBiFunction());
            }
            return entry;
        };
        return run;
    }

    private static BiFunction<Map.Entry<String, Long>, Map.Entry<String, Long>, Map.Entry<String, Long>>
    getMyBiFunction() {
        BiFunction<Map.Entry<String, Long>, Map.Entry<String, Long>, Map.Entry<String, Long>> function =
                (K, V) -> {
                    if (K.getValue() < V.getValue()) {
                        return V;
                    }
                    return K;
                };
        return function;
    }
}
