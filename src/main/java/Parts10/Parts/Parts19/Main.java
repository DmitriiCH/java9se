package Parts10.Parts.Parts19;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.LongAdder;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/*
Усовершенствуйте программу из предыдущего упражнения, используя
класс LongAdder.
 */
public class Main {
    public static LongAdder count = new LongAdder();

    public static void main(String[] args) throws IOException, InterruptedException {
        List<Path> paths = Files.walk(Paths.get("src\\main\\java\\Parts10\\Parts\\Parts3\\Folder")).filter(f -> f.toFile().isFile()).collect(Collectors.toList());
        Lock countLock = new ReentrantLock();
        for (Path p : paths) {
            new Thread(getRunnable(p.toFile(), countLock)).start();
        }
        Thread.sleep(1000);
        System.out.println(count);
    }

    private static Runnable getRunnable (File file, Lock lock) {
        Runnable run = () -> {
            int x = 0;
            try {
                List<String> list = Files.readAllLines(file.toPath());
                for (String s: list) {
                    x += s.split("\\s").length;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            lock.lock();
            try {
                count.add(x);
            }finally {
                lock.unlock();
            }
        };
        return run;
    }
}
