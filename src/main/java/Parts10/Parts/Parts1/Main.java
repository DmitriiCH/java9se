package Parts10.Parts.Parts1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/*
Используя параллельные потоки данных, найдите в каталоге все файлы, содержащие заданное
слово. Как найти только первый файл? Действительно ли поиск файлов осуществляется парал-
лельно?

Если нужно найти только первый файл, то достаточно выставить limit следующим образом:
Files.walk(dir).parallel().filter(f -> containsWord(word, f)).limit(1).map(f -> f.getFileName().toString()).collect(Collectors.toList());

Поиск файлов осуществляется параллельно, так как вызван метод parallel(), который разбивает поток на части, выполня-
ющиеся в параллельных потоках, в каждом выполняется поиск, и затем подсчитывается результат.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        fileNames("C:\\Users\\dmch1019\\IdeaProjects\\Java9SE\\src\\main\\java\\Parts10\\Parts\\Parts1\\Test", "netcracker").forEach(System.out::println);
    }

    public static List<String> fileNames(String directory, String word) throws IOException {
        Path path = Paths.get(directory);
        return Files.walk(path).parallel().filter(f -> containsWord(word, f)).map(f -> f.getFileName().toString()).collect(Collectors.toList());
    }

    private static boolean containsWord(String word, Path path) {
        try {
            return (path.toFile().isFile()) ? Files.readAllLines(path).contains(word) : false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}