package Parts10.Parts.Parts24;

import java.util.Arrays;

/*
Найдите ошибку в следующем фрагменте кода:

public class Stack {
    private Object[] values = new Object[10];
    private int size;

    public void push(Object newValue) {
        synchronized (values) {
            if (size == values.length) {
                values = Arrays.copyOf(values, 2 * size);
            }
            values[size] = newValue;
            size++;
        }
    }
    ...
}

В данном примере кода, синхронизация отрабатывает, т.к. объект values хранится внутри
экземпляра класса Stack и при обращении к нему блокируется экземпляр класса Stack
для других потоков. Все тело метода должно храниться в блоке

synchronized (myLock) {
        ...
}

Но целесообразней не использовать такой объект, а объявить метод как:
public synchronized void push(Object newValue) {...}
 */
public class Stack {
    private Object[] values = new Object[10];
    private int size;

    public void push(Object newValue) {
        synchronized (values) {
            if (size == values.length) {
                values = Arrays.copyOf(values, 2 * size);
            }
            values[size] = newValue;
            size++;
        }
    }

    public int getSize() {
        return size;
    }
}
