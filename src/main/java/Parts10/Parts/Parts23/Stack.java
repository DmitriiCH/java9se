package Parts10.Parts.Parts23;

import java.util.concurrent.locks.ReentrantLock;

/*
Найдите ошибку в следующем фрагменте кода:

public class Stack {
    public void push (Object newValue) {
        synchronized (new ReentrantLock()) {
            ...
        }
    }
    ...
}

В данном примере ошибка заключается в том что, синхронизация происходиит по вновь создаваемому
объекту, в результате чего синхронизации как таковой нет и все объекты могут зайти внутрь этого
блока. Что демонстрируется в классе Main.

 */
public class Stack {
    class Node {
        Object value;
        Node next;
    }

    private int size = 0;

    private Node top;

    public void push(Object newValue) {
        synchronized (new ReentrantLock()) {
            Node n = new Node();
            n.value = newValue;
            n.next = top;
            top = n;
            size++;
        }
    }

    public int getSize() {
        return size;
    }
}
