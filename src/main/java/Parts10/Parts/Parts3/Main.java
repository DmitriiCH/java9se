package Parts10.Parts.Parts3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/*
Реализуйте метод, возвращающий задачу для чтения всех слов из файла с целью
найти в нем заданное слово. Если задача прерывается, она должна быть завершена немедленно
с выдачей отладочного сообщения. Запланируйте выполнение этой задачи для каждого файла в каталоге.
Как только одна из задач завершится успешно, все остальные задачи должны быть немедленно прер-
ваны.
 */
public class Main {
    public static void main(String[] args) throws InterruptedException, ExecutionException, IOException {
        System.out.println(method(Paths.get("src\\main\\java\\Parts10\\Parts\\Parts3\\Folder"), "word"));
    }

    public static String method(Path start, String word) throws IOException, ExecutionException, InterruptedException {
        List<Path> files = Files.walk(start).filter(f -> f.toFile().isFile()).collect(Collectors.toList());
        // формируем список файлов в каталоге, подкаталоги отфильтруются.
        ExecutorService exec = Executors.newFixedThreadPool(files.size());// создаем пул с фиксированным количеством потоков исполнения
        String result = exec.invokeAny(searchWord(files, word));
        // выполняем для каждого файла Callable, пока не будет найден первый удачный результат
        exec.shutdown();
        return result;
    }

    public static List<Callable<String>> searchWord(List<Path> paths, String word) throws IOException {
        List<Callable<String>> list = new ArrayList<>();
        for (Path path : paths) {
            Callable<String> callable = () -> {
                List<String> temp = Files.readAllLines(path);
                for (String s : temp) {
                    if (s.contains(word)) {
                        return word + " " + path.getFileName();
                    } else throw new IOException();
                    /* если слово не найдено прогружаем ошибку,
                    т.к. нам важен первый удачный результат
                    */
                }
                return "";
            };
            list.add(callable);
        }
        return list;
    }
}
