package Parts7.Ex.Ex7_3;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {
        Set<String> badWords = new HashSet<>();
        badWords.add("sex");
        badWords.add("drugs");
        badWords.add("c++");
        if (badWords.contains("sexb".toLowerCase())) {
            System.out.println("Please choose a different user name");
        }
        printAll(badWords);
        ///////////////////////////////
        Set<Integer> set = new TreeSet<>();
        set.add(1);
        set.add(0);
        set.add(6);
        printAll(set);
    }

    public static <T> void printAll(Collection<T> coll) {
        for (T e : coll) {
            System.out.println(e);
        }
    }
}
