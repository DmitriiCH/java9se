package Parts7.Ex.Ex7_6_2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<String> sentence = new ArrayList<>(Arrays.asList("Первый", "Второй", "Третий", "Четвертый", "Пятый", "Шестой", "Седьмой"));
        List<String> subListSentence = sentence.subList(1,5);
        System.out.println(subListSentence);
    }
}
