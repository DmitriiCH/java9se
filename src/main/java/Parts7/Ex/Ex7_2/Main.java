package Parts7.Ex.Ex7_2;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Main {

    public static void main(String[] args) {
        List<String> friends = new LinkedList<>();
        ListIterator<String> iter = friends.listIterator();
        iter.add("Fred");
        iter.add("Wilma");
        iter.previous();
        iter.set("Barney");
//        for (String s : friends) {
//            System.out.println(s);
//        }
        printAll(friends);
    }

    public static <T> void printAll(List<T> list) {
        Iterator<T> iter = list.iterator();
        while (iter.hasNext()){
            System.out.println(iter.next());
        }
    }
}
