package Parts7.Ex.Ex7_4;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Map<String, Integer> counts = new HashMap<>();
        counts.put("Alice", 1);
        counts.put("Alice", 2);
        ///
        int count = counts.get("Alice");
        System.out.println(count);
        count = counts.getOrDefault("Alice", 0);
        System.out.println(count);
        for (Map.Entry<String, Integer> entry : counts.entrySet()             ) {
            String key = entry.getKey();
            Integer value = entry.getValue();
        }
    }
}
