package Parts7.Ex.Ex7_6_3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Person {

    private ArrayList<Person> friends;

    public List<Person> getFriends() {
        return Collections.unmodifiableList(friends);
    }
}

class Main {

    public static void main(String[] args) {
        Person person = new Person();
        List<Person> friends = person.getFriends();
        friends.add(new Person());
    }
}