package Parts7.Parts.Parts18;

import java.util.Collections;
import java.util.List;

/*
В классе Collections имеются статические константы EMPTY_LIST, EMPTY_MAP и
EMPTY_SET. Почему они не так полезны, как методы emptyList(), emptyMap() и
emptySet()?

Данные константы и методы предназначены для возврата пустой коллекции, которую
нельзя изменить, что либо в нее добавить. Это нужно, что бы избавиться от
лишних NullPointerException т.е. возвращать не null, а эту коллекцию. При попытке
добавить в нее элемент получим UnsupportedOperationException.
Данные константы не так полезны. Они выбрасывают предупреждение Unchecked assignment,
т.к. они являются потомками AbstractList, AbstractMap и AbstractSet и имеют внутри себя
общий тип Е, который является любым объектом. Collections.emptyList() в свою очередь
имеет такую реализацию:

    public static final <T> List<T> emptyList() {
        return (List<T>) EMPTY_LIST;
    }

 */
public class Main {
    public static void main(String[] args) {
        List<String> listConstant = Collections.EMPTY_LIST;
        listConstant.add("1");
        List listConstant2 = listConstant;
        listConstant2.add(1);

        List<String> listConstant3 =Collections.emptyList();
        listConstant3.add("1");
        List listConstant4 = listConstant3;
        listConstant4.add(1);


    }
}
