package Parts7.Parts.Parts16;

import java.util.*;
import java.util.function.IntFunction;

/*
Усовершенствуйте реализацию из предыдущего упражнения,
организовав кеширование последних 100 значений вычисленых функцией.
*/
public class Main {

    private Set<Integer> hash = new HashSet<>();

    public static void main(String[] args) {
        Main main = new Main();
        List<Integer> list = main.returnUnmodifiableList(10, (i) -> i * 2);
        System.out.println(list.size());
        list.add(1);
        main.hash.forEach((s) -> System.out.println(s));
    }

    public List<Integer> returnUnmodifiableList(int index, IntFunction<Integer> function) {
        List<Integer> list = new ArrayList<>(index);
        for (int i = 0; i <= index; i++) {
            Integer temp  = function.apply(i);
            if (i >= index - 100) {
                hash.add(temp);
            }
            list.add(temp);
        }
        return Collections.unmodifiableList(list);
    }
}