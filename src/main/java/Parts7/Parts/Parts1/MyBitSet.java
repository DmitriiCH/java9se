package Parts7.Parts.Parts1;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.function.Predicate;

public class MyBitSet {

    public static void main(String[] args) {
        int n = 15;
        BitSet bit = getSet((ArrayList<Integer>) getElements(n));
        Predicate<Integer> myFilter = createTester(n);
        BitSet filter = getSet((ArrayList<Integer>) filterSet(getElements(n), myFilter));
        bit.xor(filter);//методом xor(BitSet set) оставляем те элементы для вызываемого обьекта, которые отсутствуют в множестве set
        System.out.println(bit);
    }

    public static BitSet getSet(ArrayList<Integer> list) {
        BitSet bitSet = new BitSet();
        list.forEach(bitSet::set);
        return bitSet;
    }

    public static List<Integer> getElements(int n) {
        List<Integer> list = new ArrayList<>();
        for (int i = 2; i <= n; i++) {
            list.add(i);
        }
        return list;
    }

    public static List<Integer> filterSet(List<Integer> list, Predicate<Integer> filter) {
        List<Integer> removeObjects = new ArrayList<>();
        int count = 0;
        for (int s : list) {
            if (filter.test(s)) {
                while (searchElementForDelete(list, s * (s + count), (ArrayList<Integer>) removeObjects)) {
                    count++;
                }
                count = 0;
            } else break;
        }
        return removeObjects;
    }

    private static boolean searchElementForDelete(List<Integer> list, int element, ArrayList<Integer> result) {
        for (int x : list) {
            if (x == element) {
                result.add(element);
                return true;
            }
        }
        return false;
    }

    public static Predicate<Integer> createTester(int n) {
        Predicate<Integer> filter = (setElement) -> {
            return setElement * setElement <= n;
        };
        return filter;
    }
}
