package Parts7.Parts.Parts14;

import java.util.*;
import java.util.function.IntFunction;

/*
Напишите метод для получения неизменяемого представления списка чисел от 0 до n,
не сохраняя эти числа.
*/
public class Main {

    private HashSet<Integer> hash = new HashSet<>();

    public static void main(String[] args) {
        List list = new Main().returnUnmodifiableList(10);
        System.out.println(list.size());
        list.add(1);
    }

    public List<Integer> returnUnmodifiableList(int index) {
        List<Integer> list = new ArrayList<>(index);
        for (int i = 0; i <= index; i++) {
            list.add(i);
        }
        return Collections.unmodifiableList(list);
    }
}