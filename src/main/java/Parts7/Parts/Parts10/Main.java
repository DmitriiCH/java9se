package Parts7.Parts.Parts10;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.PriorityQueue;

/*
Реализуйте алгоритм Дейкстры для поиска кратчайших путей между городами,
связанными сетью автомобильных дорог. (Описание этого алгоритма можно найти в
популярной литературе по алгоритмам или в соответствующей статье Википедии).
Воспользуйтесь вспомогательным классом Neighbor для хранения названия соседнего города
и расстояния до него. Представьте полученный граф в виде преобразования названий
городов в множества соседних городов. Воспользуйтесь в данном алгоритме классом
PriorityQueue<Neighbor>.
 */
public class Main {

    private static ArrayList<String> results = new ArrayList<>();
    private static HashSet<Neighbor> set = new HashSet<>();
    private static City startCity;
    private static int totalLength;

    public static void main(String[] args) {
        City samara = new City("Samara");
        City saratov = new City("Saratov");
        City tolyatti = new City("Tolyatti");
        City sizran = new City("Sizran");
        City penza = new City("Penza");
        City mosсow = new City("Mosсow");
        samara.addNeighbor(new Neighbor(saratov, 550));
        samara.addNeighbor(new Neighbor(tolyatti, 100));
        //
        saratov.addNeighbor(new Neighbor(samara, 550));
        saratov.addNeighbor(new Neighbor(sizran, 300));
        saratov.addNeighbor(new Neighbor(mosсow, 1200));
        //
        tolyatti.addNeighbor(new Neighbor(samara, 100));
        tolyatti.addNeighbor(new Neighbor(sizran, 50));
        //
        sizran.addNeighbor(new Neighbor(tolyatti, 50));
        sizran.addNeighbor(new Neighbor(saratov, 300));
        sizran.addNeighbor(new Neighbor(penza, 600));
        sizran.addNeighbor(new Neighbor(mosсow, 900));
        //
        penza.addNeighbor(new Neighbor(sizran, 600));
        penza.addNeighbor(new Neighbor(mosсow, 150));
        //
        mosсow.addNeighbor(new Neighbor(penza, 150));
        mosсow.addNeighbor(new Neighbor(sizran, 900));
        mosсow.addNeighbor(new Neighbor(saratov, 1200));
        //
        searchLength(samara, penza);
        results.forEach(System.out::println);
    }

    public static void searchLength(City start, City finish) {
        startCity = start;
        searchLength(start, finish, 0);
        //Вспомогательный перегруженный метод, принимающий расстояние, оно необходимо
        //для вычисления общего расстояния. Для пользователя его задовать не нужно.
    }

    /*
    Создаем очередь, в которой находятся ближайшие города, для города start.
    Обходим рекурсивно каждый объект из очереди, если конечный город совпада-
    ет с искомым - записываем результат в общий список этого класса. После
    выхода из очередного рекурсивного метода, удаляем его из множества set пройденых
    городов. Таким образом мы проверяем абсолютно все возможные комбинации.
    Множество set используется что бы не возвращаться в предыдущие города.
    Когда мы рекурсивно выходим из метода и удаляем город из множества,
    мы в него повторно не попадем, так как этот город удаляется из очереди города start.
     */
    private static void searchLength(City start, City finish, int length) {
        totalLength += length;
        PriorityQueue<Neighbor> queue = start.getQueue();
        if (start.equals(finish)) {
            StringBuilder distance = new StringBuilder();
            results.add(String.format("%s - %s%s, %s км", startCity.getName(), distance.toString(), finish.getName(),
                    (totalLength)));
        } else {
            for (Neighbor neighbor : queue) {
                if (neighbor.getNeighboringCity().equals(finish)) {
                    StringBuilder distance = new StringBuilder();
                    set.forEach((s) -> distance.append(s.getNeighboringCity().getName() + " - "));
                    results.add(String.format("%s - %s%s, %s км", startCity.getName(), distance.toString(), finish.getName(),
                            (neighbor.getDistance() + totalLength)));
                } else {
                    if (!neighbor.getNeighboringCity().equals(finish) && !neighbor.getNeighboringCity().equals(start) && !set.contains(neighbor) && !startCity.equals(neighbor.getNeighboringCity())) {
                        set.add(neighbor);
                        searchLength(neighbor.getNeighboringCity(), finish, neighbor.getDistance());
                        set.remove(neighbor);
                        totalLength -= neighbor.getDistance();
                    }
                }
            }
        }
    }
}

