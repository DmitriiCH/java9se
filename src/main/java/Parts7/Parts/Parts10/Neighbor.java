package Parts7.Parts.Parts10;

import java.util.Objects;

public class Neighbor implements Comparable<Neighbor> {

    private City neighboringCity;
    private int distance;

    public Neighbor(City neighboringCity, int distance) {
        this.neighboringCity = neighboringCity;
        this.distance = distance;
    }

    public City getNeighboringCity() {
        return neighboringCity;
    }

    public int getDistance() {
        return distance;
    }

    @Override
    public int compareTo(Neighbor o) {
        return this.distance - o.distance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Neighbor neighbor = (Neighbor) o;
        return Objects.equals(neighboringCity, neighbor.neighboringCity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(neighboringCity);
    }
}
