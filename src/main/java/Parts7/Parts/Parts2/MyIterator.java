package Parts7.Parts.Parts2;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyIterator implements Iterator {

    private char[] elements;
    private int index = 0;

    public MyIterator(char[] elements) {
        this.elements = elements;
    }

    @Override
    public boolean hasNext() {
        return index < elements.length;
    }

    @Override
    public Character next() {
        if (hasNext()) {
            return elements[index++];
        } else {
            throw new NoSuchElementException("No such element.");
        }
    }

    public char upperChar() {
        String s = "" + next();
        s = s.toUpperCase();
        return s.toCharArray()[0];
    }
}

