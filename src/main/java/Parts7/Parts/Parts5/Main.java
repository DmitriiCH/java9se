package Parts7.Parts.Parts5;

import java.util.*;

/*
Реализуйте метод public static void swap(List<?> list, int i, int j),
выполняющий перестановку элементов обычным образом, когда класс, опреде-
ляющий тип параметра list, реализует интерфейс RandomAccess, а иначе
сводящий к минимуму обход элементов на позициях, обозначаемых индексами i и j.
*/
public class Main {

    public static void main(String[] args) {
        List<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        swap(list, 0, 1);
        list.forEach(System.out::println);
    }

    public static void swap(List<?> list, int i, int j) {
        swapHelper(list, i, j); //методом захвата типа.
    }

    private static <T> void swapHelper(List<T> list, int i, int j) {
        if (list instanceof RandomAccess) {
            Collections.swap(list, i, j);
        } else {
            T temp = list.get(i);
            list.set(i, list.get(j));
            list.set(j, temp);
        }
    }
}

