package Parts7.Parts.Parts3;

import java.util.HashSet;
import java.util.Set;

/*
Как вычислить объеденение, пересечение и разность двух множеств,
используя только методы из интерфейса Set, но не организуя циклы?
 */
public class Main {

    public static void main(String[] args) {
        Set<String> set1 = new HashSet<>();
        set1.add("1");
        set1.add("2");
        set1.add("3");
        set1.add("4");
        Set<String> set2 = new HashSet<>();
        set2.add("3");
        set2.add("4");
        set2.add("5");
        set2.add("6");
//        Set<String> set4 = union(set1, set2);
//        Set<String> set4 = intersect(set1, set2);
        Set<String> set4 = difference(set1, set2);
        set4.forEach(System.out::println);
    }

    public static <E> Set<E> union(Set<E> set1, Set<E> set2) {
        set1.addAll(set2);
        return set1;
    }

    public static <E> Set<E> intersect(Set<E> set1, Set<E> set2) {
        set1.retainAll(set2); //удаляем элементы множества, которых нет во втором множестве
        return set1;
    }

    public static <E> Set<E> difference(Set<E> set1, Set<E> set2) {
        set1.containsAll(set2); // возвращаются те элементы, которые есть во втором множесте + элементы первого множества
        return set1;
    }
}

