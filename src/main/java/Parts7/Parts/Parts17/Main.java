package Parts7.Parts.Parts17;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
Покажите, каким образом проверяемое представление может сообщить о конкретной ошибке,
ставшей причиной загрязнения "кучи".

Загрязнение кучи - это результат при котором в коллекции оказываются элементы других типов,
не соответствующие типам указанным в объявлении колекции: пример такого загрязнения показан ниже.
Причем ошибка при выполнении программы возникает при попытке извлеч этот элемент из "кучи", а не
в моменте его ввода в коллекцию (СТРОКА 27)
Чтобы избежать возникновение таких ошибок, следует воспользоваться проверяемым представлением
Collections.checkedList(). Такое представление контролирует все операции добавления элементов в
список и в случае добавления неверного типа возникнет исключение ClassCastException в строке 34.
 */
public class Main {

    public static void main(String[] args) {
        List ints = new ArrayList<Integer>();
        ints.add(1);
        ints.add(2);
        List<String> strings = ints; //в результате ссылка strings типа List<String> ссылается на
        // объект new ArrayList<Integer>(), что приводит к загрязнению кучи.
        strings.add("S");
        strings.forEach(System.out::println);

        List strings2 = Collections.checkedList(new ArrayList<>(), Integer.class);
        strings2.add(1);
        strings2.add(2);
        List<String> strings3 = strings2;
        strings3.add("1");
        strings2.forEach(System.out::println);
    }
}
