package Parts7.Parts.Parts7;

import java.io.*;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.IntFunction;

/*
Напишите программу для чтения всех слов из файла и вывода частоты,
с которой каждое слово встречается в нем. Воспользуйтесь для этой
цели классом TreeMap<String, Integer>.
 */
public class Main {

    public static void main(String[] args) {
        File file = new File("./src/main/java/Parts7/Parts/Parts7/testFile.txt");
        try {
            readFiles(file).forEach((el1, el2) -> System.out.println(el1 + " - " + el2));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Map<String, Integer> readFiles(File file) throws IOException {
        Map<String, Integer> map = new TreeMap<>();
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String str;
        while ((str = reader.readLine()) != null) {
            String[] elements = str.split(" ");
            editorMap(elements, map);
        }
        return map;
    }

    private static void editorMap(String[] elements, Map<String, Integer> map) {
        for (String element : elements) {
            map.merge(element, 1, Integer::sum);
        }
    }
}


