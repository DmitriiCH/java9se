package Parts7.Parts.Parts4;

import java.lang.reflect.Array;
import java.util.*;

/*
Воспроизведите ситуацию, когда возникает исключение типа
ConcurrentModificationException. Что можно предпринять, чтобы избежать
этого?

Данная ошибка возникает в результате модифицирования коллекции, во время прохода по ней.
Для того что бы избежать ее, нужно вызывать метод remove() у итератора.

        for (Integer x : list) {
            if (x == 2) list.remove(2);
        }

        или

        while (iterator.hasNext()) {
            int i = iterator.next();
            if (i == 2) iterator.remove();
        }

 */
public class Main {

    public static void main(String[] args) {
        List<Integer> list = init(1, 10);
        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) {
            int i = iterator.next();
            if (i == 2) {
                iterator.remove();
            }
        }
        printCollection(list);
    }

    private static List<Integer> init(int indexStart, int IndexFinish) {
        List<Integer> list = new ArrayList<>();
        for (int i = indexStart; i <= IndexFinish; i++) {
            list.add(i);
        }
        return list;
    }

    public static <T> void printCollection(List<T> list) {
        for (T t : list) {
            System.out.println(t);
        }
    }
}

