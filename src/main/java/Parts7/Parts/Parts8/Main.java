package Parts7.Parts.Parts8;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/*
Напишите программу для чтения всех слов из файла и вывода строк,
в которых каждое слово встречается в нем. Воспользуйтесь для этой
цели преобразованием из символьных строк в множества.
 */
public class Main {

    public static void main(String[] args) {
        File file = new File("./src/main/java/Parts7/Parts/Parts8/text.txt");
        try {
//            readFilesAndWriteStringToSet(file).forEach((el1) -> System.out.println(el1 + " - "));
            printStrings(readFilesAndWriteStringToSet(file), readFilesAndWriteStringsToList(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Set<String> readFilesAndWriteStringToSet(File file) throws IOException {
        Set<String> list = new TreeSet<>();
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String str;
        while ((str = reader.readLine()) != null) {
            String[] elements = str.split(" ");
            for (String element : elements) {
                list.add((element.replace(".","")).replace(",",""));
            }
        }
        reader.close();
        return list;
    }

    public static List<String> readFilesAndWriteStringsToList(File file) throws IOException {
        List<String> list = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String str;
        while ((str = reader.readLine()) != null) {
                list.add(str);
        }
        reader.close();
        return list;
    }

    public static <T> void printStrings(Set<String> set, List<String> list) throws IOException {
        for (String s : set) {
            for (String ss : list) {
                List<String> elements = Arrays.asList(ss.split(" "));
                if(elements.contains(s)){
                    System.out.println("\"" + s + "\" - " + ss);
                }
            }
        }
    }
}
