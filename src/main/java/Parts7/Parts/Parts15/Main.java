package Parts7.Parts.Parts15;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.function.IntFunction;

/*
Обобщите предыдущее упражнение произвольным функциональным интерфейсом IntFunction.
Имейте в виду, что в коненом итоге может получиться бесконечная коллекция,
поэто-му некоторые методы (например, size() и toArray())
должны генерировать исклчение типа UnsupportedOperationException.
*/
public class Main {

    private HashSet<Integer> hash = new HashSet<>();

    public static void main(String[] args) {
        List<Integer> list = new Main().returnUnmodifiableList(10000, (i) -> i * 2);
        System.out.println(list.size());
        list.add(1);
    }


    public List<Integer> returnUnmodifiableList(int index, IntFunction<Integer> function) {
        List<Integer> list = new ArrayList<>(index);
        for (int i = 0; i <= index; i++) {
            list.add(function.apply(i));
        }
        return Collections.unmodifiableList(list);
    }
}