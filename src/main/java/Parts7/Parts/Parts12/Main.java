package Parts7.Parts.Parts12;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/*
Используя метод Collections.shuffle(), напишите программу для чтения предложения,
перетасовки его слов и вывода результата. Устраните (до и после перетасовки)
написание начального слова с заглавной буквы и наличие точки в конце предложения.
Подсказка: не перетасовывайте при этом слова.
 */
public class Main {

    public static void main(String[] args) {
        List<String> words = getWords("Напишите. Программу. Для. Чтения. Предложения. В. Списочный. Массив.");
//        List<String> words = getWords("Напишите программу для чтения предложения в списочный массив.");
        words.forEach(System.out::println);
    }

    public static List<String> getWords(String string) {
        String[] strings = string.split(" ");
        strings[0] = strings[0].toLowerCase();
        if (strings[strings.length - 1].endsWith(".")) {
            strings[strings.length - 1] = strings[strings.length - 1].substring(0, strings[strings.length - 1].length() - 1);
        }
        List<String> words = new ArrayList<>(Arrays.asList(strings));
        Collections.shuffle(words);
        words.set(0, words.get(0).toLowerCase());
        if (words.get(words.size() - 1).endsWith(".")) {
            words.set(words.size()- 1,words.get(words.size() - 1).replace(".", ""));
        }
        return words;
    }
}

