package Parts7.Parts.Parts13;

import java.util.LinkedHashMap;

/*
Всякий раз, когда в отображение типа LinkedHashMap вводится новый элемент,
 вызывается метод removeEldestEntry(). Реализуйте подкласс Cache, производ-
 ный от класса LinkedHashMap и ограничивающий размер отображения заданной
 величиной, предосталяемой в конструкторе этого класса.
 */
public class Cache<K, V> extends LinkedHashMap<K, V> {

    private int capacity;

    public Cache(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public V put(K key, V value) {
        if (size() >= capacity) {
            return value;
        } else {
            return super.put(key, value);
        }
    }
}

class Main {

    public static void main(String[] args) {
        Cache<String, String> cache = new Cache<>(3);
        cache.put("Один","1");
        cache.put("Два","1");
        cache.put("Три","1");
        System.out.println(cache.put("Четыре","4"));
        System.out.println(cache.size());

    }
}
