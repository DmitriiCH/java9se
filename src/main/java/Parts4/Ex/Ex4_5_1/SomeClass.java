package Parts4.Ex.Ex4_5_1;

import java.lang.reflect.*;

public class SomeClass {

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException,
            InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        Person p = new Person();
        Class clss = Person.class;
        Class clss1 = p.getClass();
        Class clss2 = Class.forName("Parts4.Ex.Ex4_5_1.Person");
//        Constructor[] constructors = clss.getDeclaredConstructors();
//        for (Constructor c : constructors) {
//            System.out.println(c.getName() + ", " + Modifier.toString(c.getModifiers()));
//            Parameter[] parameters = c.getParameters();
//            for (Parameter p1 : parameters) {
//                System.out.println(p1.getType().getName() + " = " + p1.getName());
//            }
//        }
        Person person = new Person();
        Method[] methods = clss.getDeclaredMethods();
        for (Method method : methods) {
//            System.out.println(method.getName() + ", " + Modifier.toString(method.getModifiers()));
//            Parameter[] parameters = method.getParameters();
//            for (Parameter p1 : parameters) {
//                System.out.println(p1.getType().getName() + " = " + p1.getName());
//            }
            method.invoke(person,"text");
        }
    }
}

class Person {

    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person() {
    }

    public static String personMethod(String s) {
        System.out.println(s);
        return s;
    }
}