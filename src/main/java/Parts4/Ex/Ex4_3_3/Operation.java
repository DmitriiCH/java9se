package Parts4.Ex.Ex4_3_3;

public enum Operation {
    ADD {
        public int eval(int arg1, int arg2) {
            return arg1 + arg2;
        }
    },

    SUBSTRACT {
        public int eval(int arg1, int arg2) {
            return arg1 - arg2;
        }
    },

    MULTIPLY {
        public int eval(int arg1, int arg2) {
            return arg1 * arg2;
        }
    },

    DIVIDE {
        public int eval(int arg1, int arg2) {
            return arg1 / arg2;
        }
    };

    public abstract int eval(int arg1, int arg2);
}

class Main {

    public static void main(String[] args) {
        int first = 1;
        int second = 2;
        Operation op = Operation.valueOf("ADD");
        int result = op.eval(first, second);
        System.out.println(result);
    }
}