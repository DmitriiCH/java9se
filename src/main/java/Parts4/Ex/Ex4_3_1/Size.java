package Parts4.Ex.Ex4_3_1;

import java.util.Arrays;

public enum Size {
    SMALL("S"), MEDIUM("M"), LARGE("L"), EXTRA_LARGE("E");

    private String abbreviation;

    private Size(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
}

class Main {

    public static void main(String[] args) {
        Size notMySize = Size.valueOf("SMALL");
        System.out.println("name() " + notMySize.name());
        System.out.println("compareTo() " + notMySize.compareTo(Size.LARGE));
        System.out.println("getDeclaringClass() " + notMySize.getDeclaringClass());
        System.out.println("getAbbreviation() " + notMySize.getAbbreviation());
        /////////
        Size[] allValues = Size.values();
        System.out.println("Arrays.toString(allValues) " + Arrays.toString(allValues));
        /////////
        for (Size s : Size.values()) {
            System.out.println(s);
        }
        System.out.println("ordinal() " + Size.LARGE.ordinal());
    }
}