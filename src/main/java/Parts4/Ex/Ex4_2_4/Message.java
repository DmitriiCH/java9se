package Parts4.Ex.Ex4_2_4;

import java.util.ArrayList;

public class Message implements Cloneable {

    private String sender;
    private ArrayList<String> recipients;
    private String text;

    public Message(String sender, String text) {
        this.sender = sender;
        this.text = text;
    }

    public void addRecipient(String recipint) {
    }

    public Message clone() throws CloneNotSupportedException {
        return (Message) super.clone();
    }

    public Message cloneOne() {
        Message cloned = new Message(sender, text);
        cloned.recipients = (ArrayList<String>) recipients.clone();
        return cloned;
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        Message specialOffer = new Message("aaa", "asasa");
        Message cloneOfSpecialOffer = (Message) specialOffer.clone();
        System.out.println(specialOffer.equals(cloneOfSpecialOffer));
    }
}
