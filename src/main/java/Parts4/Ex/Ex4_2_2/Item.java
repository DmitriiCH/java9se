package Parts4.Ex.Ex4_2_2;

import java.util.Objects;

public class Item {

    private String description;
    private double price;

    @Override
    public String toString() {
        return getClass().getName() + "[description = " + description + ", price = " + price + "]";
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, price);
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) return true;
        if (getClass() != otherObject.getClass()) return false;
        if (otherObject == null) return false;
        Item item = (Item) otherObject;
        return Objects.equals(this.description, item.description) && this.price == item.price;
    }
}
