package Parts4.Ex.Ex4_2_2;

import java.util.Objects;

public class DisconectedItem extends Item {

    private double discount;

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), discount);
    }

    public boolean equals(Object otherObject) {
        if (!super.equals(otherObject)) return false;
        DisconectedItem other = (DisconectedItem) otherObject;
        return discount == other.discount;
    }
}
