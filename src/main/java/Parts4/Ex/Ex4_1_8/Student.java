package Parts4.Ex.Ex4_1_8;

public class Student extends Person {

    private int id;

    public Student(String name, int id) {
        super(name);
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
