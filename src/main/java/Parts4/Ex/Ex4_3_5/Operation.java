package Parts4.Ex.Ex4_3_5;

public enum Operation {
    ADD, SUBSTRACT, MULTIPLY, DIVIDE;

    public static int eval(Operation op, int args1, int args2) {
        int result = 0;
        switch (op) {
            case ADD: {
                result = args1 + args2;
                break;
            }
            case SUBSTRACT: {
                result = args1 - args2;
                break;
            }
            case MULTIPLY: {
                result = args1 * args2;
                break;
            }
            case DIVIDE: {
                result = args1 / args2;
                break;
            }
        }
        return result;
    }
}
