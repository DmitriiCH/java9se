package Parts4.Ex.Ex4_1_1;

public class Manager extends Employee {

    private double bonus;

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    public Manager(String name, double salary) {
        super(name, salary);
        this.bonus = 0;
    }

    @Override
    public double getSalary() {
        return super.getSalary() + bonus;
    }

    public static void main(String[] args) {
        Manager manager = new Manager("Boss", 1.6);
        manager.setBonus(10000);
        manager.reiseSalary(5);
        Employee empl = manager;
        double salary = empl.getSalary();
        System.out.println(salary);
        ///////////////
        Employee empl1 = new Manager("Boss", 1.6);
        if(empl1 instanceof Manager){
            ((Manager)empl).setBonus(10000);
        }
        //empl1.setBonus(10000);
    }
}
