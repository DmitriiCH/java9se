package Parts4.Ex.Ex4_2_3;

import java.util.Objects;

public class Main {

    public static void main(String[] args) {
        Integer x = 1;
        Integer y = 1;
        System.out.println(x.hashCode() == y.hashCode());
        System.out.println("Mary".hashCode());
        System.out.println("Myra".hashCode());
    }
}
