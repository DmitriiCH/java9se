package Parts4.Ex.Ex4_4_1;

import java.lang.reflect.Modifier;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws ClassNotFoundException {
        Object obj = "String";
        Class<?> cl = obj.getClass();
        System.out.println("This object is an instance of " + cl.getName());
        ///////////////////////
        System.out.println("String".getClass().getSimpleName());
        System.out.println("String".getClass().getName());
        ///////////////////////
        String className = "java.util.Scanner";
        Class<?> cl1 = Class.forName(className);
        System.out.println(cl1.getSimpleName());
        ///////////////////////
        Class<?> cl2 = java.util.Scanner.class;
        Class<?> cl3 = String[].class;
        Class<?> cl4 = Runnable.class;
        Class<?> cl5 = void.class;
        System.out.println(cl3.getName() + " " + cl3.getCanonicalName() + " " + cl3.getTypeName());
        System.out.println(cl4.getName() + " " + cl5.getSimpleName());
        System.out.println("\nMethods Class");
        Class<?> clss = Main.class;
        System.out.println("getCanonicalName() " + clss.getCanonicalName());
        System.out.println("getSimpleName() " + clss.getSimpleName());
        System.out.println("getTypeName() " + clss.getTypeName());
        System.out.println("toString() " + clss.toString());
        System.out.println("toGenericString() " + clss.toGenericString());
        System.out.println("getSuperclass() " + clss.getSuperclass());
        System.out.println("getInterfaces() " + clss.getInterfaces());
        System.out.println("getPackage() " + clss.getPackage());
        System.out.println("getModifiers() " + Modifier.toString(clss.getModifiers()));
    }
}