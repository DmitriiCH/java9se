package Parts4.Parts.Parts8;

import Parts4.Parts.Parts1.Point;

public class Main {

    public static void main(String[] args) {
        Class clss = int[].class;
        Class clss1 = int.class;
        System.out.println("getCanonicalName() " + clss.getCanonicalName());
        System.out.println("getSimpleName() " + clss.getSimpleName());
        System.out.println("getTypeName() " + clss.getTypeName());
        System.out.println("getName() " + clss.getName());
        System.out.println("toString() " + clss.toString());
        System.out.println("toGenericString() " + clss.toGenericString());
        System.out.println();
        System.out.println("getCanonicalName() " + clss1.getCanonicalName());
        System.out.println("getSimpleName() " + clss1.getSimpleName());
        System.out.println("getTypeName() " + clss1.getTypeName());
        System.out.println("getName() " + clss1.getName());
        System.out.println("toString() " + clss1.toString());
        System.out.println("toGenericString() " + clss1.toGenericString());



//        getCanonicalName() Возвращает каноническое имя базового класса как определяется Спецификацией языка Java.
//        Возвращает null, если базовый класс не имеет канонического имени (т.
//                е.если это локальный или анонимный класс или массив, компонент которого тип не имеет
//        канонического названия).

//        getSimpleName() Возвращает простое имя базового класса, как указано в исходном коде

//        getTypeName() Вернуть информативную строку для имени этого типа.
//                getName() Возвращает имя объекта(класс, интерфейс, класс массива, примитивный тип или void),
//        представленный этим объектом

//        toString() Преобразует объект в строку

//        toGenericString() Возвращает строку, описывающую этот класс, включая информация о модификаторах и
//        параметрах типа.
    }
}
//В классе Class имеются шесть методов, возвращающих строковое представление типа, описываемого обьектом типа Class.
// Чем отличается их применение к массивам, обобщенным типам, внутренним классам и примитивным типам.