package Parts4.Parts.Parts7;

public enum Color {
    BLACK, RED, BLUE, GREEN, CYAN, MAGENTA, YELLOW, WHITE;

    public static Color getRed() {
        return RED;
    }

    public static Color getGreen() {
        return GREEN;
    }

    public static Color getBlue() {
        return BLUE;
    }
}

class Main {

    public static void main(String[] args) {
        Color blu = Color.getBlue();
        Color red = Color.getRed();
        Color greed = Color.getGreen();
        Color cyan = Color.CYAN;
    }
}
//Определите перечислимый тип для восьми комбинаций основынх цветов - BLACK, RED, BLUE, CYAN, MAGENTA, YELLOW, WHITE - с
//методами getRed(), getGreeen() и getBlue().