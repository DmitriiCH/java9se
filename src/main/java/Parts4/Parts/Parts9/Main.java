package Parts4.Parts.Parts9;

import Parts4.Parts.Parts1.Point;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class Main {

    private static int i;
    double d;
    String s;
    Point point;

    @Override
    public String toString() {
        String result = "";
        Class clss = this.getClass();
        Field[] fields = clss.getDeclaredFields();
        for (Field field : fields) {
            if(!Modifier.toString(field.getModifiers()).contains("static")){
                result += field.getType() + " " + field.getName() + "\n";
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Main main = new Main();
        System.out.println(main.toString());
    }
}
//Напишите "универсальный" метод toString(), в котором применяется рефлексия для получения символьной строки со всеми
//переменными экземпляра обьекта. В качестве дополнительного задания можете организовать обработку циклических ссылок.