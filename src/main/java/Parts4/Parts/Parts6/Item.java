package Parts4.Parts.Parts6;

import java.util.Objects;

public class Item {

    private String description;
    private double price;

    public Item(String description, double price) {
        this.description = description;
        this.price = price;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
//        if (getClass() != otherObject.getClass()) return false;
        if (!(otherObject instanceof Item)) return false;
        Item other = (Item) otherObject;
        return Objects.equals(description, other.description) && price == other.price;
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, price);
    }
}
//Допустим, что в методе Item.equals(), представленном в разделе 4.2.2, используется проверка с помощью операции
//instanceof. Реализуйте метод DiscountedItem.equals() таким образом, чтобы выполнять в нем сравнение только с
//суперклассом, если его параметр otherObject относится к типу Item, но с учетом скидки, если это тип DiscountedItem.
//Продемонстрируйте, что этот метод сохраняет симметричность, но не транзитивность, т.е. способность обнаруживать
//сочетание товаров по обычной цене и со скидкой, чтобы делать вызовы x.equals(y) и y.equals(z), но не x.equals(z).