package Parts4.Parts.Parts6;


import java.util.Objects;

public class DiscountedItem extends Item {

    private double discount;

    public DiscountedItem(String description, double price, double discount) {
        super(description, price);
        this.discount = discount;
    }

    public boolean equals(Object otherObject) {
        if (otherObject instanceof DiscountedItem) {
            if (!super.equals(otherObject)) return false;
            DiscountedItem other = (DiscountedItem) otherObject;
            return other.discount == discount;
        } else {
            return super.equals(otherObject);
        }
    }

    public int hashCode() {
        return Objects.hash(super.hashCode(), discount);
    }

    public static void main(String[] args) {
        Item item = new Item("Помидора", 21.6);
        Item discountedItem = new DiscountedItem("Помидора", 21.6, 4);
        Item discountedItem2 = new DiscountedItem("Помидора", 21.6,4);
        System.out.println(discountedItem.equals(item));
        System.out.println(discountedItem.equals(discountedItem2));
        System.out.println(discountedItem2.equals(discountedItem));
    }
}
//Допустим, что в методе Item.equals(), представленном в разделе 4.2.2, используется проверка с помощью операции
//instanceof. Реализуйте метод DiscountedItem.equals() таким образом, чтобы выполнять в нем сравнение только с
//суперклассом, если его параметр otherObject относится к типу Item, но с учетом скидки, если это тип DiscountedItem.
//Продемонстрируйте, что этот метод сохраняет симметричность, но не транзитивность, т.е. способность обнаруживать
//сочетание товаров по обычной цене и со скидкой, чтобы делать вызовы x.equals(y) и y.equals(z), но не x.equals(z).