package Parts4.Parts.Parts11;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class HelloWorld {

    public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException, NoSuchMethodException
            , InvocationTargetException, IllegalAccessException, InstantiationException {
        myPrintln();
    }

    public static void myPrintln() throws ClassNotFoundException, NoSuchFieldException, NoSuchMethodException,
            InvocationTargetException, IllegalAccessException, InstantiationException {
        Class clss = Class.forName("java.lang.System");
        Field fields = clss.getField("out");
        ///
        System.out.println();
        Class clss2 = Class.forName(fields.getType().getCanonicalName());
        Method m = clss2.getMethod("println", String.class);
        m.invoke(fields,"Hello world!");
    }
}
//Напишите программу, выводящую сообщение "Hello, World!", воспользовавшись рефлексией для поиска поля out в классе java
//.lang.System и методом invoke() для вызова метода println();