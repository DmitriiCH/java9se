package Parts4.Parts.Parts4;

public abstract class Shape {

    protected Point point;

    public Shape(Point point) {
        this.point = point;
    }

    public abstract Point getCenter() throws CloneNotSupportedException;
}
//Определите абстрактный класс Shape с переменной экземпляра класса Point; конструктором и конкретным методом public
// void moveBy(double dx, double dy), перемещающим точку на заданное расстояние; а также абстрактным классом public
// Point getCenter(). Предоставьте конкретные подсклассы Circle, Rectangle, Line  с конструкторами public Circle
// (Point center, double radius), public Rectangle(Point topLeft, double width, double height) и public Line(Point
// from, Point to).