package Parts4.Parts.Parts4;

import java.util.Objects;

public class Circle extends Shape implements Cloneable {

    private double radius;

    public Circle(Point center, double radius) {
        super(center);
        this.radius = radius;
    }

    @Override
    public Point getCenter() {
        return point;
    }

    @Override
    protected Circle clone() throws CloneNotSupportedException {
        return (Circle) super.clone();
    }
}
//Определите абстрактный класс Shape с переменной экземпляра класса Point; конструктором и конкретным методом public
// void moveBy(double dx, double dy), перемещающим точку на заданное расстояние; а также абстрактным классом public
// Point getCenter(). Предоставьте конкретные подсклассы Circle, Rectangle, Line  с конструкторами public Circle
// (Point center, double radius), public Rectangle(Point topLeft, double width, double height) и public Line(Point
// from, Point to).