package Parts4.Parts.Parts4;

public class Rectangle extends Shape implements Cloneable {

    private double width;
    private double height;

    public Rectangle(Point topLeft, double width, double height) {
        super(topLeft);
        this.width = width;
        this.height = height;
    }

    @Override
    protected Rectangle clone() throws CloneNotSupportedException {
        return (Rectangle) super.clone();
    }

    @Override
    public Point getCenter() throws CloneNotSupportedException {
        Point center = point.clone();
        center.moveBy(width/2,height/2);
        return center;
    }
}
