package Parts4.Parts.Parts4;

public class Main {

    public static void main(String[] args) throws CloneNotSupportedException {
        Circle circle = new Circle(new Point(1,2),10);
        System.out.println(circle.getCenter().getX() + " " + circle.getCenter().getY());
        Circle cloneCircle = circle.clone();
        System.out.println("Clone Circle " + cloneCircle.getCenter().getX() + " " + cloneCircle.getCenter().getY());
        //
        Line line = new Line(new Point(2,3), new Point(3,4));
        System.out.println(line.getCenter().getX() + " " + line.getCenter().getY());
        Line cloneLine = line.clone();
        System.out.println("Clone Line " + cloneLine.getCenter().getX() + " " + cloneLine.getCenter().getY());
        //
        Rectangle rectangle = new Rectangle(new Point(1,2),2,4);
        System.out.println(rectangle.getCenter().getX() + " " + rectangle.getCenter().getY());
        Rectangle cloneRectandle = rectangle.clone();
        System.out.println("Clone Rectangle " + cloneRectandle.getCenter().getX() + " " + cloneRectandle.getCenter().getY());
    }
}
