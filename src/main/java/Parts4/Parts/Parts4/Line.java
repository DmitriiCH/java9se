package Parts4.Parts.Parts4;

public class Line extends Shape implements Cloneable {

    private Point to;

    public Line(Point from, Point to) {
        super(from);
        this.to = to;
    }

    @Override
    public Point getCenter() {
        Point center = new Point(0, 0);
        center.moveBy((to.getX() + point.getX()) / 2, (to.getY() + point.getY()) / 2);
        return center;
    }

    @Override
    protected Line clone() throws CloneNotSupportedException {
        return (Line) super.clone();
    }
}