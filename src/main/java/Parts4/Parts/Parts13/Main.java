package Parts4.Parts.Parts13;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;

public class Main {

    public static void printAllValuesInMethod(Method method, int min, int max, int count) {
        System.out.println("Modifier - " + Modifier.toString(method.getModifiers()));
        System.out.println("returnType - " + method.getReturnType());
        System.out.println("methodName - " + method.getName());
    }

    public static void main(String[] args) throws NoSuchMethodException {
        Method method = Math.class.getMethod("sqrt", double.class);
        printAllValuesInMethod(method, 1, 10, 1);
//        public static double sqrt(double a) {
//        public static String toHexString(double d) {
    }
}
//Напишите метод, выводящий таблицу значений из любого обьекта типа Method, описывающий метод с параметрами типа double
//или Double. Помимо обьекта типа Method, этот метод должен принимать нижний и верхний предел, а также величину шага.
//Продемонстрируйте свой метод, выведя таблицы для методов Math.sqrt() и Double.toHexString(). Напишите еще один вариант
//данного метода, но на этот раз воспользуйтесь обьектом типа DoubleFunction<Object> вместо обьекта типа Method (см
//. раздел 3.6.2). Сопоставьте безопастность, эффективность и удобство обоих вариантов данного метода.