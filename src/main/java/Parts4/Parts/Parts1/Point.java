package Parts4.Parts.Parts1;

import java.util.Objects;

public class Point {

    protected double x, y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null) return false;
        if (getClass() != object.getClass()) return false;
        Point point = (Point) object;
        return Double.compare(point.x, x) == 0 &&
                Double.compare(point.y, y) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return getClass().getName() + "[x = " + x + ", y = " + y + "]";
    }
}
//1. Определите класс Point с конструктором Point(double x, double y) и методами доступа getX(), getY(). Определите
// также подскласс LabeledPoint с конструктором LabeledPoint(String label, double x, double y) и методом доступа
// getLabel().