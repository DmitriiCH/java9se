package Parts4.Parts.Parts1;

import java.util.Objects;

public class LabeledPoint extends Point {

    private String label;

    public LabeledPoint(String label, double x, double y) {
        super(x, y);
        this.label = label;

    }

    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return getClass().getName() + "[label = " + label + "]";
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null) return false;
        if (getClass() != object.getClass()) return false;
        if (!super.equals(object)) return false;

        LabeledPoint that = (LabeledPoint) object;
        return Objects.equals(label, that.label);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), label);
    }

    public static void main(String[] args) {
        Point point = new Point(1.3, 1.4);
        Point point2 = new Point(1.3, 1.4);
        System.out.println(point.equals(point2));
        System.out.println(point.toString());
        System.out.println(point.hashCode());
        ///
        LabeledPoint labeledPoint = new LabeledPoint("Label", 1.4, 1.6);
        LabeledPoint labeledPoint2 = new LabeledPoint("Label", 1.4, 1.6);
        System.out.println(labeledPoint.equals(labeledPoint2));
        System.out.println(labeledPoint.toString());
        System.out.println(labeledPoint.hashCode());
        labeledPoint.x = 1.6;
        labeledPoint.y = 1.7;
    }
}
