package Parts4.Parts.Parts12;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;

public class Main {

    public static void main(String[] args) throws InvocationTargetException,
            IllegalAccessException, NoSuchMethodException {
        Date startDate = new Date();
        PascalTriangle.genList(999);
        Date endDate = new Date();
        getTime(startDate, endDate);
        ///
        Date startDate1 = new Date();
        PascalTriangle triangle = new PascalTriangle();
        Class clss = triangle.getClass();
        Method method = clss.getMethod("genList",Integer.class);
        method.invoke(null, 999);
        Date endDate1 = new Date();
        getTime(startDate1, endDate1);
    }

    public static void getTime(Date start, Date end) {
        System.out.println(end.getTime() - start.getTime());
    }
}

class PascalTriangle {

    public static ArrayList<ArrayList<Integer>> genList(Integer numRows) {
        ArrayList<ArrayList<Integer>> list = new ArrayList<>();
        if (numRows == 0) {
            return list;
        }
        for (int i = 0; i < numRows; i++) {
            list.add(new ArrayList<>());
        }
        list.get(0).add(1);
        for (int i = 1; i < numRows; i++) {
            list.get(i).add(1);
            for (int j = 1; j < i; j++) {
                list.get(i).add(list.get(i - 1).get(j) + list.get(i - 1).get(j - 1));
            }
            list.get(i).add(1);
        }
        return list;
    }
}