package Parts3.Ex.Ex3_2_2;

public interface IntSequence {

    default boolean hasNext() {
        return true;
    }

    int next();
}
