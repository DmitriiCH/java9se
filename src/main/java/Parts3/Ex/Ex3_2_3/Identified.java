package Parts3.Ex.Ex3_2_3;

public interface Identified {

    default int getId() {
        return Math.abs(hashCode());
    }
}
