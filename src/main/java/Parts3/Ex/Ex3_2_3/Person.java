package Parts3.Ex.Ex3_2_3;

public interface Person {

    String getName();

    default int getId() {
        return 0;
    }
}
