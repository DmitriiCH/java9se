package Parts3.Ex.Ex3_4_2;

import java.util.Arrays;
import java.util.Comparator;

public class Test {

    public static void main(String[] args) {
        String[] friends = {"Peter", "Paul", "Mary"};
        Arrays.sort(friends, (first, second) -> first.length() - second.length());
        System.out.println(Arrays.toString(friends));
    }
}
