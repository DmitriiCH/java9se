package Parts3.Ex.Ex3_1_1;

public interface IntSequence {
    public boolean hasNext();
    public int next();
}
