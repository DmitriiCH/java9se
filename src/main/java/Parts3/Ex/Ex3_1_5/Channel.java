package Parts3.Ex.Ex3_1_5;

import java.io.Closeable;

public interface Channel extends Closeable {
    boolean isOpen();
}
