package Parts3.Ex.Ex3_1_5;

import java.io.Closeable;
import java.io.IOException;

public class Chann implements Channel {

    @Override
    public void close() throws IOException {
    }

    @Override
    public boolean isOpen() {
        return false;
    }

    public static void main(String[] args) {
        Closeable chann = new Chann();
        if(chann instanceof Channel){
            Channel channel = (Channel) chann;
            System.out.println(channel);
            if(channel instanceof Chann){
                Chann chann1 = (Chann) channel;
                System.out.println(chann1);
            }
        }
    }
}
