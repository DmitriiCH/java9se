package Parts3.Ex.Ex3_1_6;

import Parts3.Ex.Ex3_1_1.IntSequence;

import java.io.Closeable;
import java.io.IOException;

public class FileSequence implements IntSequence, Closeable {

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public int next() {
        return 0;
    }

    @Override
    public void close() throws IOException {
    }
}
