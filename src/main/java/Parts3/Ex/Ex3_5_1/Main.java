package Parts3.Ex.Ex3_5_1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>(Arrays.asList("Василий", "Павел", "Петя"));
        list.forEach(s -> System.out.println(s));
        ///
        list.forEach(System.out::println);
    }
}
