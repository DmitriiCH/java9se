package Parts3.Ex.Ex3_3_2;

import java.util.Arrays;
import java.util.Comparator;

public class LengthComparator implements Comparator<String> {

    public int compare(String first, String second) {
        return first.length() - second.length();
    }

    public static void main(String[] args) {
        Comparator<String> comp = new LengthComparator();
        String[] friends = {"Peter", "Paul", "Mary"};
        System.out.println(Arrays.toString(friends));
        if (comp.compare(friends[0], friends[1]) > 0) {
        }
        Arrays.sort(friends, new LengthComparator());
        System.out.println(Arrays.toString(friends));
    }
}
