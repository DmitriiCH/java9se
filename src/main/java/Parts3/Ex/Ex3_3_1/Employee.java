package Parts3.Ex.Ex3_3_1;

import java.util.Arrays;

public class Employee implements Comparable<Employee> {

    private int id;
    private double salary;

    public int getId() {
        return id;
    }

//    public int compareTo(Employee other) {
//        return getId() - other.getId();
//    }

    public int compareTo(Employee other) {
        return Double.compare(salary, other.salary);
    }

    public static void main(String[] args) {
        String[] friends = {"Peter", "Paul", "Mary"};
        Arrays.sort(friends);
        System.out.println(Arrays.toString(friends));
    }
}
