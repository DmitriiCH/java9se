package Parts3.Ex.Ex3_3_3;

public class HelloTask implements Runnable {

    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("Hello, World!");
        }
    }

    public static void main(String[] args) {
        Runnable task = new HelloTask();
        Thread thread = new Thread(task);
        thread.start();
    }
}
