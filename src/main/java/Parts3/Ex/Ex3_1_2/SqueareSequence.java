package Parts3.Ex.Ex3_1_2;

import Parts3.Ex.Ex3_1_1.IntSequence;

public class SqueareSequence implements IntSequence {

    private int i;

    public boolean hasNext() {
        return true;
    }

    public int next() {
        i++;
        return i * i;
    }

    public static double average(IntSequence seq, int n) {
        int count = 0;
        double sum = 0;
        while (seq.hasNext() && count < n) {
            count++;
            sum += seq.next();
        }
        return count == 0 ? 0 : sum / count;
    }

    public static void main(String[] args) {
        SqueareSequence squares = new SqueareSequence();
        double avg = average(squares, 100);
        System.out.println(avg);
    }
}
