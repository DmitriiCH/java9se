package Parts3.Ex.Ex3_1_2;

import Parts3.Ex.Ex3_1_1.IntSequence;

public class DigitSequence implements IntSequence {

    private int number;

    public DigitSequence(int n) {
        number = n;
    }

    @Override
    public boolean hasNext() {
        return number != 0;
    }

    public int next() {
        int result = number % 10;
        number /= 10;
        return result;
    }

    public int rest() {
        return number;
    }

    public static void main(String[] args) {
        IntSequence digits = new DigitSequence(1729);
        double avg = SqueareSequence.average(digits, 100);
        System.out.println(avg);
    }
}
