package Parts3.Ex.Ex3_2_1;

import Parts3.Ex.Ex3_1_2.DigitSequence;

public interface IntSequence {
    public static IntSequence digitsOf(int n){
        return (IntSequence) new DigitSequence(n);
    }
}
