package Parts3.Ex.Ex3_3_4;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class CancelAction implements EventHandler<ActionEvent> {

    public void handle(ActionEvent event) {
        System.out.println("Oh, noes!");
    }
}
