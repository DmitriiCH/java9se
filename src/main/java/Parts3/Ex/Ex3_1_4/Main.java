package Parts3.Ex.Ex3_1_4;

import Parts3.Ex.Ex3_1_1.IntSequence;
import Parts3.Ex.Ex3_1_2.DigitSequence;

public class Main {

    public static void main(String[] args) {
        IntSequence sequence = new DigitSequence(1729);
        if (sequence instanceof DigitSequence) {
            System.out.println(sequence instanceof DigitSequence);
            DigitSequence digits = (DigitSequence) sequence;
            System.out.println(digits.rest());
        }
        ///////////////
        IntSequence intSequence1 = null;
        if (intSequence1 instanceof DigitSequence){
            DigitSequence digitSequence = (DigitSequence) intSequence1;
        }
        System.out.println(intSequence1 instanceof DigitSequence);
    }
}
