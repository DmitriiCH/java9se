package Parts3.Parts.Parts1;

public interface Measurable {

    double getMeasure();
}
