package Parts3.Parts.Parts1;

public class Main {

    public static double average(Measurable[] objects) {
        double sum = 0;
        for (int i = 0; i < objects.length; i++) {
            sum += objects[i].getMeasure();
        }
        return sum == 0 ? 0 : sum / objects.length;
    }

    public static void main(String[] args) {
        Measurable[] objects = new Employee[]{new Employee(1.3), new Employee(2.3), new Employee(3.3)};
        System.out.println(average(objects));
    }
}