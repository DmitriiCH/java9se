package Parts3.Parts.Parts6;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static BigInteger average(Sequence sequence, BigInteger bigInteger) {
        BigInteger count = BigInteger.ZERO;
        BigInteger sum = BigInteger.ZERO;
        while (sequence.hasNext() && count.compareTo(bigInteger) == -1) {
            count = count.add(BigInteger.ONE);
            sum = sum.add((BigInteger) sequence.next());
        }
        return count.compareTo(BigInteger.ZERO) == 0 ? BigInteger.ZERO : sum.divide(count);
    }

    public static void main(String[] args) {
        SquareSequence squareSequence = new SquareSequence();
        BigInteger bigDecimal = average(squareSequence, new BigInteger(String.valueOf(10)));
        System.out.println(bigDecimal);
    }
}
//На самом деле класс SquareSequence не предоставляет бесконечную последовательность квадратов целых чисел из-за
//возможного переполнения. Как же он тогда действует? Устраните этот недостаток, определив интерфейс как Sequence<T> и
//класс SquareSequence, реализующий интерфейс Sequence<BigInteger>.