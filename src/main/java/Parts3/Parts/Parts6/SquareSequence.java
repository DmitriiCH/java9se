package Parts3.Parts.Parts6;

import java.math.BigInteger;

public class SquareSequence implements Sequence<BigInteger> {

    private BigInteger bigInteger = BigInteger.ZERO;

    public boolean hasNext() {
        return true;
    }

    public BigInteger next() {
        bigInteger = bigInteger.add(BigInteger.ONE);
        return bigInteger.multiply(bigInteger);
    }
}
