package Parts3.Parts.Parts6;

public interface Sequence<T> {
    boolean hasNext();
    T next();
}
