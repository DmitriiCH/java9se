package Parts3.Parts.Parts8;

import java.util.*;

public class Main {

    public void luckySort(ArrayList<String> strings, Comparator<String> comp) {
        for (int i = 0; i < strings.size(); i++) {
            if(i != strings.size() - 1){
                int compareLength = comp.compare(strings.get(i), strings.get(i + 1));
                if(compareLength <= 0){
                    Collections.shuffle(strings);
                }
            }
        }
    }

    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>(Arrays.asList("Mdпапа", "Pet", "Paul", "Mdsdsy"));
        Main main = new Main();
        main.luckySort(list,new ListComparator());
        System.out.println(list);
    }
}
//Реализуйте метод void luckySort(ArrayList<String> strings, Comparator <String> comp),
//вызывающий метод Collections.shuffle () для списочного массива до тех пор, пока элементы этого массива
// располагаются в возрастающем порядке,
//задаваемом компаратором.