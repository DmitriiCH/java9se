package Parts3.Parts.Parts11;

import java.io.File;
import java.io.FileFilter;

public class Directory {

    public static File[] getDirectory(String directory) {
        File file = new File(directory);
        return file.listFiles(pathname -> pathname.isDirectory());
    }
////////////////////////////////////////////
//    public static File[] getDirectory (String directory) {
//        File file = new File(directory);
//        return file.listFiles(File::isDirectory);
//    }
////////////////////////////////////////////
//    public static File[] getDirectory (String directory) {
//        File file = new File(directory);
//        return file.listFiles(new FileFilter() {
//            @Override
//            public boolean accept(File pathname) {
//                return pathname.isDirectory();
//            }
//        });
//    }
}

class myFilterFile implements FileFilter {

    @Override
    public boolean accept(File pathname) {
        return pathname.isDirectory();
    }
}
//Используя методы listFiles (FileFilter) и isDirectory из класса java.
//io.File, напишите метод, возвращающий все подкаталоги из заданного каталога.
//Воспользуйтесь для этой цели лямбда-выражением вместо объекта типа FileFilter.
//Сделайте то же самое, используя ссылку на метод и анонимный внутренний класс.