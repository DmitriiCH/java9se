package Parts3.Parts.Parts14;

import java.util.Arrays;

public class Run {

    public static Runnable getRunnable(Runnable[] runnables) {
        return () -> {
            for (Runnable task : runnables) {
                task.run();
            }
        };
    }
}
//Напишите метод, принимающий массив экземпляров типа Runnable и возвращающий экземпляр типа Runnable,
//метод которого run () выполняет их по порядку. Организуйте возврат лямбда-выражения.