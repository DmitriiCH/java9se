package Parts3.Parts.Parts9;

public class Greeter implements Runnable {

    private int n;
    private String target;

    public Greeter(int n, String target) {
        this.n = n;
        this.target = target;
    }

    @Override
    public void run() {
        for (int i = 0; i < n; i++) {
            System.out.println("Hello, " + target);
        }
    }
}
//Создайте класс Greeter, реализующий интерфейс Runnable, метод которого run () выводит п раз сообщение "Hello, " +
// target,
//где п и target — параметры, устанавливаемые в конструкторе.
//Получите два экземпляра этого класса с разными сообщениями и выполните их параллельно в двух потоках.