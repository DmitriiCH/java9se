package Parts3.Parts.Parts9;

public class Main {

    public static void main(String[] args) {
        Greeter greeter1 = new Greeter(6, "Петя");
        Greeter greeter2 = new Greeter(3, "Коля");
        Thread thread1 = new Thread(greeter1);
        Thread thread2 = new Thread(greeter2);
        thread1.start();
        thread2.start();
    }
}