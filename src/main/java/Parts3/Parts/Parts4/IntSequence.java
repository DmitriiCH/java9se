package Parts3.Parts.Parts4;

public interface IntSequence {

    public static int[] of(int... a) {
        int[] ints = new int[a.length];
        int index = 0;
        for (int intVar : a) {
            ints[index] = intVar;
            index++;
        }
        return ints;
    }

    public static IntSequence ofTwo(int... a) {
        return new IntSequence() {
            public int[] of(int... a) {
                int[] ints = new int[a.length];
                int index = 0;
                for (int intVar : a) {
                    ints[index] = intVar;
                    index++;
                }
                return ints;
            }
        };
    }
}