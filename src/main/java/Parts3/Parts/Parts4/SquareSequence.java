package Parts3.Parts.Parts4;

import java.util.Arrays;

public class SquareSequence {

    public static void main(String[] args) {
        int[] ints = IntSequence.of (3, 1, 4, 1, 5, 9);
        IntSequence intSequence = IntSequence.ofTwo(3, 1, 4, 1, 5, 9);
        System.out.println(Arrays.toString(ints));
        System.out.println(intSequence);
    }
}
//Реализуйте статический метод of () из интерфейса IntSequence, возвращающий последовательность из передаваемых ему
// аргументов.
//Например, в результате вызова метода IntSequence.of (3, 1, 4, 1, 5, 9) возвращается последовательность из шести
// значений.
//В качестве дополнительного задания организуйте возврат экземпляра анонимного внутреннего класса.