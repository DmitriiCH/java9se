package Parts3.Parts.Parts12;

import java.io.File;
import java.io.FilenameFilter;

public class Directory {

    public static File[] getFileInDirectory(String directory, String suffix) {
        File file = new File(directory);
        String[] strings = file.list((dir, name) -> name.toLowerCase().endsWith(suffix));
        File[] files = new File[strings.length];
        for (int i = 0; i < strings.length; i++) {
            files[i] = new File(strings[i]);
        }
        return files;
    }

    public static void main(String[] args) {
        File[] files = getFileInDirectory("C:\\Users\\dmch1019\\Desktop\\Java", "txt");
        for (File f : files) {
            System.out.println(f);
        }
    }
}
//Используя метод list (FilenameFilter) из класса java. io. File, напишите метод, возвращающий все файлы из заданного
// каталога с указанным расширением.
//Воспользуйтесь для этой цели лямбда-выражением вместо объекта типа FilenameFilter.Какая переменная из объемлющей
// области действия захватывается лямбда-выражением?