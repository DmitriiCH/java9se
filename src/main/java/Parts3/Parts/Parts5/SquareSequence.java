package Parts3.Parts.Parts5;

public class SquareSequence implements IntSequence {

    public static void main(String[] args) {
        String contant = IntSequence.constant(1);
        System.out.println(contant);
        IntSequenceTwo intSequenceTwo = (int a) -> {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < 3; i++) {
                stringBuilder.append(1);
                stringBuilder.append(" ");
                if(i == 2){
                    stringBuilder.append(" . . .");
                }
            }
            return stringBuilder.toString();
        };
        System.out.println(intSequenceTwo.constant(1));
    }
}
//Реализуйте статический метод constant () из интерфейса IntSequence, возвращающий бесконечную последовательность
// констант.
//Например, в результате вызова IntSequence. constant (1) возвращается бесконечная последовательность 111. . ..
//В качестве дополнительного задания сделайте то же самое с помощью лямбда-выражения.