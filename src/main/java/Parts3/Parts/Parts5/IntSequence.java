package Parts3.Parts.Parts5;

public interface IntSequence {

    public static String constant(int a) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 3; i++) {
            stringBuilder.append(1);
            stringBuilder.append(" ");
            if(i == 2){
                stringBuilder.append(" . . .");
            }
        }
        return stringBuilder.toString();
    }
}