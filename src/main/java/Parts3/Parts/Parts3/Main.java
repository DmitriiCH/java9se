package Parts3.Parts.Parts3;

import javax.imageio.stream.ImageOutputStream;
import java.io.Closeable;
import java.io.DataOutput;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Serializable s = new String();
        Iterator scanner = new Scanner(System.in);
        AutoCloseable imageOutputStream = null;
    }
}
//Супертипы для String: java.io.Serializable, Comparable<String>, CharSequence
//Супертипы для Scanner: Iterator<String>, Closeable, AutoCloseable
//Супертипы для ImageOutputStream: ImageInputStream, DataOutput, DataOutput, Closeable, AutoCloseable
//Каковы все супертипы для типа String, Scanner или ImageOutputStream?
//Следует иметь в виду, что у каждого типа имеется свой супертип. Класс или интерфейс без явно объявленного супертипа