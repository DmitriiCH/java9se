package Parts3.Parts.Parts2;

import Parts3.Parts.Parts1.Measurable;

public class Employee implements Measurable {

    private double measure;
    private String name;

    public Employee(String name, double measure) {
        this.name = name;
        this.measure = measure;
    }

    @Override
    public double getMeasure() {
        return this.measure;
    }

    public String getName() {
        return name;
    }
}
