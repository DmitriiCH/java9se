package Parts3.Parts.Parts2;

import Parts3.Parts.Parts1.Measurable;

public class Main {

    public static Measurable largest(Measurable[] objects) {
        double measure = objects[0].getMeasure();
        int count = 0;
        for (int i = 1; i < objects.length; i++) {
            if (objects[i].getMeasure() > measure) {
                measure = objects[i].getMeasure();
                count = i;
            }
        }
        return objects[count];
    }

    public static void main(String[] args) {
        Measurable[] objects = new Employee[]{new Employee("Джек", 1.3), new Employee("Расел", 2.3), new Employee(
                "Кроул", 3.3)};
        System.out.println(((Employee) largest(objects)).getName());
    }
}
//Продолжите предыдущее упражнение, предоставив метод Measurable largest (Measurable [ ] objects).
//Воспользуйтесь им, чтобы выяснить имя работника с самой высокой зарплатой.
//Зачем требуется приведение типов?