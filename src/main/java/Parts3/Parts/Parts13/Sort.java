package Parts3.Parts.Parts13;

import Parts3.Parts.Parts11.Directory;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

public class Sort {

    public static void sortArray(File[] files) {
        Arrays.sort(files, ((o1, o2) -> {
            if (o1.isDirectory()) {
                if (o2.isDirectory()) {
                    return o1.getAbsolutePath().compareTo(o2.getAbsolutePath());
                } else return -1;
            } else if (o1.isFile()) {
                if (o2.isFile()) {
                    return o1.getAbsolutePath().compareTo(o2.getAbsolutePath());
                } else return 1;
            } else return 0;
        }));
    }

    public static void main(String[] args) {
        File file = new File("C:\\Users\\dmch1019\\Desktop\\Java");
        //File[] files = file.listFiles(pathname -> pathname.isFile() || pathname.isDirectory());
        File[] files = new File[11];
        files[0] = new File("C:\\Users\\dmch1019\\Desktop\\Java");
        files[1] = new File("C:\\Users\\dmch1019\\Desktop\\Java\\ArrayList vs. LinkedList.PNG");
        files[2] = new File("C:\\Users\\dmch1019\\Desktop\\atp.web.crawler-1.0.5");
        files[3] = new File("C:\\Users\\dmch1019");
        files[4] = new File("C:\\Recovery\\ReAgentOld.xml");
        files[5] = new File("C:\\Users\\dmch1019\\Desktop\\С раб стола");
        files[6] = new File("C:\\Program Files\\Synaptics\\SynTP\\Smb_driver_AMDASF.sys");
        files[7] = new File("C:\\Program Files\\Synaptics\\SynTP\\Syn3FingerFlick.wmv");
        files[8] = new File("C:\\app");
        files[9] = new File("C:\\Users\\dmch1019\\Documents\\SynKernelDiag2020-05-13_12-36-57.log");
        files[10] = new File("C:\\m2\\aopalliance");
        for (File f : files) {
            System.out.println(f.getName());
        }
        System.out.println(" ");
        sortArray(files);
        for (File f : files) {
            System.out.println(f.getAbsolutePath());
        }
    }
}
//Если задан массив объектов типа File, отсортируйте его таким образом,
//чтобы каталоги следовали перед файлами, а в каждой группе отсортируйте элементы по пути к ним.
//Воспользуйтесь лямбда-выражением, чтобы указать компаратор типа Comparator.
//Comparator возвращает int по следующей схеме:
//отрицательный int (первый объект отрицательный, то есть меньше)
//положительный int (первый объект положительный, хороший, то есть больший)
//ноль = объекты равны