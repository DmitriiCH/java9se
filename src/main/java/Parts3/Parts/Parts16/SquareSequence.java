package Parts3.Parts.Parts16;

import java.util.Random;

public class SquareSequence {

    private static Random generator = new Random();

    public static IntSequence randomlnts(int low, int high) {
        class RandomSequence implements IntSequence {

            public int next() {
                return low + generator.nextInt(high - low + 1);
            }

            public boolean hasNext() {
                return true;
            }
        }
        new RandomSequence().hasNext();
        return new RandomSequence();
    }

    private static class RandomSequence implements IntSequence {

        int low;
        int high;

        public RandomSequence(int low, int high) {
            this.low = low;
            this.high = high;
        }

        public int next() {
            return low + generator.nextInt(high - low + 1);
        }

        public boolean hasNext() {
            return true;
        }
    }

    public static void main(String[] args) {
        SquareSequence.RandomSequence randomSequence = new SquareSequence.RandomSequence(1,10);
        System.out.println(randomSequence.next());
        System.out.println(randomSequence.hasNext());
    }
}
//Реализуйте локальный класс RandomSequence, упоминавшийся в разделе 3.9.1, как вложенный класс за пределами метода randomlnts ().