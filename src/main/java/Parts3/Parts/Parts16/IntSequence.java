package Parts3.Parts.Parts16;

public interface IntSequence {
    public int next();

    public boolean hasNext();
}
