package Parts3.Parts.Parts15;

import Parts3.Parts.Parts15.ReverseSortEmployee.EmployeeNameComparatorReverse;
import Parts3.Parts.Parts15.ReverseSortEmployee.EmployeeSalaryComparatorReverse;
import Parts3.Parts.Parts15.SortEmployee.EmployeeNameComparator;
import Parts3.Parts.Parts15.SortEmployee.EmployeeSalaryComparator;

import java.util.Arrays;
import java.util.Comparator;

public class Employee {

    private String name;
    private int salary;

    public String getName() {
        return name;
    }

    public int getSalary() {
        return salary;
    }

    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return getClass() + "{" + name + ", " + salary + "}";
    }

    public static void main(String[] args) {
        Employee[] employees = new Employee[6];
        employees[0] = new Employee("Коля", 1900);
        employees[1] = new Employee("Вася", 1600);
        employees[2] = new Employee("Петя", 900);
        employees[3] = new Employee("Толя", 1700);
        employees[4] = new Employee("Джек", 1350);
        employees[5] = new Employee("Расел", 2000);
        Comparator<Employee> comparator = new EmployeeSalaryComparator().thenComparing(new EmployeeNameComparator());
        Arrays.sort(employees, comparator);
        for (Employee e : employees) {
            System.out.println(e);
        }
        System.out.println();
        Comparator<Employee> comparatorReverse =
                new EmployeeSalaryComparatorReverse().thenComparing(new EmployeeNameComparatorReverse());
        Arrays.sort(employees, comparatorReverse);
        for (Employee e : employees) {
            System.out.println(e);
        }
    }
}
//Организуйте вызов метода Arrays.sort (), сортирующего работников сначала по зарплате, а затем по имени.
//Воспользуйтесь для этой цели методом Comparator.thenComparing (). Затем организуйте сортировку в обратном порядке.