package Parts3.Parts.Parts15.ReverseSortEmployee;

import Parts3.Parts.Parts15.Employee;

import java.util.Comparator;

public class EmployeeNameComparatorReverse implements Comparator<Employee> {

    @Override
    public int compare(Employee o1, Employee o2) {
        return o2.getName().compareTo(o1.getName());
    }
}
