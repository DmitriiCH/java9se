package Parts3.Parts.Parts15.SortEmployee;

import Parts3.Parts.Parts15.Employee;

import java.util.Comparator;

public class EmployeeNameComparator implements Comparator<Employee> {

    @Override
    public int compare(Employee o1, Employee o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
