package Parts3.Parts.Parts10;

public class Run {

    public static void runTogether(Runnable... tasks) {
        for (Runnable task : tasks) {
            Thread thread = new Thread(task);
            thread.start();
        }
    }

    public static void runlnOrder(Runnable... tasks) {
        for (Runnable task : tasks) {
            task.run();
        }
    }
}
//Реализуйте следующие методы:
//public static void runTogether(Runnable... tasks) public static void runlnOrder(Runnable... tasks)
//Первый метод должен выполнять каждую задачу в отдельном потоке и возвращать полученный результат,
//а второй метод — все методы в текущем потоке и возвращать полученный результат по завершении последнего метода.