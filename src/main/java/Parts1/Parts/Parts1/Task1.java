package Parts1.Parts.Parts1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
Напишите программу, вводящую целочисленное значение и выводящую его в двоичной, восьмеричной и шестнадцатеричной
форме. Организуйте вывод обратного значения в виде шестнадцатеричного числа с плавающей точкой.
 */
public class Task1 {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        System.out.println("Двоичная: " + Integer.toBinaryString(a));
        System.out.println("Восьмеричная: " + Integer.toOctalString(a));
        System.out.println("Шестнадцатеричная : " + Integer.toHexString(a));
    }
}
