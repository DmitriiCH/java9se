package Parts1.Parts.Parts14;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/*
Напишите программу, вводящую двумерный массив целочисленных значений и определяющую, содержится ли в нем магический
квадрат (т.е. одинаковая сумма значений во всех строках, столбцах и диагоналях). Принимая строки вводимых данных,
разбивайте их на отдельные целочисленные значения, прекратив этот процесс, когда пользователь введет пустую строку.
Например, на следующие вводимые данные:
16 3 2 13
3 10 11 8
9 6 7 12
4 15 14 1
(Пустая строка)
программа должна ответить утвердительно.
 */
public class Task14 {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String[]> list = new ArrayList<>();
        while (true) {
            String s = reader.readLine();
            if (s == null || s.isEmpty()) {
                break;
            }
            list.add(s.split("\\s+"));
        }
        int indexRow = list.size();
        int indexCell = list.get(0).length;
        if(indexRow != indexCell){
            System.out.println("Вы ввели неверную фигуру");
            return;
        }
        int[][] intVar = new int[indexRow][indexCell];
        for (int i = 0; i < list.size(); i++) {
            String[] strings = list.get(i);
            for (int j = 0; j < strings.length; j++) {
                intVar[i][j] = Integer.parseInt(strings[j]);
            }
        }
        System.out.println(checkMagic(intVar));
    }

    public static boolean checkMagic(int[][] ints) {
        boolean checkRow = checkRow(ints);
        boolean checkCell = checkCell(ints);
        boolean checkDiagonal = checkDiagonal(ints);
        return checkRow && checkCell && checkDiagonal;
    }

    public static boolean checkRow(int[][] ints) {
        int countNumber = 0;
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < ints.length; i++) {
            for (int j = 0; j < ints[i].length; j++) {
                countNumber += ints[i][j];
            }
            set.add(countNumber);
            countNumber = 0;
        }
        return set.size() == 1;
    }

    public static boolean checkCell(int[][] ints) {
        int countNumber = 0;
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < ints[0].length; i++) {
                countNumber += ints[0][i];

            set.add(countNumber);
            countNumber = 0;
        }
        return set.size() == 1;
    }

    public static boolean checkDiagonal(int[][] ints) {
        int countNumber = 0;
        int countNumber1 = 0;
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < ints.length; i++) {
            countNumber += ints[i][i];
            countNumber1 += ints[i][ints.length - 1 - i];
        }
        set.add(countNumber);
        set.add(countNumber1);
        return set.size() == 1;
    }
}
