package Parts1.Parts.Parts11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
Напишите программу, вводящую текстовую строку и выводящую все символы, не представленные в коде ASCII, вместе с их значениями в Юникоде.
 */
public class Task11 {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        char[] chars = s.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            int intVar = chars[i];
            if(intVar > 128){
                System.out.println("Код символа " + intVar + ", значение в Юникоде " + chars[i]);
            }
        }
    }
}
