package Parts1.Parts.Parts3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
Напишите программу, вводящую три целочисленных значения и выводящую самое большое из них, используя только условную
операцию. Сделайте то же самое с помощью метода Math.шах ().
 */
public class Task3 {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());
        int maxAB = a > b ? a : b;
        System.out.println(maxAB > c ? maxAB : c);
        // через Math.max()
        int mathMaxAB = Math.max(a,b);
        System.out.println(Math.max(mathMaxAB,c));
    }
}
