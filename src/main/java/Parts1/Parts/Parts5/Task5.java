package Parts1.Parts.Parts5;
/*
Что произойдет, если привести числовое значение типа double к значению типа int,
которое больше самого большого значения типа int? Попробуйте сделать это.
 */
public class Task5 {

    public static void main(String[] args) {
        double d = Double.MAX_VALUE;
        int i = (int) d;
        System.out.println(d);
        System.out.println(i);
        System.out.println("Max int = " + Integer.MAX_VALUE);
    }
}
