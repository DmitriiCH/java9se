package Parts1.Parts.Parts4;
/*
Напишите программу, выводящую наименьшее и наибольшее положительные значения типа double.
Подсказка: воспользуйтесь методом Math.nextUp () из прикладного программного интерфейса Java API.
 */
public class Task4 {

    public static void main(String[] args) {
        System.out.println(Math.nextUp(1.3));
        System.out.println(Math.nextDown(0.1));
        System.out.println(Math.nextUp(Double.MAX_VALUE));
    }
}
