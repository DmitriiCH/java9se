package Parts1.Parts.Parts13;

import java.util.*;

/*
Напишите программу, выбирающую и выводящую лотерейную комбинацию из шести отдельных чисел в пределах от 1 до 49.
Чтобы выбрать шесть отдельных чисел, начните со списочного массива, заполняемого числами от 1 до 49.
Выберите произвольный индекс и удалите элемент массива.
Повторите эти действия шесть раз подряд. Выведите полученный результат в отсортированном порядке.
 */
public class Task13 {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i < 50; i++) {
            list.add(i);
        }
        Random random = new Random();
        int[] result = new int[6];
        for (int i = 0; i < 6; i++) {
            int index = random.nextInt(49) +  1;
            result[i] = list.get(index);
        }
        removeIndexInList(list,result);
        Arrays.sort(result);
        System.out.println(Arrays.toString(result));
    }

    public static void removeIndexInList(List<Integer> list, int[] ints){
        for (int i = 0; i < ints.length; i++) {
            list.remove((Integer) ints[i]);
        }
        //removeAll(Arrays.asList(ints));
    }
}
