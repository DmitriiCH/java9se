package Parts1.Parts.Parts15;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/*
Напишите программу, сохраняющую треугольник Паскаля вплоть до заданной величины п в переменной типа
ArrayList<ArrayList<Integer».
 */
public class Task15 {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        ArrayList<ArrayList<Integer>> arrayList = genList(n);
        for (int i = 0; i < arrayList.size(); i++) {
            for (int j = 0; j < arrayList.get(i).size(); j++) {
                System.out.print(arrayList.get(i).get(j) + " ");
            }
            System.out.println();
        }
    }

    public static ArrayList<ArrayList<Integer>> genList(int numRows) {
        ArrayList<ArrayList<Integer>> list = new ArrayList<>();
        if (numRows == 0) {
            return list;
        }
        for (int i = 0; i < numRows; i++){
            list.add(new ArrayList<>());
        }
        list.get(0).add(1);
        for (int i = 1; i < numRows; i++) {
            list.get(i).add(1);
            for (int j = 1; j < i; j++){
                list.get(i).add(list.get(i - 1).get(j) + list.get(i - 1).get(j - 1));
            }
            list.get(i).add(1);
        }
        return list;
    }
}