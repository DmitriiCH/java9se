package Parts1.Parts.Parts12;

/*
В состав комплекта разработки Java Development Kit входит архивный файл src. zip с исходным кодом библиотеки Java.
Разархивируйте этот файл и с помощью избранного вами инструментального средства для поиска текста
найдите в этом исходном коде примеры применения последовательностей операторов break и continue с меткой.
Выберите один из этих примеров и перепишите его без оператора с меткой.
 */
public class Task12 {

    public static void main(String[] args) {
        label:
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (j > i) {
                    System.out.println();
                    continue label;
                }
                System.out.print(" " + (i * j));
            }
        }
        System.out.println();
        nonLabels();
    }

    public static void nonLabels() {
//        label:
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (j > i) {
                    System.out.println();
                    break;
                }
                System.out.print(" " + (i * j));
            }
        }
        System.out.println();
    }
}
