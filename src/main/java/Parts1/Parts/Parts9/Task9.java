package Parts1.Parts.Parts9;

/*
В разделе 1.5.3 был приведен пример сравнения двух символьных строк s и t при вызове метода s. equals (t), но не с
помощью операции s != t. Придумайте другой пример, в котором не применяется метод substring ().
 */
public class Task9 {

    public static void main(String[] args) {
        String s = "Hello World!";
        String h = "Hello";
        String w = " World!";

        String result = h.concat(w);
        System.out.println(s == result);
        System.out.println(s.equals(result));
    }
}
