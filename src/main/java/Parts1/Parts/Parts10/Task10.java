package Parts1.Parts.Parts10;

import java.io.IOException;
import java.util.Random;

/*
Напишите программу, составляющую произвольную символьную строку из букв и цифр, генерируя произвольное значение типа
long и выводя его по основанию 36.
 */
public class Task10 {

    public static void main(String[] args) throws IOException {
        Long x = rnd();
        System.out.println(getСipher(x));
    }

    public static String getСipher(Long value) {
        value = Math.abs(value);
        StringBuilder rezult = new StringBuilder();
        while (value >= 36) {
            rezult.append(getChar((int) (value % 36)));
            value /= 36;
            if (value < 36) rezult.append(getChar((int) (value % 36)));
        }
        return rezult.toString();
    }

    public static char getChar(int value) {
        char[] simbols = {'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к',
                'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц',
                'ч', 'ш', '9', '8', '7', '6', '5', '4', '3', '2', '1', '0'};
        return simbols[value];
    }

    private static Long rnd() {
        Random random = new Random();
        return random.nextLong();
    }
}