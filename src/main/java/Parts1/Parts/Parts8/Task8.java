package Parts1.Parts.Parts8;

import java.util.Scanner;

/*
Напишите программу, вводящую символьную строку и выводящую все ее непустые подстроки.
 */
public class Task8 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        String[] strings = s.trim().split("\\s+");
        for (String stringVar : strings) {
            System.out.print(stringVar);
        }
    }
}