package Parts1.Parts.Parts7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
Напишите программу, вводящую два числа в пределах от 0 до 65535,
сохраняющую их в переменных типа short и вычисляющую их сумму, разность, произведение,
частное и остаток без знака, не преобразуя эти величины в тип int.
 */
public class Task7 {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String message = "Введите 2 числа в пределах от 0 до 65535";
        System.out.println(message);
        short s = Short.parseShort(reader.readLine());
        short s1 = Short.parseShort(reader.readLine());
        if (s < 0 || s1 < 0) {
            System.out.println(message);
        } else {
            System.out.println(s + s1);
            System.out.println(s - s1);
            System.out.println(s * s1);
            System.out.println(s / s1);
            System.out.println(s % s1);
        }
    }
}
