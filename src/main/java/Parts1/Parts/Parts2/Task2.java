package Parts1.Parts.Parts2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
Напишите программу, вводящую целочисленное (как положительное, так и отрицательное) значение угла и нормализующую его
 в пределах от 0 до 359 градусов.
Попробуйте сделать это сначала с помощью операции %, а затем метода floorMod().
 */
public class Task2 {

//    public static void main(String[] args) throws IOException {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        int adjustment = Integer.parseInt(reader.readLine());
//
//        System.out.println(normalize360(adjustment));
//        System.out.println(Math.floorMod(adjustment, 360));
//    }
//
//    public static double normalize360(double angle) {
//        angle = angle % 360;
//        if (angle < 0) {
//            angle = angle + 360;
//        }
//        return angle;
//    }

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.println(nonFloorMod(Integer.parseInt(reader.readLine())));
            System.out.println(Math.floorMod(Integer.parseInt(reader.readLine()), 360));
        }
    }

    public static String nonFloorMod(int input) {
        if (input == 360 || input == 0) {
            return "0";
        }
        if (input > 360) {
            return input % 360 + "";
        }
        if (input < 0) {
            return 360 + (input % 360) + "";
        }
        return input + "";
    }
}
