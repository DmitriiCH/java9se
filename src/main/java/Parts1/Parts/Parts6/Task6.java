package Parts1.Parts.Parts6;

import java.math.BigInteger;

/*
Напишите программу, вычисляющую факториал п! = 1 х 2 * . . . х п, используя класс Biglnteger.
Вычислите факториал числа 1000.
 */
public class Task6 {

    public static void main(String[] args) {
        BigInteger bigInteger = BigInteger.ONE;
        for (int i = 1; i <= 1000; i++) {
            bigInteger = bigInteger.multiply(BigInteger.valueOf(i));
        }
        System.out.println(bigInteger);
    }
}
