package Parts1.Ex;

import java.util.ArrayList;
import java.util.Arrays;

public class T1_8_6 {

    public static void main(String[] args) {
        int[] numbers = new int[10];
        int[] primes = numbers;
        ////////
        int[] copiedPrimes = Arrays.copyOf(numbers,numbers.length);
        ////////////////////////////////////////////////////////////////
        ArrayList<String> people = new ArrayList<>();
        ArrayList<String> friends = people;
        ////////
        ArrayList<String> copiedPepople = new ArrayList<>(people);
        ////////
        String[] strings = new String[10];
        ArrayList<String> stringArrayList = new ArrayList<>(Arrays.asList(strings));
        ////////
        String[] names = stringArrayList.toArray(new String[0]);
    }
}
