package Parts1.Ex;

public class T1_9_3 {

    public static void main(String[] args) {
        double doubleVar = average(1.0, 5.6);
        System.out.println(doubleVar);
        //
        double[] scores = {3, 4.5, 10, 0};
        double avg = average(scores);
        System.out.println(avg);
        //
        System.out.println(max(1.0, 3.6, 5.4, 9.8, 7.6));
    }

    public static double average(double... values) {
        double sum = 0;
        for (double v : values) {
            sum += v;
        }
        return values.length == 0 ? 0 : sum / values.length;
    }

    public static double max(double first, double... rest) {
        double result = first;
        for (double v : rest) {
            result = Math.max(v, result);
        }
        return result;
    }
}
