package Parts1.Ex;

import java.util.Arrays;

public class T1_5_2 {

    public static void main(String[] args) {
        String greeting = "Hello, World!";
        String location = greeting.substring(7,12);
        System.out.println(location);
        //////////////////////////////
        String names = "Peter, Paul, Mary";
        String[] result = names.split(",");
        System.out.println(Arrays.toString(result));
        //////////////////////////////
        String names1 = "Peter, Paul, Mary";
        String[] result1 = names.split("\\s+");
        System.out.println(Arrays.toString(result1));
    }
}
