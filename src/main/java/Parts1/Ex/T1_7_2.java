package Parts1.Ex;

import java.util.Random;

public class T1_7_2 {

    public static void main(String[] args) {
        Random random = new Random();
        int sum = 0, target = 9, count = 0;
        while (sum < target) {
            int nextVar = random.nextInt(10);
            sum += nextVar;
            count++;
        }
        System.out.println(sum);
        ////////////////////////////////////////
        int intVar;
        do {
            intVar = random.nextInt(10);
            count++;
        } while (intVar < target);
        System.out.println(intVar);
        ////////////////////////////////////////
        for (int i = 0; i <= 20; i++) {
            int a = random.nextInt(10);
            sum += a;
        }
        ////////////////////////////////////////
        for (int i = 0; i < 6; i++) {
            if(i == 1) continue;
            System.out.println(i);
        }
    }
}
