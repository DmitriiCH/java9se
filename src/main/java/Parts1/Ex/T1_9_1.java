package Parts1.Ex;

public class T1_9_1 {

    public static void main(String[] args) {
        System.out.println(average(1.0,2.5));
    }

    public static double average(double x, double y){
        double sum = x + y;
        return sum / 2;
    }
}
