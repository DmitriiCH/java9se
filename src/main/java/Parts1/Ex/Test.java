package Parts1.Ex;

import java.util.*;

public class Test {

    public static void main(String[] args) {
        int[] intVar = {1, 2, 3, 4, 5, 6};
        int[] intVar1 = Arrays.copyOf(intVar, intVar.length);
        System.out.println(Arrays.toString(intVar1));
        //
        int[] intVar2 = Arrays.copyOf(intVar, 3);
        System.out.println(Arrays.toString(intVar2));
        //
        int[] intVar3 = Arrays.copyOfRange(intVar, 1, 3);
        System.out.println(Arrays.toString(intVar3));
        //
        System.out.println(Arrays.equals(intVar1, intVar));
        ////////////////////////////////////////////////////////////
        int[][] intVars = {{1, 2}, {3, 4}};
        int[][] intVars1 = Arrays.copyOf(intVars, intVars.length);
        System.out.println(Arrays.deepEquals(intVars, intVars1));
        System.out.println(Arrays.deepToString(intVars1));
        ////////////////////////////////////////////////////////////
        String[] strings = {"1", "2", "3", "4", "5", "6"};
        ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(strings));
        System.out.println("arrayList " + arrayList);
        //
        String[] strings1 = arrayList.toArray(new String[0]);
        System.out.println("strings1 " + Arrays.toString(strings1));
        //
        Iterator<String> iterator = arrayList.iterator();
        while (iterator.hasNext()) {
            String text = iterator.next();
            if ("3".equals(text)) {
                iterator.remove();
            } else{
                System.out.print("Iterator: " + text);
                System.out.print(" ");
            }
        }
        ////////////////////////////////////////////////////////////
        Date date = new Date();
        System.out.println(date.getTime());
    }
}