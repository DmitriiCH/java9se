package Parts1.Ex;

import java.util.Arrays;

public class T1_8_9 {

    public static void main(String[] args) {
        int[][] square = {{16, 3, 2, 13},
                {3, 10, 11, 8},
                {9, 6, 7, 12},
                {4, 15, 14, 1}
        };
        int elemetns = square[1][2];
        System.out.println(elemetns);
        System.out.println(square[2][1]);
        //
        int[][] square1 = new int[4][4];
        //Сначала указываются строки
        //а затем столбцы многомерного массива
        System.out.println(Arrays.toString(square[0]));
        //
        for (int i = 0; i < square.length; i++) {
            for (int j = 0; j < square[i].length; j++) {
                System.out.print(square[i][j]);
                System.out.print(" ");
            }
            System.out.println();
        }
        //
        for (int[] row : square) {
            for (int element : row) {
                System.out.println(element);
            }
        }
        //
        System.out.println(Arrays.deepToString(square));
    }
}
