package Parts1.Ex;

public class T1_2_2 {

    public static void main(String[] args) {
        double POSITIVE_INFINITY = Double.POSITIVE_INFINITY;
        double NEGATIVE_INFINITY = Double.NEGATIVE_INFINITY;
        double NaN = Double.NaN;
        if(Double.isInfinite(POSITIVE_INFINITY)){
            System.out.println("POSITIVE_INFINITY");
        }
        if(Double.isFinite(NEGATIVE_INFINITY)){
            System.out.println("NEGATIVE_INFINITY");
        }
        if(Double.isNaN(NaN)){
            System.out.println("NaN");
        }
    }
}
