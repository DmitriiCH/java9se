package Parts1.Ex;

import java.math.BigDecimal;
import java.math.BigInteger;

public class T1_4_6 {

    public static void main(String[] args) {
        BigInteger bigInteger = new BigInteger("233232232");//целочисленные числа
        BigDecimal bigDecimal = new BigDecimal(44545);//числа с плавающей точкой
        BigInteger zero = BigInteger.ZERO;
        BigInteger one = BigInteger.ONE;
        BigInteger ten = BigInteger.TEN;
        //////////////////////////////////////
        BigInteger n = BigInteger.valueOf(876543210123456789L);
        BigInteger k = new BigInteger("9876543210123456789");
        BigInteger r = BigInteger.valueOf(5).multiply(ten.add(k));//r = 5 * (n + k)
    }
}
