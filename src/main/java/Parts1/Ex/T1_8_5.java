package Parts1.Ex;

import java.util.ArrayList;

public class T1_8_5 {

    public static void main(String[] args) {
        int[] numbers = new int[10];
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        //////
        for (int n : numbers) {
            sum += n;
        }
        //////
        ArrayList<String> arrayList = new ArrayList<>();
        for (String name : arrayList) {
            System.out.println(name);
        }
    }
}
