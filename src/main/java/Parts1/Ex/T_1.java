package Parts1.Ex;

import java.util.Random;

public class T_1 {

    public static void main(String[] args) {
        int i = 131;
        byte b = (byte) i;
        System.out.println(b);
        System.out.println(Byte.MIN_VALUE + " " + Byte.MAX_VALUE);
        //////////////////////
        int length = "Hello, world! ".length();
//        System.out.println(length);
        //////////////////////////////
        System.out.println(new Random().nextInt());
        Random generator = new Random();
        //////////////////////////////
        System.out.println(generator.nextInt());
        System.out.println(generator.nextInt());
        //////////////////////////////
        byte valueByte = -128; //127
        short valueShort = -32768; //32767
        int valueInt = -2147483648; //214748647
        long valueLong = -9223372036854775808L; //9223372036854775807
        //////////////////////////////
        System.out.println(Integer.MAX_VALUE + " <=int.MAX_VALUE and int.MIN_VALUE=> " + Integer.MIN_VALUE);
        System.out.println(Byte.MAX_VALUE + " <=byte.MAX_VALUE and byte.MIN_VALUE=> " + Byte.MIN_VALUE);
        System.out.println(Short.MAX_VALUE + " <=short.MAX_VALUE and short.MIN_VALUE=> " + Short.MIN_VALUE);
        System.out.println(Long.MAX_VALUE + " <=long.MAX_VALUE and long.MIN_VALUE=> " + Long.MIN_VALUE);
        System.out.println("Hello, world!".length());
    }
}
