package Parts1.Ex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class T8_7 {

    public static void main(String[] args) {
        //заполнение обычного и списочного массива
        int[] numbers = new int[10];
        Arrays.fill(numbers, 0);
        //
        ArrayList<String> friends = new ArrayList<>();
        Collections.fill(friends, "Paul");
        //Сортировка массивов
        Arrays.sort(numbers);
        Collections.sort(friends);
        //Строковое представление массивов
        String number = Arrays.toString(numbers);
        String people = friends.toString();
        System.out.println(number + "\n" + people);
        //
        Collections.reverse(friends);
        //Обращает элементы списочного массива
        Collections.shuffle(friends);
        //Перетасовывает элементы в произвольной форме
    }
}
