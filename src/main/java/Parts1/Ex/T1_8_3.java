package Parts1.Ex;

import java.util.ArrayList;

public class T1_8_3 {

    public static void main(String[] args) {
        ArrayList<String> friends;
        friends = new ArrayList<>();
        friends.add("Peter");
        friends.add("Paul");
        ////////
        friends.remove(1);
        friends.add(0,"Paul");
        ////////
        String first = friends.get(0);
        System.out.println(first);
        friends.set(0,"Mary");
        ////////
        for (int i = 0; i < friends.size(); i++) {
            System.out.println(friends.get(i));
        }
    }
}
