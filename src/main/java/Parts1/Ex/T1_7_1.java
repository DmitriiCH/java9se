package Parts1.Ex;

public class T1_7_1 {

    public static void main(String[] args) {
        int count = 0;
        String output;
        switch (count) {
            case 0:
                output = "None";
                break;
            case 1:
                output = "One";
                break;
            case 2:
            case 3:
            case 4:
            case 5:
                output = Integer.toString(count);
                break;
            default:
                output = "Many";
                break;
        }
        System.out.println(output);
    }
}
