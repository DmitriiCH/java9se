package Parts14.Ex.Ex14_2_4;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Main {

    public static void main(String[] args) throws ScriptException, NoSuchMethodException {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");
        //////
        engine.eval("function greet(how, whom) { return how + ', ' + whom + '!' }");
        Invocable invocable = (Invocable) engine;
        Object result = invocable.invokeFunction("greet", "Hello", "World");
        System.out.println(result + "\n");
        /////////////////////////////////////////
        ScriptEngineManager maneger1 = new ScriptEngineManager();
        ScriptEngine engine1 = maneger1.getEngineByName("nashorn");
        engine1.eval("function Greeter(how) { this.how = how }");
        engine1.eval("Greeter.prototype.welcome = function(whom) { return this.how + ', ' + whom + '!' }");
        Object yo = engine1.eval("new Greeter('yo')");
        Invocable invocable1 = (Invocable) engine1;
        Object result1 = invocable1.invokeMethod(yo,"welcome","World");
        System.out.println(result1 + "\n");
        /////////////////////////////////////////
        ScriptEngineManager maneger2 = new ScriptEngineManager();
        ScriptEngine engine2 = maneger1.getEngineByName("nashorn");
        engine2.eval("function welcome(whom) { return 'Hello, ' + whom + '!' }");
        Greeter g = ((Invocable) engine2).getInterface(Greeter.class);
        Object result2 = g.welcome("World");
        System.out.println(result2);
    }
}
