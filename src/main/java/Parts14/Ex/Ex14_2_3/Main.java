package Parts14.Ex.Ex14_2_3;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.StringWriter;

public class Main {

    public static void main(String[] args) throws ScriptException {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");
        StringWriter writer = new StringWriter();
        engine.getContext().setWriter(writer);
        engine.eval("print('Hello')");
        String result = writer.toString();
        System.out.println(result);
    }
}
