package Parts14.Ex.Ex14_2_1;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Main {

    public static void main(String[] args) throws ScriptException {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");
        engine.eval("n = 1728");
        Object result = engine.eval("n + 1");
        System.out.println(result);
        System.out.println(engine.getFactory().getParameter("THREADING"));
    }
}
