package Parts14.Ex.Ex14_2_2;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Main {

    public static void main(String[] args) throws ScriptException {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");
        engine.put("k",1728);
        System.out.println(engine.eval("k + 1"));
        //////
        engine.eval("n = 1728");
        System.out.println(engine.get("n"));
        //////
        Bindings scope = engine.createBindings();
        scope.put("p", 1728);
        Object result = engine.eval("p + 1", scope);
        System.out.println(result);
    }
}
