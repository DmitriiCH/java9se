package Parts14.Parts.Parts11;

/*
Напишите сценарий nextYear.js для получения возраста пользователя
и вывода результата "Next year, you will be ..." (В следующем году вам будет ...)
с прибавлением 1 к введенной величине возраста пользователя.
Возраст может быть указан в режиме командной строки или в переменной окружения AGE.
Если же он так или иначе отсутствует, то предложите пользователю ввести его.

Чтобы запустить скрипт в windows заходим в cmd.exe
cd (путь к jjs.exe)
Принцип запуска такой: jjs [options] [script-files] [-- arguments]
Нам нужно запустить в режиме -scripting что бы отрабатываели некоторые команды
в файле скрипта в частности - $ARG[0].
jjs -scripting C:\Users\dmch1019\IdeaProjects\Java9SE\src\main\java\Parts14\Parts\Parts11\script.js  -- 10

jjs -scripting script.js -- 10 тут аргумент это возраст по условию задачи.

код скрипта:

var age = $ARG[0];
проверяем значение в аргументе, если null то выводим сообщение, с просьбой указать аргумент
if (age == null) {
	print('Enter age as argument')
}
else {
	age = +$ARG[0] + 1;
	print ('Next year, to you will be ' + age)
}
 */
public class Main {
}
