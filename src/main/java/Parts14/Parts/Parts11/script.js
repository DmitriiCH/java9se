var age = $ARG[0];
if (age == null) {
	print('Enter age in startup arguments.js')
}
else {
	age = +$ARG[0] + 1;
	print ('Next year, you will be ' + age)
}