package Parts14.Parts.Parts7;

import java.math.BigInteger;

/*
Выполните утилиту jjs и сделайте следующий вызов:
var b = new java.math.BigInteger('1234567890987654321')
Затем отобразите содержимое переменной b, просто введя b
и нажав клавишу <Enter>. Что вы получите в итоге? Какое значение возвращается
в результате вызова b.mod(java.math.BigInteger.TEN)? Почему значение переменной b
отображается столь необычно? Как отобразить фактическое зн переменной b?

при вводе в js кода
var b = new java.math.BigInteger('1234567890987654321')
b
я получаю значение 1234567890987654321
при вызове b.mod(java.math.BigInteger.TEN) - возвращается число 1.
Выводы сверил с выводом из java - все отрабатывает верно.
 */
public class Main {

    public static void main(String[] args) {
        BigInteger b = new BigInteger("1234567890987654321");
        System.out.println(b);
        System.out.println(b.mod(BigInteger.TEN));
    }
}

