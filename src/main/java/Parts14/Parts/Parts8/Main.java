package Parts14.Parts.Parts8;

/*
В конце раздела 14.3.9 было показано, каким образом можно расширить класс ArrayList,
чтобы протоколировать каждый вызов метода add (). Но это подходит только для одного объекта.
Напишите функцию JavaScript, которая должна служить фабрикой для таких объектов,
позволяя формировать любое количество списочных массивов для протоколирования.


С помощью этой функции создается массив с переопределенным методом add, логирующим в консоль
function createList () {
	var arr = new (Java.extend (java.util.ArrayList)) {
		add: function(x) {
		print ('Adding ' + x);
		return Java.super(arr).add(x)
		}
	}
	return arr;
}

Получить обьъект :
var arr = new java.util.ArrayList (памятка)

Фабрика по созданию массивов (возвращает массив длинной заданной в параметре метода)
function factoryArrayList (count) {
	var arrLists = [];
	for (var i = 0; i < count; i++){
		arrLists[i] = createList(); - создает объект ArrayList
	}
	return arrLists;
}

для проверки результата:
var list1 = factoryArrayList(3)
list1[0].add(5)
list1[1].add(15)
list1[2].add(25)

var list2 = list1[0]
list2.add(10)

var list3 = list1[1]
list3.add(5)

print ('list1 length - '  + list1.length + ', list1[0] - ' + list1[0] + ', list1[1] - ' + list1[1] +', list1[2] - ' + list1[2])
print ('list2 length - '  + list2.length + ', list2[0] - ' + list2[0]  + ', list2[1] - ' + list2[1])
print ('list3 length - '  + list3.length + ', list3[0]  - ' + list3[0]  + ', list3[1] - ' + list3[1])

jjs -scripting C:\Users\dmch1019\IdeaProjects\Java9SE\src\main\java\Parts14\Parts\Parts8\script.js
 */
public class Main {
}
