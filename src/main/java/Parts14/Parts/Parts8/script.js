function createList () {
	var arr = new (Java.extend (java.util.ArrayList)) {
		add: function(x) {
		print ('Added ' + x);
		return Java.super(arr).add(x)
		}
	}
	return arr;
}

function factoryArrayList (count) {
	var arrLists = [];
	for (var i = 0; i < count; i++){
		arrLists[i] = createList();
	}
	return arrLists;
}

var list1 = factoryArrayList(3)
list1[0].add(5)
list1[1].add(15)
list1[2].add(25)

var list2 = list1[0]
list2.add(10)

var list3 = list1[1]
list3.add(5)

print ('list1 length - '  + list1.length + ', list1[0] - ' + list1[0] + ', list1[1] - ' + list1[1] +', list1[2] - ' + list1[2])
print ('list2 length - '  + list2.length + ', list2[0] - ' + list2[0]  + ', list2[1] - ' + list2[1])
print ('list3 length - '  + list3.length + ', list3[0]  - ' + list3[0]  + ', list3[1] - ' + list3[1])