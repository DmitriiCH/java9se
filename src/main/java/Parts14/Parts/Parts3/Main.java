package Parts14.Parts.Parts3;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.Arrays;

/*
Насколько целесообразна компиляция средствами Nashorn? Напишите сценарий JavaScript
для сортировки массива простейшим способом перестановки до тех пор, пока элементы массива не
будут отсортированы. Сравните время выполнения компилируемой и интерпретируемой версий этого
сценария.
 */
public class Main {

    public static void main(String[] args) throws ScriptException {
        long timeJS = sortJS();
        long timeJava = sortJava();
        System.out.printf("Время сортировки используя механизм сценария JavaScript (nashorn): %s мс\n" +
                "время обычной сортировки на Java: %s мс", timeJS, timeJava);
    }

    //  Синтаксис создания массива и сортировка в JS
    public static Long sortJS() throws ScriptException {
        long start = System.currentTimeMillis();
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");
//      Массив в JS
        engine.eval("var arr = [2, 4, 9, 3, 1, 5, 7, 6, 8]");
//      Сортирую массив методом пузырька (по условию)
        engine.eval("for (var i = 0; i < arr.length; i++) {" +
                "for (var j = i; j < arr.length; j++) {" +
                "if (arr[i] > arr[j]) {" +
                "var k = arr[i];" +
                "arr[i] = arr[j];" +
                "arr[j] = k;}}}");
        return System.currentTimeMillis() - start;
    }

    public static Long sortJava() throws ScriptException {
        long start = System.currentTimeMillis();
        int[] arr = {2, 4, 9, 3, 1, 5, 7, 6, 8};
        for (int i = 0; i < arr.length; i++) {
            for (int j = i; j < arr.length; j++) {
                if (arr[i] > arr[j]) {
                    int k = arr[i];
                    arr[i] = arr[j];
                    arr[j] = k;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
        return System.currentTimeMillis() - start;
    }
}

