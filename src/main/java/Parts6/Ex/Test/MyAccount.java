package Parts6.Ex.Test;

public class MyAccount<T> {

    private T id;
    private int sum;

    public MyAccount(T id, int sum) {
        this.id = id;
        this.sum = sum;
    }

    public T getId() {
        return id;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }
}

class MyProgram {

    public static void main(String[] args) {
        MyAccount<String> acc1 = new MyAccount<>("2345", 5000);
        String acc1Id = acc1.getId();
        System.out.println(acc1Id);
        //
        MyAccount<Integer> acc2 = new MyAccount<Integer>(2345, 5000);
        Integer acc2Id = acc2.getId();
        System.out.println(acc2Id);
    }
}