package Parts6.Ex.Test;

public class Main {

    public static void main(String[] args) {
        Body body = new Body();
        //
        SmallHead smallHead = new SmallHead();
        MediumHead mediumHead = new MediumHead();
        BigHead bigHead = new BigHead();
        Head head = new Head();
        //
        Robot<Head> robot1 = new Robot(body,head);
        Robot<SmallHead> robot = new Robot<SmallHead>(body, smallHead);
        robot.getHead().burn();

        Robot<BigHead> robot2 = new Robot<BigHead>(body,bigHead);
        robot2.getHead().turn();

    }
}
