package Parts6.Ex.Test;

public class MyAccount1 implements Accountable<String>{

    private String id;
    private int sum;

    MyAccount1(String id, int sum){
        this.id = id;
        this.sum = sum;
    }

    public String getId() { return id; }
    public int getSum() { return sum; }
    public void setSum(int sum) { this.sum = sum; }
}

interface Accountable<T>{
    T getId();
    int getSum();
    void setSum(int sum);
}

class Program3{

    public static void main(String[] args) {

        Accountable<String> acc1 = new MyAccount1("1235rwr", 5000);
        Account acc2 = new Account("2373", 4300);
        System.out.println(acc1.getId());
        System.out.println(acc2.getId());
    }
}