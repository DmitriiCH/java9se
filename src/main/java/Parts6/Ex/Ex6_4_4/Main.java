package Parts6.Ex.Ex6_4_4;

import java.util.ArrayList;

public class Main {

    public static boolean hasNulls(ArrayList<?> elements) {
        for (Object o : elements) {
            if (o == null) {
                return true;
            }
        }
        return false;
    }
}
