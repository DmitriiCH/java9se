package Parts6.Ex.Ex6_3;

import java.io.PrintStream;
import java.util.ArrayList;

public class MyArrayList {

    public static <T extends AutoCloseable> void closeAll(ArrayList<T> elems) throws Exception {
        for (T elem : elems) {
            elem.close();
        }
    }

    public static <T extends Runnable & AutoCloseable> void closeAll2(ArrayList<T> elems) throws Exception {
        for (T elem : elems) {
            elem.close();
        }
    }
}

class Main {

    public static void main(String[] args) {
        try {
            ArrayList<PrintStream> list = new ArrayList();
            MyArrayList.closeAll(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}