package Parts6.Ex.Ex6_4_1;

import Parts6.Ex.Ex6_4.Employee;
import Parts6.Ex.Ex6_4.Manager;

import java.util.ArrayList;

public class Main {

    public static void printNames(ArrayList<? extends Employee> staff) {
        for (int i = 0; i < staff.size(); i++) {
            Employee e = staff.get(i);
            System.out.println(e.getName());
        }
    }

    public static void main(String[] args) {
        ArrayList<Manager> list = new ArrayList<>();
        list.add(new Manager("1", "Петя"));
        list.add(new Manager("2", "Коля"));
        list.add(new Manager("3", "Даша"));
        printNames(list);
    }
}
