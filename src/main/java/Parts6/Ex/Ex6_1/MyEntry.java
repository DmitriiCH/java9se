package Parts6.Ex.Ex6_1;

public class MyEntry<K, V> {

    private K key;
    private V value;

    public MyEntry(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }
}

class Main {

    public static void main(String[] args) {
        MyEntry<String, Integer> myEntry = new MyEntry<>("Ключ", 66);
        System.out.printf("Ключ: \"%s\", значение: \"%d\"", myEntry.getKey(), myEntry.getValue());
    }
}