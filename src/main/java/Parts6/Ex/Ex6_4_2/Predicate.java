package Parts6.Ex.Ex6_4_2;

public interface Predicate<T> {

    boolean test(T args);
}
