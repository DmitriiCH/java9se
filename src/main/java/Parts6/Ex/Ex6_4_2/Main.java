package Parts6.Ex.Ex6_4_2;

import Parts6.Ex.Ex6_4.Employee;

public class Main {

    public static void printAll(Employee[] staff, Predicate<? super Employee> filter) {
        for (Employee e : staff) {
            if (filter.test(e)) {
                System.out.println(e.getName());
            }
        }
    }
}
