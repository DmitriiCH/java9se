package Parts6.Ex.Ex6_4_5;

import java.util.ArrayList;

public class Main {

    public static void swap(ArrayList<?> elem, int i, int j) {
        swapHelper(elem, i, j);
    }

    public static <T> void swapHelper(ArrayList<T> elem, int i, int j) {
        T temp = elem.get(i);
        elem.set(i, elem.get(j));
        elem.set(j, temp);
    }
}
