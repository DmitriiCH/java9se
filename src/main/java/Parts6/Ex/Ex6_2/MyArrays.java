package Parts6.Ex.Ex6_2;

import java.util.Arrays;

public class MyArrays {

    public static <T> void swap(T[] array, int i, int j) {
        T temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

class Main {

    public static void main(String[] args) {
        String[] strings = new String[]{"один", "два", "три"};
        MyArrays.<String>swap(strings, 0, 2);
        System.out.println(Arrays.toString(strings));
    }
}