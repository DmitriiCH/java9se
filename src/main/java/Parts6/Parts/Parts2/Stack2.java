package Parts6.Parts.Parts2;

import java.util.Arrays;

/*
Еще раз реализуйте обобщенный класс Stack<E>, используя массив для хранения элементов. Если требуется, нарастите массив в методе push ().
Предоставьте два решения этой задачи: одно — с массивом типа Е [ ], другое — с массивом типа Object [ ].
Оба решения должны компилироваться без всяких предупреждений. Какое из них вы предпочтете сами и почему?
 */
public class Stack2<E> {

    private Object[] elements = new Object[1];
    private int size = 0;

    public void push(E element) {
        if (size > 0 && elements[elements.length - 1] != null) {
            elements = Arrays.copyOf(elements, size + 5);
        }
        elements[size] = element;
        size++;
    }

    public E pop() {
        E element = (E) elements[size - 1];
        elements = Arrays.copyOf(elements, size - 1);
        size--;
        return element;
    }

    public boolean isEmpty() {
        return size == 0;
    }
}