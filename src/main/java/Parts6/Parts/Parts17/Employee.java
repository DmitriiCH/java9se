package Parts6.Parts.Parts17;

/*

Определите класс Employee, реализующий интерфейс Comparable<Employee>.
Используя утилиту javap, продемонстрируйте, что мостовой метод был синтезирован.
Каково его назначение?

Для того чтобы это продемонстрировать, нужно через командную строку скомпилировать класс командой javac Employee.java.
Затем выполнить команду javap Employee.class
В результате вывод в командную строку будет следующим :


Compiled from "Employee.java"
public class Parts6.Parts.Parts17.Employee implements java.lang.Comparable<Parts6.Parts.Parts17.Employee> {
  public Parts6.Parts.Parts17.Employee();
  public int compareTo(Parts6.Parts.Parts17.Employee);
  public int compareTo(java.lang.Object);
}

Видно что был синтезироан мостовый метод public int compareTo(java.lang.Object);
Он необходим для того что бы вызвать метод compareTo из класса Employee.
Например если ссылке класса родителя присвоить объект класса наследника :
Comparable<Employee> comp = new Employee();
то во время компиляции, при стирании типов, метод интерфейса Comparable
имеет вид compareTo(Object o) у которого тип параметра не совпадает с методом compareTo(Employee o)
класса Employee. И поэтому при вызове метода, будет вызван метод класса родителя, а не наследника.
Что бы этого не произошло компилятор автоматически синтезирует метод:
    int compareTo(java.lang.Object obj) {
         add ((Employee) obj)
     }
благодаря этому вызывается нужный нам переопределенный в классе наследнике метод.
 */
public class Employee implements Comparable<Employee> {

    @Override
    public int compareTo(Employee o) {
        return 0;
    }
}
