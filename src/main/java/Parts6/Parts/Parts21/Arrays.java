package Parts6.Parts.Parts21;

import java.lang.reflect.Array;
import java.util.List;

/*
Используя аннатацию  @SafeVarargs, напишите метод, позволяющий строить
массивы обобщенных типов, как в следующем примере:
    List<String> [] result = Arrays.<List<String>>construct(10);
Устанавливает результат в списке типа List<String>[] длинной 10
 */
public class Arrays {

    @SafeVarargs
    public static <T> T[] construct(int length, T... types) {
        T[] elements = (T[]) Array.newInstance(types[0].getClass(), length);
        return elements;
    }
}

class Main {

    public static void main(String[] args) {
        List<String>[] result = Arrays.<List<String>>construct(10);
    }
}