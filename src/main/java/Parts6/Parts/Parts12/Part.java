package Parts6.Parts.Parts12;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/*
Реализуйте следующий метод, сохраняющий наименьший и наибольший элементы из массива elements в списке result:
public static <Т> void minmax(List<T> elements, Comparator<? super T> comp, List<? super T> result)
Обратите внимание на подстановочный тип в последнем параметре. Для хранения полученного результата подойдет любой супертип обобщенного типа Т.
 */
public class Part {

    public static <T> void minMax(List<T> elements, Comparator<? super T> comp, List<? super T> result) {
        T min = Collections.min(elements, comp);
        result.add(min);
        T max = Collections.max(elements, comp);
        result.add(max);
    }
}
