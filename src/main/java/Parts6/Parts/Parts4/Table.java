package Parts6.Parts.Parts4;

import java.util.ArrayList;
import java.util.Objects;

/*
Сделайте вложенным класс Entry из предыдущего упражнения. Должен ли этот класс быть обобщенным?
 */
public class Table<K, V> {

    private ArrayList<Entry> list = new ArrayList<>();

    public void add(Entry element) {
        list.add(element);
    }

    public V getValue(K key) {
        for (Entry element : list) {
            if (element.getKey().equals(key)) {
                return (V) element.getValue();
            }
        }
        throw new RuntimeException("Ключ не найден");
    }

    public boolean setValue(K key, V value) {
        for (Entry element : list) {
            if (element.getKey().equals(key)) {
                element.setValue(value);
                return true;
            }
        }
        return false;
    }

    public boolean removeKey(K key) {
        for (Entry element : list) {
            if (element.getKey().equals(key)) {
                list.remove(element);
                return true;
            }
        }
        return false;
    }

    public int size() {
        return list.size();
    }

    public class Entry {

        private K key;
        private V value;

        public Entry(K k, V v) {
            this.key = k;
            this.value = v;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        public void setValue(V v) {
            this.value = v;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Entry entry = (Entry) o;
            return Objects.equals(key, entry.key) &&
                    Objects.equals(value, entry.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(key, value);
        }
    }
}
