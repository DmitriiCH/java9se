package Parts6.Parts.Parts14;

import java.util.ArrayList;

/*
Реализуйте усовершенствованный вариант метода closeAll (), представленного в разделе 6.3.
Закройте все элементы даже в том случае, если некоторые из них генерируют исключение.
В таком случае сгенерируйте исключение впоследствии. Если исключение генерируется в результате двух или больше вызовов данного метода, свяжите их в цепочку.
 */
public class Part {

    public static <T extends AutoCloseable> void closeAll(ArrayList<T> elems) {
        for (T element : elems) {
            try {
                element.close();
            } catch (Exception e) {
                try {
                    element.close();
                } catch (Exception ee) {
                    element = null;
                    ee.printStackTrace();
                }
            }
        }
    }
}
