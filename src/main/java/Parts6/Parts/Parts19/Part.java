package Parts6.Parts.Parts19;

import java.lang.reflect.Array;
import java.util.function.IntFunction;

/*
Рассмотрите следующий метод, представленный в разделе 6.6.3

        public static <T> ArrayList<T> repeat (int n, T obj) {
        ArrayList<T> elements = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            elements.add(obj);
        }
        return elements;
        }

Этот метод без особых хлопот  составляет списочный массив типа ArrayList<T>
из элементов обобщенного типа T. Можно ли получить массив типа []T из
этого списочного массива, не пользуясь объектом типа Class или ссылкой
на конструктор? Если нельзя, то почему?

Нет, создание объектов (new T() или new T[] ) обобщенного типа невозможо, т.к. :
"Обобщенные типы нельзя употреблять в таких выражениях, как, например, new Т (...) или new Т [... ].
Подобные формы запрещены, поскольку они не воплощают того, что намеревается сделать программист, когда стирается обобщенный тип Т.".
Ниже приведены примеры, как создать массивы обобщенных типов.

Примитивные типы так же не могут быть обобщенными, т.к. при стирании типов,
примитивный тип не может быть типом Object.
 */
public class Part {

    public static void main(String[] args) {
        String[] strings = repeat1(10, "hi");
        Integer[] numbers = repeat2(5, 10, Integer[]::new);
    }

    public static <T> T[] repeat1(int n, T obj) {
        T[] elements = (T[]) Array.newInstance(obj.getClass(), n);  //пример использования рефлексии для создания объекта.
        return elements;
    }

    public static <T> T[] repeat2(int n, T obj, IntFunction<T[]> constructor) {
        T[] elements = constructor.apply(n); // пример использования функционального интерфейса для создания объекта.
        return elements;
    }
}
