package Parts6.Parts.Parts24;

/*
Какие методы можно вызвать для переменной типа Class<?>, не прибегая
к приведению типов?

для переменной типа Class<?> можно вызвать любой метод, относящийся к классу Class.
Т.к. <?> означает, что ссылка класса Class<?> может ссылаться на любой объект.
 */
public class Part {

    public static void main(String[] args) {
        String s = "sss";
        Class<?> myclass = Integer.class;
        method(myclass);
    }

    public static void method(Class iclass) {
        System.out.println(iclass.getSimpleName());
    }
}