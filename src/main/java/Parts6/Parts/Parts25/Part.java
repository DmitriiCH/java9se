package Parts6.Parts.Parts25;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;

/*
Наипишите метод public static String genericDeclaration (Method m),
возвращающий объявление метода m(), перечисляющего  параметры типа с их ограничениями
и типами параметров метода, включая их аргументы типа, если это обобщенные типы.
 */
public class Part {

    public static void main(String[] args) throws NoSuchMethodException {
        Method method = Part.class.getMethod("method", ArrayList.class, int.class);
        System.out.println(genericDeclaration(method));
    }

    public static String genericDeclaration(Method m) {
        String methodName = m.getName();
        String parameters = getParametersString(m);
        String returnType = getReturnsType(m);
        return String.format("Method name - %s\nReturn type - %s\nParameters type - %s", methodName, returnType, parameters);
    }

    private static String getParametersString(Method m) {
        Type[] types = m.getGenericParameterTypes();
        return Arrays.toString(types);
    }

    private static String getReturnsType(Method m) {
        return m.getGenericReturnType().getTypeName() + " " + m.getReturnType();
    }

    public static <T extends Number> T method(ArrayList<Integer> list, int x) {
        return null;
    }
}