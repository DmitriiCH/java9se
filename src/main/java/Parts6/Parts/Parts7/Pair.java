package Parts6.Parts.Parts7;

import java.util.ArrayList;

/*
Реализуйте обобщенный класс Pair<E>, позволяющий сохранять пару элементов типа Е. Предоставьте методы доступа для получения первого и второго элементов.
 */
public class Pair<E> {

    private ArrayList<E> list = new ArrayList<>();

    public Pair(E... elem) {
        for (E e : elem) {
            list.add(e);
        }
    }

    public E getElement(int index) {
        if (index > list.size()) {
            throw new ArrayIndexOutOfBoundsException("Индекс " + index + " больше чем размер листа");
        }
        return list.get(index);
    }

    public E getElement1() {
        return list.get(0);
    }

    public E getElement2() {
        return list.get(1);
    }
}

class Main {

    public static void main(String[] args) {
        Pair<String> pair = new Pair<>("первый", "второй");
        System.out.println(pair.getElement1());
        System.out.println(pair.getElement2());
        System.out.println(pair.getElement(3));
    }
}