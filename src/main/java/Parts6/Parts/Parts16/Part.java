package Parts6.Parts.Parts16;

import java.util.Collection;
import java.util.List;

/*
К чему приведет стирание типов в приведенных ниже методах из класса
Collection?


public static <Т extends Comparable<? super Т» void sort(List<T> list)

public static <T extends Object & Comparable<? super Т» T max (Collections <? extends T> coll)

Cтирание типов приведет эти методы к такому виду:
    public static Comparable void sort(List list) {

    }

    public static Object T max(Collection coll) {
        return null;
    }
 */
public class Part {

    public static <T extends Comparable<? super T>> void sort(List<T> list) {
    }

    public static <T extends Object & Comparable<? super T>> T max(Collection<? extends T> coll) {
        return null;
    }
}