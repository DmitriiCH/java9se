package Parts6.Parts.Parts8;

import java.util.ArrayList;

/*
Видоизмените класс из предыдущего упражнения, введя методы шах () и min () для получения наибольшего и наименьшего из двух элементов.
Наложите соответствующее ограничение на обобщенный тип Е.
 */
public class Pair<E extends Number> {

    private ArrayList<E> list = new ArrayList<>();

    public Pair(E... elem) {
        for (E e : elem) {
            list.add(e);
        }
    }

    public E getElement(int index) {
        if (index > list.size()) {
            throw new ArrayIndexOutOfBoundsException("Индекс " + index + " больше чем размер листа");
        }
        return list.get(index);
    }

    public E getElement1() {
        return getElement(0);
    }

    public E getElement2() {
        return getElement(1);
    }

    public E max() {
        Number max = getElement(0);
        for (E e : list) {
            if (e.doubleValue() > max.doubleValue()) {
                max = e.doubleValue();
            }
        }
        return (E) max;
    }

    public E min() {
        Number min = getElement(0);
        for (E e : list) {
            if (e.doubleValue() < min.doubleValue()) {
                min = e.doubleValue();
            }
        }
        return (E) min;
    }
}

class Main {

    public static void main(String[] args) {
        Pair<Number> pair = new Pair<>(1, 2, 3, 4, 5);
        System.out.println(pair.max());
        System.out.println(pair.min());
    }
}