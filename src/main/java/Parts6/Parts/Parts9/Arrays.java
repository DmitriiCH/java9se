package Parts6.Parts.Parts9;

import Parts6.Parts.Parts8.Pair;

import java.util.ArrayList;

/*
Предоставьте в служебном классе Arrays следующий метод, возвращающий пару, состоящую из первого и последнего элементов массива а, указав подходящий аргумент
типа: public static <Е> Pair<E> firstLast(ArrayList<__> a)
 */
public class Arrays {

    public static <E extends Number> Pair<E> firstLast(ArrayList<E> a) {
        return new Pair<>(a.get(0), a.get(a.size() - 1));
    }
}
