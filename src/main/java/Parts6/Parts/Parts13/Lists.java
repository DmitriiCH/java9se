package Parts6.Parts.Parts13;

import java.util.List;

public class Lists {

    public static void swap(List<?> list, int firstIndex, int lastIndex) {
        swapHelper(list, firstIndex, lastIndex);
    }

    public static <T> void swapHelper(List<T> list, int firstIndex, int lastIndex) {
        T firstElement = list.get(firstIndex);
        T latElement = list.get(lastIndex);
        list.add(lastIndex, firstElement);
        list.remove(lastIndex + 1);
        list.add(firstIndex, latElement);
        list.remove(firstIndex + 1);
    }

}
