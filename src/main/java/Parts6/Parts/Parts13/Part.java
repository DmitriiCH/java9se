package Parts6.Parts.Parts13;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/*
С учетом метода из предыдущего упражнения рассмотрите следующий метод:
public static <Т> void maxmin(List<T> elements, Comparator<? super T> comp, List<? super T> result) {
minmax(elements, comp, result);
Lists.swapHelper(result, 0, 1);
}
Почему этот метод нельзя скомпилировать без захвата подстановки? Подсказка: попробуйте предоставить явный тип Lists. <____>swapHelper (result, 0, 1).

Этот метод нельзя скомпилировать без захвата подстановки, т.к. в списке result,
нет явного типа, в нем могут быть типы T и его суперклассы. Т.е. нельзя могу указать тип
       ? firstElement = list.get(firstIndex);
       ? latElement = list.get(lastIndex);
Для этого используется захват подстановки.
 */
public class Part {

    public static <T> void minMax(List<T> elements, Comparator<? super T> comp, List<? super T> result) {
        T min = Collections.min(elements, comp);
        result.add(min);
        T max = Collections.max(elements, comp);
        result.add(max);
    }

    public static <T> void maxMin(List<T> elements, Comparator<? super T> comp, List<? super T> result) {
        minMax(elements, comp, result);
        //Lists.swapHelper(result, 0, 1);
    }
}
