package Parts6.Parts.Parts6;

import java.math.BigDecimal;
import java.util.ArrayList;

/*
Реализуйте обобщенный метод, присоединяющий все элементы из одного списочного массива к другому.
Воспользуйтесь метасимволом подстановки для обозначения одного из аргументов типа.
Предоставьте два равнозначных решения: одно с подстановочным типом ? extends Е, другое — с подстановочным типом ? super Е.
 */
public class Parts {

    public static void main(String[] args) {
//        ArrayList<Number> list1 = new ArrayList<>();
//        ArrayList<Double> list2 = new ArrayList<>();
//        ArrayList<BigDecimal> list3 = new ArrayList<>();
//        list1.add(1);
//        list1.add(6);
//        list2.add(1.2);
//        list2.add(1.6);
//        list3.add(new BigDecimal("11111111111111111111111111111111111111111111111111111"));
//        joining1(list1, list2);
//        joining1(list1, list3);
//        for (Object obj : list1) {
//            System.out.println(obj);
//        }
        ArrayList<Integer> list4 = new ArrayList<>();
        ArrayList<Number> list5 = new ArrayList<>();
        ArrayList<Object> list6 = new ArrayList<>();
        list4.add(1);
        list5.add(2.2);
        list6.add("object");
        joining2(list4, list5);
        joining2(list4, list6);
        for (Object obj : list4) {
            System.out.println(obj);
        }
    }

    /*
    в результирующем списке я могу указать любой элемент, так как ограничение задается типом
    E, который я сам задаю. Но добавить в этот список я могу только те элементы, которые являются его наследником.
     */
    public static <E> void joining1(ArrayList<E> rezult, ArrayList<? extends E> source) {
        for (E element : source) {
            rezult.add(element);
        }
    }

    /*
    в результирующем списке я могу указать любой элемент, так как ограничение задается типом
    E, который я сам задаю. Но добавить в этот список я могу только те элементы, которые являются его суперклассом.
     */
    public static <E> void joining2(ArrayList<E> rezult, ArrayList<? super E> source) {
        for (Object element : source) {
            rezult.add((E) element);
        }
    }
}