package Parts6.Parts.Parts22;

import java.util.concurrent.Callable;

/*
Усовершенствуйте метод

        public static <V, T extends Throwable> V doWork(Callable<V> c, T ex) throws T {
        try {
            return c.call();
        } catch (Throwable relEx) {
            ex.initCause(relEx);
            throw ex;

        }
    }

представленный в разделе 6.6.7, таким образом, чтобы передать ему объект исключения,
который вряд ли будет вообще использован. Вместо этого данный метод должен принимать ссылку
на класс исключения.
 */
public class Part {

    public static void main(String[] args) {
        Class<RuntimeException> runtimeException = RuntimeException.class;
        doWork(() -> 10 / 0, runtimeException);
    }

//    public static <V, T extends Throwable> V doWork(Callable<V> c, T ex) throws T {
//        try {
//            return c.call();
//        } catch (Throwable relEx) {
//            ex.initCause(relEx);
//            throw ex;
//        }
//    }

    public static <V, T extends Throwable> V doWork(Callable<V> c, Class<T> ex) throws T {
        try {
            return c.call();
        } catch (Throwable relEx) {
            T exception = null;
            try {
                exception = ex.newInstance(); // используя ссылку на класс исключения, создаем ее объект;
            } catch (Exception e) {
                e.printStackTrace();
            }
            relEx.initCause(exception);
            throw (T) relEx;
        }
    }
}