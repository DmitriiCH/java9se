package Parts6.Parts.Parts23;

/*
Во врезке "Внимание!" из раздела 6.6.7 упоминается вспомогательный метод throwAs (),
применяемый для "приведения" типа исключения ех к типу RuntimeException и его генерирования.
Почему для этой цели нельзя воспользоваться обычным приведением типов, т.е. throw (RuntimeException) ex?



Обычным приведением типов воспользоваться нельзя, т.к. если будет поймана ошибка не
производная от RuntimeException, то при приведении типов возникнет исключение.
 */
public class Part {

    @SuppressWarnings("unchecked")
    private static <T extends Throwable> void throwAs(Throwable e) throws T {
        throw (T) e; // Приведение обобщенного типа стирается в тип (Throwable) е
    }
}
