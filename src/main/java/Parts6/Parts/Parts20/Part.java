package Parts6.Parts.Parts20;

import java.lang.reflect.Array;

/*
Реализуйте следующий метод:
    @SafeVarargs
    public static final <T> T[] repeat (int n, T... objs);
Он должен возвращать массив n копий заданных объектов. Имейте ввиду, что
для этого не потребуется объект типа Class или ссылка на конструктор,
поскольку наращивать кол-во объектов objs можно рефлексивно.

 */
public class Part {

    @SafeVarargs
    public static final <T> T[] repeat(int n, T... objs) {
        T[] elements = (T[]) Array.newInstance(objs[0].getClass(), (n * objs.length));
        int k = 0;
        for (T element : objs) {
            for (int i = 0; i < n; i++) {
                elements[i + k] = element;
            }
            k += n;
        }
        return elements;
    }
}