package Parts6.Parts.Parts1;

import java.util.ArrayList;

/*
1. Реализуйте обобщенный класс Stack<E>, управляющий списочным массивом, состоящим из элементов типа Е. Предоставьте методы push(), рор() и isEmpty().
 */
public class Stack<E> {

    private ArrayList<E> elements = new ArrayList<>();

    public void push(E element) {
        elements.add(element);
    }

    public E pop() {
        E element = elements.get(elements.size() - 1);
        elements.remove(elements.size() - 1);
        return element;
    }

    public boolean isEmpty() {
        return elements.isEmpty();
    }
}

class Main {

    public static void main(String[] args) {
        Stack<String> stack = new Stack<>();
        System.out.println(stack.isEmpty());
        stack.push("Первый");
        stack.push("Второй");
        System.out.println(stack.isEmpty());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.isEmpty());
    }
}