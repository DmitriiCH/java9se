package Parts6.Parts.Parts15;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/*
Реализуйте метод mар (), получающий списочный массив и объект типа Functior<T, R> (см. главу 3) и возвращающий списочный массив,
состоящий из результатов применения функции к заданным элементам этого массива.
 */
public class Part {

    public static <T, R> List<R> map(List<T> list, Function<T, R> function) {
        List<R> result = new ArrayList<>();
        for (T element : list) {
            result.add(function.apply(element));
        }
        return result;
    }
}
