package Parts6.Parts.Parts11;

import Parts6.Parts.Parts8.Pair;

import java.util.ArrayList;

/*
Продолжая предыдущее упражнение, предоставьте метод minMax (), возвращающий объект типа Pair с наименьшим и наибольшим элементами массива.
 */
public class Arrays {

    public static <E extends Number> Pair<E> firstLast(ArrayList<E> a) {
        return new Pair<>(a.get(0), a.get(a.size() - 1));
    }

    public static <E extends Number> E max(ArrayList<E> a) {
        Number max = a.get(0);
        for (Number element : a) {
            if (element.doubleValue() >= max.doubleValue()) {
                max = element.doubleValue();
            }
        }
        return (E) max;
    }

    public static <E extends Number> E min(ArrayList<E> a) {
        Number min = a.get(0);
        for (Number element : a) {
            if (element.doubleValue() <= min.doubleValue()) {
                min = element.doubleValue();
            }
        }
        return (E) min;
    }

    public static <E extends Number> Pair<E> minMax(ArrayList<E> a) {
        return new Pair<>(min(a), max(a));
    }
}