package Parts6.Parts.Parts18;

import java.util.Arrays;
import java.util.function.IntFunction;

/*
Рассмотрите следующий метод, представленный в разделе 6.6.3

    public static <T> T[] repeat (int n, T obj, IntFunction<T[]> constr)

Почему исход вызова Arrays.repeat (10, 42, int[]::new) окажется неудачным?
Как устранить этот недостаток?  Что нужно сделать для применения других
примитивных типов?
 */
public class Part {


    public static void main(String[] args) {
        String[] elements = repeat(10, "Hi", String[]::new);
        System.out.println(Arrays.toString(elements));
        //int[] elements2 = repeat(10, 42, int[]::new);
        //Integer[] elements2 = repeat(10, 100, Integer[]::new);
//        elements2.getClass();
    }

    public static <T> T[] repeat(int n, T obj, IntFunction<T[]> constr) {
        T[] result = constr.apply(n);
        for (int i = 0; i < n; i++) {
            result[i] = obj;
        }
        return result;
    }
}
