package Parts6.Parts.Parts5;

/*
Рассмотрите следующий вариант метода swap(), где массив может
быть предоставлени с помощью аргументов переменной длинны:

    public static <T> T[] swap (int i, int j, T... values) {
        T temp = values[i];
        values[i] = values[j];
        values[j] = temp;
        return values;
    }

Рассмотрите следующий вызов:
    Double[] result = Arrays.swap(0, 1, 1.5, 2, 3);
Какое сообщение об ошибке вы получите? Далее сделайте такой вызов:
    Double[] result2 = Arrays.<Double>swap(0, 1, 1.5, 2, 3);
Изменилось ли к лучшему сообщение об ошибке? Что нужно сделать для устранения ошибки?
 */
public class Parts {

    public static void main(String[] args) {
//        Double[] result = (Double[]) Arrays.swap(0, 1, 1.5, 2, 3);
// ошибка, что в values используются некорректные типы, нужен тип Double, т.к. result имеет тип Double.
//        Double[] result2 = Arrays.<Double>swap(0, 1, 1.5, 2, 3); // аналогично

        Double[] result3 = Arrays.swap(0, 1, 1.5, 2.0, 3.0);
    }
}
