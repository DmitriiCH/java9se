package Parts6.Parts.Parts3;

import java.util.ArrayList;

/*
Реализуйте обобщенный класс Table<K, V>, управляющий списочным массивом, состоящим из элементов типа Entry<K, V>.
Предоставьте методы для получения значения, связанного с ключом, установки значения по заданному ключу и удаления ключа.
 */
public class Table<K, V> {

    private ArrayList<Entry<K, V>> list = new ArrayList<>();

    public void add(Entry<K, V> element) {
        list.add(element);
    }

    public V getValue(K key) {
        for (Entry element : list) {
            if (element.getKey().equals(key)) {
                return (V) element.getValue();
            }
        }
        throw new RuntimeException("Ключ не найден");
    }

    public boolean setValue(K key, V value) {
        for (Entry element : list) {
            if (element.getKey().equals(key)) {
                element.setValue(value);
                return true;
            }
        }
        return false;
    }

    public boolean removeKey(K key) {
        for (Entry element : list) {
            if (element.getKey().equals(key)) {
                list.remove(element);
                return true;
            }
        }
        return false;
    }

    public int size() {
        return list.size();
    }
}

class Main {

    public static void main(String[] args) {
        Table<String, String> table = new Table<>();
        table.add(new Entry<String, String>("первый", "второй"));
        System.out.println(table.size());
        table.setValue("первый", "fdfdfd");
        System.out.println(table.getValue("первый"));
        System.out.println(table.removeKey("первый"));
        System.out.println(table.getValue("первый"));
    }
}
