package Parts11.Parts.Parts3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class MyObjectOutputStream {
    private File file;
    private PrintWriter writer;

    public MyObjectOutputStream(File file) throws FileNotFoundException {
        this.file = file;
        writer = new PrintWriter(file);
    }

    public void write(Object object) throws IllegalAccessException, FileNotFoundException {
        if (!verifyAnnotationSerializable(object.getClass())) {
            throw new RuntimeException(object.getClass() + " - класс не поддерживает аннотацию \"Serializable\"");
        }

        Field[] fields = object.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true); //Метод setAccessible(true) разрешает нам дальнейшую работу с полем.
            writer(field.get(object), field.getType());
        }
    }

    // записываем примитивные поля в файл, либо дальше раскрываем
    private void writer(Object object, Class<?> type) throws IllegalAccessException, FileNotFoundException {
        if (type.isPrimitive() || object instanceof String) {
            writer.println(object);
        } else {
            write(object);
        }
    }

    private boolean verifyAnnotationSerializable(Class<?> cl) {
        for (Annotation an : cl.getDeclaredAnnotations()) {
            if (an.annotationType().getSimpleName().equals("Serializable")) {
                return true;
            }
        }
        return false;
    }

    public void close() {
        writer.close();
    }
}
