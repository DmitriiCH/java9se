package Parts11.Parts.Parts3;

import Parts11.Parts.Parts2.Serializable;
import Parts11.Parts.Parts4.MyClass2;

@Serializable
public class MyClass1 {

    protected String str;
    int x;
    double y;

    MyClass2 mc = new MyClass2("MyClass2", 10, 11);

    public MyClass1(String str, int x, double y) {
        this.str = str;
        this.x = x;
        this.y = y;
    }
}
