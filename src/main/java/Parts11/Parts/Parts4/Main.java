package Parts11.Parts.Parts4;


import java.io.IOException;
import java.nio.file.Paths;

/*
Если бы аннотации присутствовали в первых версиях java, то интерфейс
Serializable, безусловно, был бы снабжен анотацией. Реализуйте анотацию
@Serializable. Выберите текстовый или двоичный формат для сохраняемости.
Предоставте классы для потоков ввода-вывода, чтения и записи, сохраняющих
состояние объектов путем сохранения и восстановления всех полей, содержащих
значения примитивных типов или же самих поддающихся сериализации. Не обращайте
пока что внемание на циклические ссылки.
 */
public class Main {
    public static void main(String[] args) throws IOException, IllegalAccessException {
        MyClass2 cl = new MyClass2("MyClass2", 5, 5);
        MyObjectOutputStream out = new MyObjectOutputStream(Paths.get("src\\main\\java\\Parts11\\Parts\\Parts4\\file.txt").toFile());
        out.write(cl);
        out.close();
        //
        MyObjectInputStream ois = new MyObjectInputStream(Paths.get("src\\main\\java\\Parts11\\Parts\\Parts4\\file.txt"));
        MyClass2 cl2 = ois.read(cl);
        System.out.println(cl2.str);
        System.out.println(cl2.x);
        System.out.println(cl2.y);
    }
}
