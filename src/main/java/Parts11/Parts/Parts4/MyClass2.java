package Parts11.Parts.Parts4;

import Parts11.Parts.Parts2.Serializable;

@Serializable
public class MyClass2 {

    public String str;

    @Transient
    public int x;

    @Transient
    public double y;

    public MyClass2(String str, int x, double y) {
        this.str = str;
        this.x = x;
        this.y = y;
    }
}
