package Parts11.Parts.Parts1;

import java.util.Arrays;
import java.util.Objects;

/*
Поясните, каким образом можно изменить метод Object.clone(), чтобы
воспользоваться аннотацией @Cloneable вместо маркерного интерфейса
Cloneable.

Можно изменить следующим образом:
В методе Object.clone(), выполнить проверку с помощью рефлексии, на наличие
у класса соответствующей анотации. Для этого нам необходимо получить экземпляр
класса getClass или .class и у него вызвать метод getDeclaredAnnotations(), возвращающий
массив всех анотаций относящихся к классу. Проверить имеется ли у него анотация
@Cloneable и если да, то выполнить клонирование.

 */
@Deprecated
public class Main {

    public static void main(String[] args) {
        Class cl = Main.class;

        System.out.println(Arrays.toString(cl.getDeclaredAnnotations()));
    }
}

