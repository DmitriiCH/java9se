package Parts11.Parts.Parts2;

import java.io.IOException;
import java.nio.file.Paths;

/*
Если бы аннотации присутствовали в первых версиях java, то интерфейс
Serializable, безусловно, был бы снабжен анотацией. Реализуйте анотацию
@Serializable. Выберите текстовый или двоичный формат для сохраняемости.
Предоставте классы для потоков ввода-вывода, чтения и записи, сохраняющих
состояние объектов путем сохранения и восстановления всех полей, содержащих
значения примитивных типов или же самих поддающихся сериализации. Не обращайте
пока что внемание на циклические ссылки.
 */
public class Main {
    public static void main(String[] args) throws IOException, IllegalAccessException {
        MyClass1 cl = new MyClass1("MyClass1", 5, 5);
        MyObjectOutputStream out = new MyObjectOutputStream(Paths.get("src\\main\\java\\Parts11\\Parts\\Parts2\\file.txt").toFile());
        out.write(cl);
        out.close();
        //
        MyObjectInputStream ois = new MyObjectInputStream(Paths.get("src\\main\\java\\Parts11\\Parts\\Parts2\\file.txt"));
        MyClass1 cl1 = ois.read(cl);
        System.out.println(cl1.str);
        System.out.println(cl.x);
        System.out.println(cl.y);
    }
}
