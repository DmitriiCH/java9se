package Parts11.Parts.Parts2;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class MyObjectInputStream {
    private Path path;
    private Queue<String> queue;

    public MyObjectInputStream(Path path) throws IOException {
        this.path = path;
        queue = new LinkedList<>(Files.readAllLines(path));
    }

    public MyClass1 read(Object obj) throws IOException, IllegalAccessException {
        if (!verifyAnnotationSerializ(MyClass1.class))
            throw new RuntimeException("Не поддерживает аннотацию Serializable");

        Field[] fields = MyClass1.class.getDeclaredFields();
         for (Field f : fields) {
            f.setAccessible(true);
            set(f, obj, queue.poll());
        }
        return new MyClass1((String) fields[0].get(obj), (Integer) fields[1].get(obj), (Double) fields[2].get(obj));
    }

    private void set(Field f, Object obj, String value) throws IllegalAccessException {
        switch (f.getType().getSimpleName()) {
            case "String": {
                f.set(obj, value);
                return;
            }
            case "double": {
                f.setDouble(obj, Double.parseDouble(value));
                return;
            }
            case "int": {
                f.setInt(obj, Integer.parseInt(value));
                return;
            }
            case "float": {
                f.setFloat(obj, Float.parseFloat(value));
                return;
            }
            case "short": {
                f.setShort(obj, Short.parseShort(value));
                return;
            }
            case "char": {
                f.setChar(obj, value.charAt(0));
                return;
            }
            case "long": {
                f.setLong(obj, Long.parseLong(value));
                return;
            }
            case "byte": {
                f.setByte(obj, Byte.parseByte(value));
                return;
            }
            case "boolean": {
                f.setBoolean(obj, (value.equals("true")));
                return;
            }
        }
    }

    private boolean verifyAnnotationSerializ(Class cl) {
        for (Annotation an : cl.getDeclaredAnnotations()) {
            if (an.annotationType().getSimpleName().equals("Serializable")) {
                return true;
            }
        }
        return false;
    }
}
