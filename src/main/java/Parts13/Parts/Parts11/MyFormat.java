package Parts13.Parts.Parts11;

import java.util.Locale;
import java.util.ResourceBundle;

public class MyFormat {

    private Locale locale;
    private ResourceBundle res;

    public MyFormat(Locale locale) {
        this.locale = locale;
        res = ResourceBundle.getBundle("formatter", locale);
    }

    public String getFormat() {
        return res.getString("format");
    }
}

