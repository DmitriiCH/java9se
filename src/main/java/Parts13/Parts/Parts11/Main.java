package Parts13.Parts.Parts11;

import java.util.Locale;

/*
Предоставьте класс для отображения форматов бумаги с учетом региональных настроек, используя
предпочтительные еденицы измерения и стандартный формат бумаги в заданных региональных настройках.
(Во всем мире, кроме Соединенных Штатов и Канады, употребляются форматы бумаги по стандарту ISO 216.
И только в трех странах официально не принята метрическая система: Либерии, Бирме и Соединенных Штатах.)

ISO 216:1975 определяет две серии форматов бумаги: A и B;
размер   Формат А мм     Формат B мм
0	    841 × 1189	    1000 × 1414
1	    594 × 841	    707 × 1000
2	    420 × 594	    500 × 707
3	    297 × 420	    353 × 500
4	    210 × 297	    250 × 353
5	    148 × 210	    176 × 250
6	    105 × 148	    125 × 176
7	    74 × 105	    88 × 125
8	    52 × 74	         62 × 88
9	    37 × 52	         44 × 62
10	    26 × 37	         31 × 44

Для США или Канады будет браться бандл (format_en_) если экземпляр Locale является US или CA.
Все остальные страны используют один формат бумаги, поэтому для всех остальных берется дефолтное
значение из файла format.properties. Т.к. ResourceBundle.getBundle берет дефолный файл из бандла, если нет файла
для подходящей локали.
(Для США и канады используется один и тот же формат Letter US описанный в файле formatter_en_US.properties и formatter_en_CA.properties,
в США размеры представлены в дюймах, т.к. там своя метрика)

 */
public class Main {
    public static void main(String[] args) {
        MyFormat usFormat = new MyFormat(Locale.US);
        MyFormat caFormat = new MyFormat(Locale.CANADA);
        MyFormat enFormat = new MyFormat(Locale.FRANCE);
        System.out.printf("%s\n/---------------/\n%s\n\n", Locale.US.getDisplayLanguage(Locale.US), usFormat.getFormat());
        System.out.printf("%s\n/---------------/\n%s\n\n", Locale.CANADA.getDisplayLanguage(Locale.CANADA), caFormat.getFormat());
        System.out.printf("%s\n/---------------/\n%s", Locale.FRANCE.getDisplayLanguage(Locale.FRANCE), enFormat.getFormat());
    }
}
