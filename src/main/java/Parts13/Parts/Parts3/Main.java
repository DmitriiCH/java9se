package Parts13.Parts.Parts3;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

/*
В каких из региональных настроек вашей виртуальной машины JVM применяются те же
условные обозначения дат (в формате месяц/день/год), что и в Соединенных Штатах.
 */
public class Main {

    public static void main(String[] args) {
        LocalDate localDate = LocalDate.now();
        printLocal(localDate);
    }

    public static void printLocal(LocalDate localDate) {
        for (Locale locale : Locale.getAvailableLocales()) {
            String formatUS = getFormat(Locale.US, localDate);
            if (formatUS.equals(getFormat(locale, localDate))) {
                System.out.println(getFormat(locale, localDate) + " " + locale + "/" + locale.getDisplayCountry());
            }
        }
    }

    public static String getFormat(Locale loc, LocalDate ld) {
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(loc);
        return formatter.format(ld);
    }
}
