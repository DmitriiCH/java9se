package Parts13.Parts.Parts2;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

/*
В каких из региональных настроек вашей виртуальной машины JVM не употребляются
арабские цифры для форматирования чисел.

Арабские цифры: 0	1	2	3	4	5	6	7	8	9;
 */
public class Main {
    public static void main(String[] args) {
        Number number = 123456789;
        for (Locale locale : Locale.getAvailableLocales()) {
            String res = getNumber(locale, number);
            if (!containsNumber(res)) {
                System.out.printf("%s - %s locale %s %s\n", number, res, locale, locale.getDisplayCountry(Locale.getDefault()));
            }
        }
    }

    public static String getNumber(Locale locale, Number number) {
        NumberFormat format = NumberFormat.getNumberInstance(locale);
        return format.format(number);
    }

    private static boolean containsNumber(String str) {
        String[] numbers = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
        for (String s: numbers) {
            if (str.contains(s)) return true;
        }
        return false;
    }
}
