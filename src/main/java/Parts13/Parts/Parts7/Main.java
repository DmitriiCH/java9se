package Parts13.Parts.Parts7;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

/*
Напишите программу, выводящую списком отображаемые и самостоятельные названия месяцев
во всех региональных настройках, где они отличаются, учитывая то, что самостоятельные названия
месяцев могут состоять из цифр.
 */
public class Main {

    public static void main(String[] args) {
        for (String lang : Locale.getISOLanguages()) {
            System.out.printf("Locale - %s\n", Locale.forLanguageTag(lang).getDisplayLanguage());
            getMonth(new Locale(lang)).forEach(System.out::println);
            System.out.println("/------------------------/\n");
        }
//        for (String lang : accumulate()) {
//            System.out.printf("Locale - %s\n%s\n\n", Locale.forLanguageTag(lang).getDisplayLanguage(), lang);
//        }
    }

    private static List<String> accumulate() {
        List<String> result = new ArrayList<>();
        for (String lang : Locale.getISOLanguages()) {
            result.addAll(getMonth(new Locale(lang)));
        }
        return result.stream().distinct().collect(Collectors.toList());
    }

    public static List<String> getMonth(Locale loc) {
        List<String> result = new ArrayList<>();
        LocalDate date = LocalDate.of(2021, 1, 1);
        do {
            result.add(date.getMonth().getDisplayName(TextStyle.FULL_STANDALONE, loc));
            date = date.plusMonths(1);
        } while (date.getYear() == 2021);
        return result;
    }
}
