package Parts13.Parts.Parts8;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
Напишите программу, выводящую списком все символы в Юникоде, расширяемые до двух и больше
символов в коде ASCII в форме нормализации KC или KD.

В стандарте Юникод диапазон из 65 536 (216) кодовых позиций
ASCII - 127 символов, каждый из которых содиржится в 1 байте.
 */
public class Main {

    public static void main(String[] args) {
        getBytes().forEach(System.out::println);
    }

    private static List<String> getBytes() {
        List<String> res = new ArrayList<>();
        for (int i = 0; i < 65536; i++) {
//          С помощью класса Normalizer проверяю является ли символ расширяемым проходя по всем символам юникода.
            boolean b = Normalizer.isNormalized((char) i + "", Normalizer.Form.NFKC);
            if (!b) {
                String s = Normalizer.normalize((char) i + "", Normalizer.Form.NFKC);
//          если символ можно разбить на составляющие из кодировки ASCII, то добавим строку в массив
                if (checkSymbol(s)) {
                    res.add((char) i + " " + i + " ASCII - " + s);
                }
            }
        }
        return res;
    }

    private static boolean checkSymbol(String str) {
        if (str.getBytes().length < 2) return false;
        for (byte b : str.getBytes()) {
            if (b < 0) return false;
        }
        return true;
    }
}
