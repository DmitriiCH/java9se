package Parts13.Parts.Parts1;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

/*
Напишите программу, демонстрирующую стили форматирования даты и времени во французском,
китайском и тайском представлениях.
 */
public class Main {

    public static void main(String[] args) {
        System.out.println(getDateTimeFormat(Locale.FRANCE));
        System.out.println(getDateTimeFormat(Locale.CHINA));
        System.out.println(getDateTimeFormat(Locale.TAIWAN));
        System.out.println(getDateTimeFormat(Locale.getDefault()));
    }

    public static String getDateTimeFormat(Locale locale) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL).withLocale(locale);
        LocalDateTime localDate = LocalDateTime.now();
        return dateTimeFormatter.format(localDate);
    }
}
