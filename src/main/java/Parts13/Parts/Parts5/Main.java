package Parts13.Parts.Parts5;

import java.util.Currency;
import java.util.Locale;

/*
Повторите предыдущее упражнение для наименования денежных едениц.
 */
public class Main {

    public static void main(String[] args) {
        //метод getISOLanguages() возвращает коды всех языков
        for (String lang : Locale.getISOLanguages()) {
            System.out.println(new Locale(lang).getDisplayLanguage(new Locale(lang)) + "\n");
            Currency.getAvailableCurrencies().stream().map(currency -> currency.getDisplayName(new Locale(lang))).forEach(System.out::println);
            System.out.println("/------------------------/");
        }
    }
}
