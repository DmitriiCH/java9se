package Parts13.Parts.Parts4;

import java.util.Arrays;
import java.util.Locale;

/*
Напишите программу, выводящую названия всех языков из региональных настроек вашей виртуальной
машины JVM на всех имеющихся языках. Отсортируйте их, исключив дубликаты.
 */
public class Main {

    public static void main(String[] args) {
        //метод getISOLanguages() возвращает коды всех языков
        for (String lang : Locale.getISOLanguages()) {
            System.out.println(new Locale(lang).getDisplayLanguage());
            // метод getAvailableLocales() возвращает массив всех региональных настроек, известных виртуальной машине
            Arrays.stream(Locale.getAvailableLocales()).map(locale -> locale.getDisplayLanguage(new Locale(lang)))
                    .sorted(String::compareTo).distinct().forEach(System.out::println);
            System.out.println("/------------------------/");
        }
    }
}
