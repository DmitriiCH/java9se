package Parts13.Parts.Parts10;

import java.nio.charset.Charset;
import java.util.Locale;

/*
Предоставьте механизм для представления имеющихся кодировок символов в удобочитаемом виде,
как это сделано в вашем браузере. Названия языков должны быть локализованы.(воспользуйтесь
переводами на местные языки).

В приведенном ниже коде показано, что все кодировки символов для всех языков
имеют свое обозначение на латинскими буквами и выводятся одинаково на любом языке.
 */
public class Main {

    public static void main(String[] args) {
        for (Charset charset : Charset.availableCharsets().values()) {
            for (Locale lang : Locale.getAvailableLocales()) {
                String dispName = charset.displayName(lang);
                System.out.printf("locale - %s, charset(%s) ", lang, dispName);
            }
            System.out.println("\n/-----------------------/");
        }
    }
}
