package Parts13.Parts.Parts6;

import java.util.Currency;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

/*
Напишите программу, выводящую списком все денежные еденицы, обозначаемые разными знаками
хотя бы в двух региональных настройках.
 */
public class Main {
    public static void main(String[] args) {
        Set<String> italySymbol = getSymbol(Locale.ITALY);
        for (String symbol : getSymbol(Locale.JAPAN)) {
            if(!(italySymbol.contains(symbol))) {
                System.out.println(symbol);
            }
        }

//        getSymbol(Locale.JAPAN).forEach(System.out::println);
//        System.out.println("/------------------------/");
//        getSymbol(Locale.ITALY).forEach(System.out::println);
    }

    public static Set<String> getSymbol(Locale loc) {
        //метод getAvailableCurrencies() возвращает множество денежных единиц известных виртуальной машине
        return Currency.getAvailableCurrencies().stream().map(currency -> currency.getSymbol(loc)).collect(Collectors.toSet());
    }
}
