package Parts13.Parts.Parts9;

import java.util.Locale;
import java.util.ResourceBundle;

/*
Попробуйте осуществить интернационализацию всех сообщений в одной из ваших прикладных
программ хотя бы на двух языках, используя комплекты ресурсов.
 */
public class Main {
    public static void main(String[] args) {
        ResourceBundle res = ResourceBundle.getBundle("m", new Locale("en"));
        Car myCar = new Car(res.getString("color"), res.getString("maxSpeed"), res.getString("description"));
        System.out.println(myCar);

        ResourceBundle res2 = ResourceBundle.getBundle("m", Locale.FRANCE);
        Car myCar2 = new Car(res2.getString("color"), res2.getString("maxSpeed"), res2.getString("description"));
        System.out.println(myCar2);

        ResourceBundle res3 = ResourceBundle.getBundle("m", new Locale("ru"));
        Car myCar3 = new Car(res3.getString("color"), res3.getString("maxSpeed"), res3.getString("description"));
        System.out.println(myCar3);

        ResourceBundle res4 = ResourceBundle.getBundle("m", Locale.ITALIAN);
        Car myCar4 = new Car(res4.getString("color"), res4.getString("maxSpeed"), res4.getString("description"));
        System.out.println(myCar4);
    }
}
