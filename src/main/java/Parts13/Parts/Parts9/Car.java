package Parts13.Parts.Parts9;

public class Car {
    private String color;
    private String maxSpeed;
    private String description;

    public Car(String color, String maxSpeed, String description) {
        this.color = color;
        this.maxSpeed = maxSpeed;
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public String getMaxSpeed() {
        return maxSpeed;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return String.format("Car[color - %s, maxSpeed - %s, description - %s]", getColor(), getMaxSpeed(), getDescription());
    }
}
