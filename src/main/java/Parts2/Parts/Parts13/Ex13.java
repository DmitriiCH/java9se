package Parts2.Parts.Parts13;

import com.opencsv.CSVReader;

import java.io.FileReader;

public class Ex13 {

    public static void main(String[] args) throws Exception {
        CSVReader reader = new CSVReader(new FileReader("C:\\Users\\dmch1019\\Desktop\\Java\\CSV.csv"));
        String[] nextLine;
        while ((nextLine = reader.readNext()) != null) {
            // nextLine[] is an array of values from the line
            System.out.println(nextLine[0]);
        }
    }
}
