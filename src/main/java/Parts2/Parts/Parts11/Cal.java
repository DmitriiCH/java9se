package Parts2.Parts.Parts11;

import java.time.DayOfWeek;
import java.time.LocalDate;

import static java.time.LocalDate.*;
import static java.lang.System.*;

//Перепишите класс Cal, чтобы использовать в нем статический импорт классов
//System и LocalDate.
public class Cal {

    public static void main(String[] args) {
        daysOfMonth(2020, 10, 1);
    }

    public static void daysOfMonth(int year, int month, int day) {
        LocalDate date = of(year, month, day);
        DayOfWeek weekday = date.getDayOfWeek();
        daysOfTheWeek(date);
        int value = weekday.getValue();
        for (int i = 1; i <= value; i++) {
            out.print("    ");
        }
        while (date.getMonthValue() == month) {
            if (date.getDayOfWeek().getValue() == 7 && value == 7) {
                out.println();
            }
            value++;
            out.printf("%4d", date.getDayOfMonth());
            date = date.plusDays(1);
        }
    }

    public static void daysOfTheWeek(LocalDate date) {
        out.println("  " + date.getMonth());
        String[] dayOfWeek = new String[]{" SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
        for (int i = 0; i < dayOfWeek.length; i++) {
            out.print(" " + dayOfWeek[i]);
        }
        out.print("\n");
    }
}
