package Parts2.Parts.Parts9;

//Реализуйте класс Саг, моделирующий передвижение автомобиля на бензиновом топливе по оси х. Предоставьте методы для
// передвижения автомобиля на заданное количество километров, заполнения топливного бака заданным количеством литров
// бензина, вычисления расстояния, пройденного от начала координат, а также уровня топлива в баке. Укажите расход
// топлива (в км/л) в качестве параметра конструктора данного класса. Должен ли этот класс быть неизменяемым и почему?
public class Car {

    private double efficiency;
    private double distance;
    private double fuelLevel;

    public Car(double efficiency) {
        this.efficiency = efficiency;
    }

    public void movement(double distance) {
        if (this.fuelLevel == 0) {
            throw new RuntimeException("Нужно заправиться, в баке " + this.fuelLevel + "л");
        }
        double fuel = (distance * this.efficiency) / 100;
        if (this.fuelLevel > fuel) {
            this.fuelLevel -= fuel;
            this.distance += distance;
        } else {
            double disTemp = (this.fuelLevel * 100) / this.efficiency;
            this.distance += disTemp;
            this.fuelLevel = this.fuelLevel - ((disTemp * this.efficiency ) / 100);
        }
    }

    public double distance() {
        return this.distance;
    }

    public void refueling(int fuel) {
        this.fuelLevel += fuel;
    }

    public double fuelLevelInTheCar() {
        return this.fuelLevel;
    }

    public static void main(String[] args) {
        Car car = new Car(10);
        car.refueling(5);
        car.movement(160);
        System.out.printf("Остаток топлива в баке %.2fл\n", car.fuelLevelInTheCar());
        System.out.printf("Мы проехали %.2fкм", car.distance());
    }
}