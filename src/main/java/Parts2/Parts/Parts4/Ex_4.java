//package Parts2.Parts.Parts4;
//
//import org.omg.CORBA.IntHolder;
//
//public class Ex_4 {
//
//    public static void main(String[] args) {
//        int a = 10;
//        int b = 5;
//        System.out.println("a = " + a + ", b = " + b);
//        replaceInt(a, b);
//        System.out.println("a = " + a + ", b = " + b);
//        ////////////////////////////////////////////////////
//        IntHolder c = new IntHolder(10);
//        IntHolder d = new IntHolder(5);
//        System.out.println("c = " + c.value + ", d = " + d.value);
//        replaceIntHolder(c, d);
//        System.out.println("c = " + c.value + ", d = " + d.value);
//    }
//
//    public static void replaceInt(int a, int b) {
//        int i = a;
//        a = b;
//        b = i;
//    }
//
//    public static void replaceIntHolder(IntHolder a, IntHolder b) {
//        IntHolder intHolder = new IntHolder();
//        intHolder.value = a.value;
//        a.value = b.value;
//        b.value = intHolder.value;
//    }
//}
