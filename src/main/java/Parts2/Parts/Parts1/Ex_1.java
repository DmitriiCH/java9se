package Parts2.Parts.Parts1;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Arrays;
import java.util.Locale;

public class Ex_1 {

    public static void main(String[] args) {
        daysOfMonth(2020, 10, 1);
    }

    public static void daysOfMonth(int year, int month, int day) {
        LocalDate date = LocalDate.of(year, month, day);
        DayOfWeek weekday = date.getDayOfWeek();
        daysOfTheWeek(date);
        int value = weekday.getValue();
        for (int i = 1; i <= value; i++) {
            System.out.print("    ");
        }
        while (date.getMonthValue() == month) {
            if (date.getDayOfWeek().getValue() == 7 && value == 7) {
                System.out.println();
            }
            value++;
            System.out.printf("%4d", date.getDayOfMonth());
            date = date.plusDays(1);
        }
    }

    public static void daysOfTheWeek(LocalDate date) {
        System.out.println("  " + date.getMonth());
        String[] dayOfWeek = new String[]{" SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
        for (int i = 0; i < dayOfWeek.length; i++) {
            System.out.print(" " + dayOfWeek[i]);
        }
        System.out.print("\n");
    }
}
