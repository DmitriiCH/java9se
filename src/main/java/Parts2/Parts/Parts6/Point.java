package Parts2.Parts.Parts6;
//Повторите предыдущее упражнение, но на этот раз сделайте методы trans late () и scale () модифицирующими.

/**
 * Обьект <code> класса Point</code>
 * описывает точку на плоскости.
 *
 * @author Dmitrii Churbanov
 * @version 1.0
 */
public class Point {

    private double x, y;

    public Point() {
        this.x = 0;
        this.y = 0;
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Возвращает значение координаты
     *
     * @return координата x
     */
    public double getX() {
        return x;
    }

    /**
     * Возвращает значение координаты
     *
     * @return координата y
     */
    public double getY() {
        return y;
    }

    /**
     * перемещает точку на определенное расстояние в направлении координат х и у
     *
     * @param x расстояние в направлении координаты x
     * @param y расстояние в направлении координаты y
     */
    public void translate(double x, double y) {
        this.x += x;
        this.y += y;
    }

    /**
     * Изменяет масштаб по обеим координатам на заданный коэффициент
     *
     * @param k коэффициент
     */
    public void scale(double k) {
        this.x *= k;
        this.y *= k;
    }

    public static void main(String[] args) {
        Point p = new Point(3, 4);
        System.out.println(p.getX() + " " + p.getY());
        p.translate(1, 3);
        p.scale(0.5);
        System.out.println(p.getX() + " " + p.getY());
    }
}
