package Parts2.Parts.Parts10;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

//Предоставьте в классе RandomNumbers два статических метода типа random Element, получающих произвольный элемент из
// обычного или списочного массива целочисленных значений. (Если обычный или списочный массив пуст, должен быть
// возвращен нуль.) Почему эти методы нельзя сделать методами экземпляра типа t [ ] или ArrayList<Integer>?
public class RandomNumbers {

    private static Random generator = new Random();

    public static int randomElementInList(ArrayList<Integer> list) {
        if (list.isEmpty()) {
            return 0;
        }
        return list.get(generator.nextInt(list.size() - 1));
    }

    public static int randomElementInArray(int[] ints) {
        if (ints.length == 0) {
            return 0;
        }
        return ints[generator.nextInt(ints.length - 1)];
    }

    public static void main(String[] args) {
        System.out.println(randomElementInArray(new int[]{1, 2, 3}));
        System.out.println(randomElementInArray(new int[]{}));
        /////////////////////////////////////////////////////////////
        System.out.println(randomElementInList(new ArrayList<>(Arrays.asList(1, 2, 3))));
        System.out.println(randomElementInList(new ArrayList<>(Arrays.asList())));
    }
}
