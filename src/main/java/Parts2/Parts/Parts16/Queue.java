package Parts2.Parts.Parts16;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

//Реализуйте в классе Queue неограниченную очередь символьных строк. Предоставьте метод add () для ввода элемента в
// хвост очереди и метод remove () для удаления элемента из головы очереди. Организуйте хранение элементов в виде
// связного списка узлов, создав вложенный класс Node. Должен ли этот класс быть статическим?
public class Queue<T> {

    private Node<T> head;
    private int size;

    public Queue() {
        head = null;
    }

    public void add(String data) {
        Node<T> node = new Node(data);
        node.next = head;
        head = node;
        size++;
    }

    public int size() {
        return size;
    }

    public void remove() {
        head = head.next;
        size--;
    }

    public void print() {
        Node<T> node = head;
        while (node != null) {
            System.out.println(node.data);
            node = node.next;
        }
    }

    public Iterator<T> iterator() {
        return (Iterator<T>) new MyIterator(head);
    }

    private static class Node<String> {

        String data;
        Node<String> next;

        Node(String data) {
            this.data = data;
        }
    }

    private class MyIterator implements Iterator<T> {

        Node<T> current;

        public MyIterator(Node<T> first) {
            current = first;
        }

        public boolean hasNext() {
            return current != null;
        }

        public T next() {
            if (!hasNext()) throw new NoSuchElementException();
            T t = current.data;
            current = current.next;
            return t;
        }
    }
}