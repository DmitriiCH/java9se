package Parts2.Parts.Parts16;

import java.util.Iterator;

public class Ex_16 {

    public static void main(String[] args) {
        Queue<String> queue = new Queue<>();
        System.out.println(queue.size());
        queue.add("Первая строка");
        queue.add("Вторая строка");
        queue.add("Третья строка");
        queue.add("Четвертая строка");
        queue.add("Пятая строка");
        queue.add("Шестая строка");
        Iterator<String> iterator = queue.iterator();
        while (iterator.hasNext()){
            String s = iterator.next();
            System.out.println(s);
        }
    }
}
