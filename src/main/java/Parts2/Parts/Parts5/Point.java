package Parts2.Parts.Parts5;
//Реализуйте неизменяемый класс Point, описывающий точку на плоскости. Предоставьте его конструктор, чтобы задать
// конкретную точку; конструктор без аргументов, чтобы задать точку в начале координат; а также методы getX (), getY
// (), translate () и scale (). В частности, метод translate () должен перемещать точку на определенное расстояние в
// направлении координат х и у, а метод scale () — изменять масштаб по обеим координатам на заданный коэффициент.
// Реализуйте эти методы таким образом, чтобы они возвращали новые точки в качестве результата. Например, в следующей
// строке кода:
//Point р = new Point(3, 4).translate(1, 3).scale(0.5); в переменной р должна быть установлена точка с координатами
// (2, 3,5).

/**
 * Обьект <code> класса Point</code>
 * описывает точку на плоскости.
 *
 * @author Dmitrii Churbanov
 * @version 1.0
 */

public class Point {

    private double x, y;

    public Point() {
        this.x = 0;
        this.y = 0;
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Возвращает значение координаты
     *
     * @return координата x
     */
    public double getX() {
        return x;
    }

    /**
     * Возвращает значение координаты
     *
     * @return координата y
     */
    public double getY() {
        return y;
    }

    /**
     * перемещает точку на определенное расстояние в направлении координат х и у
     *
     * @param x расстояние в направлении координаты x
     * @param y расстояние в направлении координаты y
     * @return новая точка перемещенная на определенное расстояние
     */
    public Point translate(int x, int y) {
        return new Point(this.x + x, this.y + y);
    }

    /**
     * Изменяет масштаб по обеим координатам на заданный коэффициент
     *
     * @param k коэффициент
     * @return новая точка с измененным масштабом
     */
    public Point scale(double k) {
        return new Point(this.x * k, this.y * k);
    }

    public static void main(String[] args) {
        Point p = new Point(3, 4);
        Point p1 = p;
        p1 = p1.translate(1, 3).scale(0.5);
        System.out.println(p.getX() + " " + p.getY());
        System.out.println(p1.getX() + " " + p1.getY());
    }
}