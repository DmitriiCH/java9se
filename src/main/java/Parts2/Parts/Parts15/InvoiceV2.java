package Parts2.Parts.Parts15;

import java.util.ArrayList;

public class InvoiceV2 {

    public static class Item {

        // Открытый вложенный класс
        private String description;
        private int quantity;
        private double unitPrice;

        public Item(String description, int quantity, double unitPrice) {
            this.description = description;
            this.quantity = quantity;
            this.unitPrice = unitPrice;
        }

        public double price() {
            return quantity * unitPrice;
        }
    }

    private ArrayList<Item> items = new ArrayList<>();

    public void add(Item item) {
        items.add(item);
    }

    public static void printItems(ArrayList<Item> items) {
        for (InvoiceV2.Item item : items) {
            System.out.println(item.description + "\n" + item.quantity + "\n" + item.unitPrice);
        }
    }

    public static void main(String[] args) {
        InvoiceV2 mylnvoice = new InvoiceV2();
        InvoiceV2.Item newltem = new InvoiceV2.Item ("Blackwell Toaster", 2, 19.95);
        InvoiceV2.Item newltem1 = new InvoiceV2.Item ("Whitewell Toaster", 3, 19.95);
        mylnvoice.add(newltem);
        mylnvoice.add(newltem1);
        printItems(mylnvoice.items);
    }
}
