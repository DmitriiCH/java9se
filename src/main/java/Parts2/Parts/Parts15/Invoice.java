package Parts2.Parts.Parts15;

import java.util.ArrayList;

//Реализуйте полностью класс Invoice, представленный в разделе 2.6.1. Предоставьте метод, выводящий счет-фактуру, и
// демонстрационную версию программы, составляющей и выводящей образец счета-фактуры.
public class Invoice {

    private static class Item {

        // Закрытый вложенный класс
        String description;
        int quantity;
        double unitPrice;

        double price() {
            return quantity * unitPrice;
        }
    }

    private ArrayList<Item> items = new ArrayList<>();

    public void addltem(String description, int quantity, double unitPrice) {
        Item newltem = new Item();
        if(description.length() > 9){
            newltem.description = description.substring(0,9);
        } else {
            newltem.description = description;
        }
        newltem.quantity = quantity;
        newltem.unitPrice = unitPrice;
        items.add(newltem);
    }

    public static void printItems(ArrayList<Item> items) {
        System.out.print("ItemName       quantity   unitPrice   totalPrice\n");
        double allPrice = 0;
        for (Item item : items) {
            System.out.printf("%-9s  %9d  %11.2f  %10.2f\n", item.description, item.quantity, item.unitPrice,
                    item.quantity * item.unitPrice);
            allPrice += item.quantity * item.unitPrice;
        }
        System.out.printf("All price: %.2f", allPrice);
    }

    public static void main(String[] args) {
        Invoice invoice = new Invoice();
        invoice.addltem("Toas", 1, 11.5);
        invoice.addltem("Whitewell", 2, 19.95);
        invoice.addltem("Toaster", 10, 15.5);
        invoice.addltem("Backwell", 3, 19.95);
        invoice.addltem("Toasdsdsdsdter", 10, 15.5);
        invoice.addltem("Backwsdsdsdsell", 3, 19.95);
        printItems(invoice.items);
    }
}
//ItemName
//quantity
//unitPrice
//totalPrice