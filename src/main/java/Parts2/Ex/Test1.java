package Parts2.Ex;

public class Test1 {

    public static void main(String[] args) {
        Human human1 = new Human();
        human1.name = "Роман";
        human1.age = 50;
        Human human2 = new Human();
        human2.name = "Василий";
        human2.age = 20;
        System.out.println(human1);
        System.out.println(human2);
    }
}

class Human {

    String name;
    int age;

    @Override
    public String toString() {
        return "{Меня зовут "
                + name + ", мне "
                + age
                + " лет}";
    }
}