package Parts2.Ex;

public class Parts2_2_3 {

    private String name;
    public double salary;

    public void reiseSalary(double byPercent) {
        double raise = salary * byPercent / 100;
        salary += raise;
    }

    public String getName() {
        return name;
    }
}
