package Parts2.Ex;

public class Parts2_3_2 {

    private String name;
    private double salary;

    public Parts2_3_2(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    public Parts2_3_2(double salary) {
        this.name = "";
        this.salary = salary;
    }

    public static void main(String[] args) {
        Parts2_3_2 parts1 = new Parts2_3_2("James Bond", 500000);
        Parts2_3_2 parts2 = new Parts2_3_2(40000);
        System.out.println(parts2.name + " " + parts2.salary);
    }
}
