package Parts2.Ex;

public class Parts2_2_4 {

    private String name;
    public double salary;

    public void reiseSalary(double byPercent) {
        double raise = salary * byPercent / 100;
        salary += raise;
    }

    public String getName() {
        return name;
    }

    public static void main(String[] args) {
        Parts2_2_4 parts = new Parts2_2_4();
        parts.salary = 1000;
        System.out.println(parts.salary);
        parts.reiseSalary(5);
        System.out.println(parts.salary);
    }
}
