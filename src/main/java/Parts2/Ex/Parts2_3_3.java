package Parts2.Ex;

public class Parts2_3_3 {

    private String name;
    private double salary;

    public Parts2_3_3(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    public Parts2_3_3(double salary) {
        this("", salary);
    }
}
