package Parts2.Ex.Ex13;

import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;

public class Ex_13 {

    public static void main(String[] args) {
        LocalDate localDate = LocalDate.now();
        System.out.println(localDate);
        //////////////////
        ZoneId  timezone = ZoneId.of("Asia/Shanghai");
        LocalDate localDate1 = LocalDate.now(timezone);
        System.out.println(localDate1);
        ///
        LocalDate date = LocalDate.of(2019, Month.FEBRUARY, 22);
        System.out.println("Сегодня = " + date);
        ///
        LocalDate today = LocalDate.ofYearDay(2019, 100);
        System.out.println("Сегодня = " + today);
        ///
        System.out.println();
        System.out.println("getYear() " + today.getYear());
        System.out.println("getMonth() " + today.getMonth());
        System.out.println("getMonthValue() " + today.getMonthValue());
        System.out.println("getDayOfMonth() " + today.getDayOfMonth());
        System.out.println("getDayOfYear() " + today.getDayOfYear());
        System.out.println("getDayOfWeek() " + today.getDayOfWeek());
        System.out.println("getEra() " + today.getEra());
        ///
        System.out.println();
        System.out.println("Сегодня = " + today);
        System.out.println("plusDays() " + today.plusDays(1));
        System.out.println("plusWeeks() " + today.plusWeeks(1));
        System.out.println("plusMonths() " + today.plusMonths(1));
        System.out.println("plusYears() " + today.plusYears(1));
        System.out.println("minusDays() " + today.minusDays(1));
        System.out.println("minusWeeks() " + today.minusWeeks(1));
        System.out.println("minusMonths() " + today.minusMonths(1));
        System.out.println("minusYears() " + today.minusYears(1));
        System.out.println();
    }
}