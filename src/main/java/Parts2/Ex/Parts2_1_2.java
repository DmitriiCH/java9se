package Parts2.Ex;

import java.util.ArrayList;

public class Parts2_1_2 {

    public static void main(String[] args) {
        ArrayList<String> friends = new ArrayList<>();
        friends.add("Peter");
        ArrayList<String> people = friends;
        people.add("Paul");
        System.out.println(people.size() + " " + friends.size());
    }
}
