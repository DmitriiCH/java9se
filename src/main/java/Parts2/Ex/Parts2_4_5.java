package Parts2.Ex;

import java.text.NumberFormat;

public class Parts2_4_5 {

    public static void main(String[] args) {
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
        NumberFormat percentFormatter = NumberFormat.getPercentInstance();
        double x = 0.1;
        System.out.println(currencyFormatter.format(x));
        /////////////////////////////////////////////////
        System.out.println(percentFormatter.format(x));
    }
}
