package Parts2.Ex;

import java.util.ArrayList;

public class Parts2_3_1 {

    private String name;
    private double salary;

    public Parts2_3_1(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    public static void main(String[] args) {
        Parts2_3_1 parts = new Parts2_3_1("James Bond", 500000);
        //////
        ArrayList<Parts2_3_1> list = new ArrayList<>();
        list.add(new Parts2_3_1("James Bond", 500000));
    }
}
