package Parts2.Ex;

public class Parts2_4_1 {
    private static int lastId = 0;
    private int id;

    public Parts2_4_1(){
        lastId++;
        id = lastId;
    }
    public static void main(String[] args){
        System.out.println(Parts2_4_1.lastId);
        Parts2_4_1 parts = new Parts2_4_1();
        System.out.println(parts.id);
        System.out.println(Parts2_4_1.lastId);
    }
}
