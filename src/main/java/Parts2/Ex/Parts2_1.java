package Parts2.Ex;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class Parts2_1 {

    public static void main(String[] args) {
        int year = 2020;
        int month = 9;
        LocalDate date = LocalDate.of(year, month, 21);
        System.out.println(date.getDayOfMonth());
        date = date.plusDays(1);
        System.out.println(date.getDayOfMonth());
        //////////////////////////////
//        while (date.getMonthValue() == month){
//            System.out.printf("%4d", date.getDayOfMonth());
//            date = date.plusDays(1);
//        }
        //////////////////////////////
        DayOfWeek weekday = date.getDayOfWeek();
        //////////////////////////////
        int value = weekday.getValue();
//        for (int i = 0; i < value; i++) {
//            System.out.print(" ");
//        }
    }
}
