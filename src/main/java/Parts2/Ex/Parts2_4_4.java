package Parts2.Ex;

import java.util.Random;

public class Parts2_4_4 {

    private static Random generator = new Random();

    public static void main(String[] args) {
        int x = 2;
        int a = 4;
        double result = Math.pow(2, 4);
        System.out.println(result);
        //////////////////////////////
        int dieToss = Parts2_4_4.nextInt(generator, 1, 6);
        System.out.println(dieToss);
    }

    public static int nextInt(Random generator, int low, int high) {
        return low + generator.nextInt(high - low + 1);
    }

    public static int nextInt(int low, int high) {
        return low + generator.nextInt(high - low + 1);
    }
}
