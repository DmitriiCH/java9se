package Parts2.Ex;

import java.util.Random;

public class Parts2_2_6 {
    private Random generator;

    public void giveRandomRaise(Parts2_2_5 parts){
        double percentage = 10 * generator.nextGaussian();
        parts.reiseSalary(percentage);
    }

    public void increaseRandomly(double x){
        double amount = x * generator.nextDouble();
        x += amount;
    }
}
