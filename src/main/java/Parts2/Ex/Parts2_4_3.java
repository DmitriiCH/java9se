package Parts2.Ex;

import java.time.LocalDate;
import java.util.ArrayList;

public class Parts2_4_3 {

    private static final ArrayList<Integer> expirationYear = new ArrayList<>();

    static {
        int year = LocalDate.now().getYear();
        for (int i = year; i <= year + 20; i++) {
            expirationYear.add(i);
        }
    }

    public static void main(String[] args) {
        for (int i : expirationYear) {
            System.out.println(i);
        }
    }
}
