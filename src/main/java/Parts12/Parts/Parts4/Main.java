package Parts12.Parts.Parts4;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Comparator;
import java.util.Locale;

/*
Напишите программу, выполняющую такие же функции, как и команда cal в Unix,
выводящая календарь на текущий месяц. Например, по команде java Cal 3 2013
должен быть выведен календарь на март 2013 года, где 1 марта приходится на пятницу,
как показано ниже. (Обозначьте каким-нибудь образом в календаре конец каждой недели.)
 */
public class Main {

    public static void main(String[] args) {
        Calendar calendar = new Calendar(2021, 6);
        Calendar calendar1 = new Calendar();
        //calendar1.printDays();
        calendar.printDays();
    }
}
