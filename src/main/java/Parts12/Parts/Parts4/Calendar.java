package Parts12.Parts.Parts4;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Locale;

public class Calendar {
    private int space = 6;
    private int month;
    private LocalDate date;

    public Calendar(int year, int month) {
        date = LocalDate.of(year, month, 1);
        this.month = month;
    }

    public Calendar() {
        LocalDate dateTemp = LocalDate.now();
        date = LocalDate.of(dateTemp.getYear(), dateTemp.getMonth(), 1);
        month = dateTemp.getMonth().getValue();
    }

    private void printFirstDayMonth() {
        System.out.print(DayOfWeek.MONDAY.getDisplayName(TextStyle.SHORT, Locale.ENGLISH) + "   ");
        System.out.printf("%-" + 3 + "s   ", DayOfWeek.TUESDAY.getDisplayName(TextStyle.SHORT, Locale.ENGLISH));
        System.out.printf("%-" + 3 + "s   ", DayOfWeek.WEDNESDAY.getDisplayName(TextStyle.SHORT, Locale.ENGLISH));
        System.out.printf("%-" + 3 + "s   ", DayOfWeek.THURSDAY.getDisplayName(TextStyle.SHORT, Locale.ENGLISH));
        System.out.printf("%-" + 3 + "s   ", DayOfWeek.FRIDAY.getDisplayName(TextStyle.SHORT, Locale.ENGLISH));
        System.out.printf("%-" + 3 + "s   ", DayOfWeek.SATURDAY.getDisplayName(TextStyle.SHORT, Locale.ENGLISH));
        System.out.printf("%-" + 3 + "s   \n", DayOfWeek.SUNDAY.getDisplayName(TextStyle.SHORT, Locale.ENGLISH));
        System.out.print(" ");
        System.out.printf(("%" + space * (date.getDayOfWeek().getValue() - 1) + "s"), date.getDayOfMonth());
        System.out.printf("%-" + (space - 1) + "s", " ");
        if (date.getDayOfWeek().getValue() == 7) System.out.println();
    }

    private void print() {
        System.out.printf("%-" + space + "s", date.getDayOfMonth());
    }


    public void printDays() {
        printFirstDayMonth();
        while (true) {
            date = date.plusDays(1);
            if (date.getMonth().getValue() != month) break;
            if (date.getDayOfWeek().getValue() == 7) {
                print();
                System.out.println();
            } else {
                print();
            }
        }
    }

}
