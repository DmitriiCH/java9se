package Parts12.Parts.Parts5;

import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.DAYS;

/*
Напишите программу, выводящую кол-во дней, которые вы прожили.
 */
public class Main {

    public static void main(String[] args) {
        LocalDate myDay = LocalDate.of(1993, 07, 21);
        LocalDate fatherDay = LocalDate.of(1952, 04, 15);
        LocalDate motherDay = LocalDate.of(1955, 06, 06);
        System.out.println(getDays(myDay));
        System.out.println(getDays(fatherDay));
        System.out.println(getDays(motherDay));
    }

    public static Long getDays(LocalDate happyBirthday) {
        LocalDate currentDate = LocalDate.now();
        return DAYS.between(happyBirthday, currentDate);
    }
}
