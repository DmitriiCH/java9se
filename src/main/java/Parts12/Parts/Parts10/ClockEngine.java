package Parts12.Parts.Parts10;

import java.time.*;
import java.util.HashMap;
import java.util.Map;

public class ClockEngine {

    private Map<String, String> cityZone;

    public ClockEngine() {
        cityZone = new HashMap<>();
        cityZone.put("Лос-Анджелес", "America/Los_Angeles");
        cityZone.put("Франкфурт", "Europe/Monaco");
    }

    /*
    параметр startCity - город вылета
    параметр startTime - время вылета
    параметр finishCity - город назначения
    параметр timeTravel - время в пути
     */
    public String finishCityTime(String startCity, LocalTime startTime, String finishCity, LocalTime timeTravel) {
        ZonedDateTime startZone = ZonedDateTime.of(LocalDate.now(), startTime, ZoneId.of(cityZone.get(startCity)));
        ZonedDateTime finishTime = startZone.plusHours(timeTravel.getHour()).
                plusMinutes(timeTravel.getMinute()).withZoneSameInstant(ZoneId.of(cityZone.get(finishCity)));
        return finishTime.getHour() + ":" + finishTime.getMinute();
    }

    /*
    параметр startCity - город вылета
    параметр startTime - время вылета
    параметр finishCity - город назначения
    параметр timeTravel - время в пути
     */
    public String timeTravel(String startCity, LocalTime startTime, String finishCity, LocalTime finisTime) {
        LocalDate localDate = LocalDate.now();
        ZonedDateTime startZone = ZonedDateTime.of(localDate, startTime, ZoneId.of(cityZone.get(startCity)));
        ZonedDateTime finishTime = ZonedDateTime.of(localDate, finisTime, ZoneId.of(cityZone.get(finishCity)));
        Long hours = Duration.between(startZone, finishTime).toHours();
        Long minutes = Duration.between(startZone, finishTime).toMinutes() - hours * 60;
        return hours + ":" + minutes;
    }
}
