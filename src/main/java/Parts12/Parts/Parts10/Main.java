package Parts12.Parts.Parts10;

import java.time.LocalTime;
import java.time.ZoneId;

/*
Самолет авиарейсом из Лос-Анджелеса во Франкфурт вылетает в 3:05 по полудни местного
времени и находится в полете 10 часов 50 минут. Когда он прибывает во Франкфурт?
Напишите программу способную выполнять подобные расчеты времени.
 */
public class Main {
    public static void main(String[] args) {
        ClockEngine engine = new ClockEngine();
        String time = engine.finishCityTime("Лос-Анджелес", LocalTime.of(3, 5), "Франкфурт", LocalTime.of(10, 50));
        System.out.println("Вы прибудете в место назначения Франкфурт в " + time + " по местному времени");
    }
}
