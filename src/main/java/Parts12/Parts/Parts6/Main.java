package Parts12.Parts.Parts6;

import java.time.DayOfWeek;
import java.time.LocalDate;

/*
Выведите перечень всех пятниц, которые пришлись на 13-е число в XX веке.
 */
public class Main {

    public static void main(String[] args) {
        LocalDate localDate = LocalDate.of(1901, 1, 13);
        printFriday(localDate);
    }

    private static void printFriday(LocalDate date) {
        while (date.getYear() < 2000) {
            if (date.getDayOfWeek().equals(DayOfWeek.FRIDAY)) {
                System.out.println(date);
            }
            date = date.plusMonths(1);
        }
    }
}
