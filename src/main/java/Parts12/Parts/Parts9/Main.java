package Parts12.Parts.Parts9;

import java.time.*;

/*
Используя снова потоковые операции, выявите все часовые пояса, смещения которых не кратны полному часу.
 */
public class Main {

    public static void main(String[] args) {
        LocalDateTime localDateTime = LocalDateTime.now();
        ZoneId.getAvailableZoneIds().stream().filter(zone ->
                checkHour(ZonedDateTime.of(localDateTime, ZoneId.of(zone)).getOffset().toString())).forEach(t ->
                System.out.println(ZonedDateTime.of(localDateTime, ZoneId.of(t)).getOffset() + " " + t));

    }

    private static boolean checkHour(String time) {
        if (time.equals("Z")) return false;
        return !time.substring(4).equals("00");
    }
}
