package Parts12.Parts.Parts1;

import java.time.LocalDate;

/*
Рассчитайте дату, на которую приходится День программиста, не пользуясь методом plusDays().
 */
public class Main {
    public static void main(String[] args) {
        //с помощью метода ofYearDay() можно получить дату, имея только номер года и номер дня года
        LocalDate programmerDate = LocalDate.ofYearDay(2021, 256);
        System.out.println(programmerDate.getMonth() + " " + programmerDate.getDayOfMonth() + " " + programmerDate.getDayOfWeek());
        System.out.println(programmerDate);
    }
}
