package Parts12.Parts.Parts7;


/*
Реализуйте класс TimeInternal, представляющий промежуток времени, пригодный для
календарных событий (например, совещаний, которые должны проходить в назначенный день
с 10:00 до 11:00) Предоставьте метод, чтобы проверить, не перекрываются ли два проме-
жутка времени.
 */
public class Main {

    public static void main(String[] args) {
        TimeInternal tm = new TimeInternal();
        tm.setDate(12, 10, 0, 11, 00);
//        tm.setDate(12, 11, 20, 11, 30);
        System.out.println(tm.intersection(12, 11, 30, 12, 0));
    }
}
