package Parts12.Parts.Parts2;

import java.time.LocalDate;

/*
Что произойдет, если добавить один год, четыре года, четыре раза по одному году
при вызове метода LocalDate.of(2000, 2, 29)?

Каждый раз при вызове метода plusYears(Long) нам возвращается новый объект класса
LocalDate с новым значением года. Класс LocalDate является неизменяемым.
 */
public class Main {
    public static void main(String[] args) {
        LocalDate date = LocalDate.of(2000, 2, 29);
        date.plusYears(1);
        System.out.println(date);
        System.out.println(date.plusYears(4));
    }
}
