package Parts12.Parts.Parts11;

import Parts12.Parts.Parts10.ClockEngine;

import java.time.LocalTime;

/*
Самолет обратным авиарейсом из Франкфурта вылетает в 14:05 и прибывает в
Лос-Анджелес в 16:40. Сколько времени длится полет? Напишите программу,
способную выполнять подобные расчеты времени.
 */
public class Main {
    public static void main(String[] args) {
        ClockEngine engine = new ClockEngine();
        String time = engine.timeTravel("Франкфурт", LocalTime.of(14, 5), "Лос-Анджелес", LocalTime.of(16, 40));
        System.out.println("Ваш путь из Франкфурт в Лос-Анджелес будет длиться " + time + " часов");
    }
}