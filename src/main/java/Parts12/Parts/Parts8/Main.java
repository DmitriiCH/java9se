package Parts12.Parts.Parts8;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;

/*
Получите смещения текущей даты во всех поддерживаемых часовых поясах для
текущего момента времени, преобразовав результат вызова метода ZoneId.getAvailableZoneIds()
в поток данных и используя потоковые операции.
 */
public class Main {

    public static void main(String[] args) {
        LocalDateTime localDateTime = LocalDateTime.now();
        ZoneId.getAvailableZoneIds().forEach(t -> System.out.println(ZonedDateTime.of(localDateTime, ZoneId.of(t))));
    }
}
