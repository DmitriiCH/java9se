package Parts8.Parts.Parts18;

import java.util.ArrayList;
import java.util.stream.Stream;

/*
Каким образом можно исключить получение смежных дубликатов из потока
данных? Сможет ли написанный вами метод обрабатывать параллельный по-
ток?

Исключить получение смежных дубликатов из потока данных можно с помощью
метода distinct(). Параллельный поток может его обработать, но выгоды
в этом нет, т.к. в потоке исполнения, обрабатывающем отдельный элемент,
неизвестно какие именно элементы следует отбросить, до тех пор, пока
сегмент не будет обработан. Что демонстирует пример ниже.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println(getTime(init().stream()));
        System.out.println(getTime(init().parallelStream()));
    }

    private static Long getTime(Stream<String> stream) {
        Long start = System.currentTimeMillis();
        stream.distinct().forEach(t-> t.length());
        return System.currentTimeMillis() - start;
    }

    private static ArrayList<String> init() {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 90; i++) {
            list.add(Integer.toString(i));
        }
        for (int i = 0; i < 90; i++) {
            list.add(String.valueOf(i));
        }
        return list;
    }
}
