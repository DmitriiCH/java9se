package Parts8.Parts.Parts3;

import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/*
Допустим, что имеется массив int[] values = {1, 4, 9, 16}. Каков результат вызова
Stream.of(values)? Как вместо этого получить поток данных типа int?

Результат вызова Stream.of(values), мы получаем поток данных, в котором данные
хранятся в оболочке Optional<T>. В библиотеке потоков данных имеются специализированные
классы IntStream, LongStream, DoubleStream, позволяющие сохранять значения примитивных типов
не прибегая к помощи оболочек.
 */
public class Main {
    public static void main(String[] args) {
        int[] values = {1, 4, 9, 16};

        IntStream stream = IntStream.of(values);
        OptionalInt max = stream.max(); //Аналогичен классу Optional, но у него есть специализированный метод getAsInt().
        int maxInt = max.getAsInt();
        System.out.println(maxInt);
    }
}
