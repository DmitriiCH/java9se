package Parts8.Parts.Parts7;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Преобразовав содержимое файла в поток лексем, выведите список первых 100
лексем, являющихся словами в том смысле, в каком они определены в предыдущем упражнении.
Прочитайте содержимое файла снова и выведите список из 10 наиболее часто употребляемых слов,
игнорируя регистр букв.

Лексемы (tokens) - это элементарные законченные слова языка.
 */
public class Main {

    public static void main(String[] args) throws IOException {
//        firstHundredWords("src\\main\\java\\Parts8\\Parts\\Parts7\\test.txt").forEach(System.out::println);
        tenOftenWords("src\\main\\java\\Parts8\\Parts\\Parts7\\test.txt").forEach((K, V) -> System.out.println(K + " " + V));
    }

    public static List<String> firstHundredWords(String paths) throws IOException {
        String content = new String(Files.readAllBytes(Paths.get(paths)));
        String[] strings = content.split("\\PL+");
        Stream<String> stream = Arrays.stream(strings);
        return stream.filter(Main::isWord).limit(100).collect(Collectors.toList());
    }

    public static Map<String, Integer> tenOftenWords(String paths) throws IOException {
        String content = new String(Files.readAllBytes(Paths.get(paths)));
        String[] strings = content.split("\\PL+");
        try (Stream<String> stream = Arrays.stream(strings)) {
            Comparator<String> comp = (str1, str2) -> {
                int x = countWord(str1, Arrays.stream(strings));
                int y = countWord(str2, Arrays.stream(strings));
                return y - x;
            };
            comp.thenComparing(String::compareTo);
            return stream.filter(Main::isWord).map(String::toLowerCase).sorted(comp).distinct().limit(10).
                    collect(Collectors.toMap(
                            (key) -> key,
                            (key) -> countWord(key, Arrays.stream(strings)),
                            (key1, key2) -> key1,
                            LinkedHashMap::new));
        }
    }

    private static int countWord(String str, Stream<String> stream) {
        int x = stream.reduce(0, (total1, string) -> {
            if (string.equalsIgnoreCase(str)) {
                total1++;
            }
            return total1;
        }, Integer::sum);
        stream.close();
        return x;
    }

    public static boolean isWord(String str) {
        return str.codePoints().allMatch(Character::isAlphabetic);//isAlphabetic(int) Проверяет, является ли символ символом алфавита
    }
}
