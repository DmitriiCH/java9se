package Parts8.Parts.Parts15;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/*
Организуйте вызов метода reduce() таким образом, чтобы вычислить
среднее в потоке данных Stream<Double>. Почему нельзя просто вычислить
сумму и разделить ее на результат, возвращаемый методом count()?

По заданию если вызвать count(), ты будет выведено число 1, так как после
reduce(Double::sum).stream() вернется поток, в котором будет одно число - сумма.
Пример - ниже.
Stream.of(1.55, 1.65, 1.75).reduce(Double::sum).stream().reduce(0).count()
 */
public class Main {

    public static void main(String[] args) {
        List<Double> list = Arrays.asList(1.55, 1.65, 0.75);
        double sum = list.stream().reduce(Double::sum).map(x -> x / list.size()).get();
        System.out.println(sum);
    }
}
