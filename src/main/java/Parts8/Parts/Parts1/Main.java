package Parts8.Parts.Parts1;

import java.util.Arrays;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Убедитесь, что запрос первых пяти длинных слов не требует вызова метода filter(),
если найдено пятое длинное слово. С этой целью просто организуйте протоко-
лирование вызова каждого метода.
 */
public class Main {

    private static Logger logger = myLogger();

    public static void main(String[] args) throws Exception {
        logger.info("Вызов main()");
        List<String> list = Arrays.asList("asdaddd", "adasdsadasd", "erewdsgdsg", "olpsdpomsvksp", "papsdklfdposcmdskap", "dasdassp[k[,mp");
        String element = list.stream().skip(4).findFirst().orElseThrow(() -> new Exception("Елемент не найден"));
//        List<String> list2 = list.stream().dropWhile(ss -> {
//            int count = 0;
//            if(ss.length() > 10){
//                count++;
//            };
//            return count == 5;
//        }).collect(Collectors.toList());
//        list2.forEach(s -> System.out.println(s));
        // если в потоке нет 5 элемента, прогрузим эксепшен
        if (!(element.length() > 10)) {
            logger.info("Вызов filter()");
            list = list.stream().filter(t -> t.length() > 10).collect(Collectors.toList());
        }
    }

    public static Logger myLogger() {
        Logger myLogger = Logger.getGlobal();
        myLogger.setLevel(Level.INFO);
        myLogger.setUseParentHandlers(false);
        ConsoleHandler handler = new ConsoleHandler();
        handler.setLevel(Level.INFO);
        myLogger.addHandler(handler);
        return myLogger;
    }
}
