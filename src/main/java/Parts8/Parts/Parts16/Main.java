package Parts8.Parts.Parts16;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

/*
Найдите 500 простых чисел с 50 десятичными цифрами, используя
параллельный поток данных типа BigInteger и метод
bigInteger.isProbablePrime(). Насколько это делается быстрее,
чем при использовании последовательного потока данных?

Десятичные цифры изображают литерами 0, 1, 2, 3, 4, 5, 6, 7, 8, 9.

Простое число — натуральное (целое положительное) число, имеющее ровно два
различных натуральных делителя — единицу и самого себя
 */
public class Main {
    public static void main(String[] args) {
        timeStream();
        timeParallelStream();
    }

    public static Long timeStream() {
        Long currentTimeStart = System.currentTimeMillis();
        ArrayList<BigInteger> list = init();
        list.stream().filter(bigInteger -> {
                    if (!(bigInteger.toString().length() == 50)) {
                        return false; // проверяем что длина 50 десятичных цифр
                    }
                    return bigInteger.isProbablePrime(1); // проверяем что число простое
                }
        ).limit(500).forEach(BigInteger::toString);
        Long currentTimeFinish = System.currentTimeMillis();
        System.out.printf("Последовательный стрим. Время выполнения - %s мс\n", (currentTimeFinish - currentTimeStart));
        return currentTimeFinish - currentTimeStart;
    }

    public static Long timeParallelStream() {
        Long currentTimeStart = System.currentTimeMillis();
        ArrayList<BigInteger> list = init();
        list.parallelStream().filter(bigInteger -> {
                    if (!(bigInteger.toString().length() == 50)) {
                        return false; // проверяем что длина 50 десятичных цифр
                    }
                    return bigInteger.isProbablePrime(1); // проверяем что число простое
                }
        ).limit(500).forEach(BigInteger::toString);
        Long currentTimeFinish = System.currentTimeMillis();
        System.out.printf("Параллельный стрим. Время выполнения - %s мс\n", (currentTimeFinish - currentTimeStart));
        return currentTimeFinish - currentTimeStart;
    }

    private static ArrayList<BigInteger> init() {
        ArrayList<BigInteger> list = new ArrayList<>();
        for (int i = 0; i < 100000; i++) {
            list.add(new BigInteger("12345678910111213141516171819202122232425262728293"));
            list.add(new BigInteger("12345678910111213141516171819202122232425262728293").add(new BigInteger(i + "")));
        }
        return list;
    }
}
