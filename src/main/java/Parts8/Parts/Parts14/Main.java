package Parts8.Parts.Parts14;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;

/*
Соедините все элементы в потоках данных Stream<ArrayList<T>> и ArrayList<T>.
Покажите, как добиться этого с помощью каждой из трех форм метода
reduce().
 */
public class Main {
    public static void main(String[] args) {
        ArrayList<String> list1 = new ArrayList<>(Arrays.asList("1", "2", "3"));
        ArrayList<String> list2 = new ArrayList<>(Arrays.asList("4","5","6"));
        Stream<ArrayList<String>> stream = Stream.of(list1, list2);
        /////
        ArrayList<String> list3 = new ArrayList<>(Arrays.asList("7", "8", "9"));
        //
        Stream<String> result = join(stream, list3);
        result.forEach(System.out::println);
    }

    public static <T> Stream<T> join(Stream<ArrayList<T>> stream, ArrayList<T> list) {
        Stream<T> listStream = list.stream();
        return stream.reduce(listStream, (stream1, list1) -> Stream.concat(stream1, list1.stream()), Stream::concat);
    }
}
