package Parts8.Parts.Parts17;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Найдите 500 самых длинных слов в романе "Война и Мир", используя параллельный поток данных.
Насколько это делается быстрее, чем при использовании последовательного потока данных?
 */
public class Main {

    public static void main(String[] args) throws IOException {
        System.out.println(theLongestWordParallelStream("./src/main/java/Parts8/Parts/Parts2/textVoinaMir.txt"));
        System.out.println(theLongestWordStream("./src/main/java/Parts8/Parts/Parts2/textVoinaMir.txt"));
    }

    public static Long theLongestWordParallelStream(String paths) throws IOException {
        String[] strings = new String(Files.readAllBytes(Paths.get(paths))).split("\\PL+");
        try (Stream<String> stream = Arrays.stream(strings).parallel()) {
            Long startTime = System.currentTimeMillis();
//          Сортируем по длинне, самые длинные слова в начало. distinct() - удаляем одни и те же слова, что бы найти самые длинные и
//          разные слова.  limit() - выставляем кол-во найденных слов. Так как в потоке действия выполняются непосредственно
//          при вызове объекта, то чтобы получить время выполнения, сохраняю результат в Map, для наглядного представления
//          результата.
            Map<String, Integer> map = stream.filter(Main::isWord).sorted((str1, str2) -> str2.length() - str1.length()).distinct().limit(500).
                    collect(Collectors.toMap(
                            (key) -> key,
                            String::length,
                            (key1, key2) -> key1,
                            HashMap::new));
            Long finishTime = System.currentTimeMillis();
//            map.forEach((K, V) -> System.out.println(K + " " + V + " букв"));
            return finishTime - startTime;
        }
    }

    public static Long theLongestWordStream(String paths) throws IOException {
        String[] strings = new String(Files.readAllBytes(Paths.get(paths))).split("\\PL+");
        try (Stream<String> stream = Arrays.stream(strings)) {
            Long startTime = System.currentTimeMillis();
//          Сортируем по длинне, самые длинные слова в начало. distinct() - удаляем одни и те же слова, что бы найти самые длинные и
//          разные слова.  limit() - выставляем кол-во найденных слов. Так как в потоке действия выполняются непосредственно
//          при вызове объекта, то чтобы получить время выполнения, сохраняю результат в Map, для наглядного представления
//          результата.
            Map<String, Integer> map = stream.filter(Main::isWord).sorted((str1, str2) -> str2.length() - str1.length()).distinct().limit(500).
                    collect(Collectors.toMap(
                            (key) -> key,
                            String::length,
                            (key1, key2) -> key1,
                            HashMap::new));
            Long finishTime = System.currentTimeMillis();
//            map.forEach((K, V) -> System.out.println(K + " " + V + " букв"));
            return finishTime - startTime;
        }
    }

    public static boolean isWord(String str) {
        return str.codePoints().allMatch(Character::isAlphabetic);
    }
}
