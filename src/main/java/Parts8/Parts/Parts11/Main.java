package Parts8.Parts.Parts11;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Определите все символьные строки максимальной длинны в задданном
конечном потоке символьных строк.
 */
public class Main {

    public static void main(String[] args) {
        maxLengthWord(Stream.of("Определите", "символьных", "символьные", "задданном", "длинны","dffdfdfdfdfdfdfdfdd")).forEach(System.out::println);
    }

    public static List<String> maxLengthWord(Stream<String> stream) {
        List<String> list = stream.collect(Collectors.toList());
        int maxLength = list.stream().reduce(0,
                (totalLength, str) -> {
                    if (totalLength < str.length()) {
                        totalLength = str.length();
                    }
                    return totalLength;
                }, Integer::compareTo);
        return list.stream().filter(str -> str.length() == maxLength).collect(Collectors.toList());
    }
}
