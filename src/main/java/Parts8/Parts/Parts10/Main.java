package Parts8.Parts.Parts10;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Определите среднюю длину строки в заданном конечном потоке символьных строк.
 */
public class Main {

    public static void main(String[] args) {
        System.out.println(averageLength(Stream.of("1", "22", "333","sdsdsds")));
    }

    public static int averageLength(Stream<String> stream) {
        List<String> list = stream.collect(Collectors.toList());
        int length = list.stream().reduce(0, (totalInt, str) -> totalInt += str.length(), Integer::sum);
        int countWord = (int) list.stream().count();
        return length / countWord;
    }
}
