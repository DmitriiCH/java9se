package Parts8.Parts.Parts6;

import java.util.Arrays;
import java.util.List;
import java.util.function.IntPredicate;
import java.util.function.Predicate;

/*
Воспользуйтесь методом String.codePoints() для реализации метода, проверяющего,
является ли симпвольная строка словом, состоящим только из букв.
(Подсказка: воспользуйтесь методом Character.isAlphabetic().) Реализуйте тем же самым способом
метод, проверяющий, является ли символьная строка достоверным в Java идентификатором.

Идентификаторы:
Имена переменных, классов, объектов, интерфейсов, методов называются идентификаторами. Названия идентификаторов выбираются по следующим правилам:
• они должны начинаться с буквы или символа подчеркивания;
• они могут содержать латинские буквы, символы подчеркивания или цифры без пробелов;
• названия идентификаторов не должны совпадать с ключевыми словами.
 */
public class Main {

    public static void main(String[] args) {
        String str = "является";
        //System.out.println(isWord(str));
        System.out.println(isIdentificator("_123456dfd"));
    }

    public static boolean isWord(String str) {
        return str.codePoints().allMatch(Character::isAlphabetic);
        //Character.isAlphabetic(int) Проверяет, является ли символ символом алфавита
    }

    public static boolean isIdentificator(String str) {
        if (str.isEmpty()) {
            return false;
        }
        int x = str.codePoints().findFirst().getAsInt();
        if (!Character.isAlphabetic(x)) {
            if (!Character.valueOf((char) x).equals('_')) {
                return false;
            }
        }
        if (str.codePoints().anyMatch(Character::isSpaceChar)) {
            //isSpaceChar() проверяем является ли символ пробелом, символом переноса строки или смены параграф
            return false;
        }
        List<String> keywords = Arrays.asList("abstract", "boolean", "break", "byte", "case", "catch", "char", "class", "const",
                "continue", "default", "do", "double", "else", "extends", "false", "final", "finally", "Float",
                "for", "goto", "if", "implements", "import", "instanceof", "int", "interface", "long", "native",
                "new", "null", "package", "private", "protected", "public", "return", "short", "static", "strictfp",
                "super", "switch", "synchronized", "this", "throw", "throws", "transient", "true", "try", "void", "volatile", //Все ключевые слова Java
                "while");
        if (keywords.contains(str.toLowerCase())) {
            return false;
        }
        return true;
    }
}
