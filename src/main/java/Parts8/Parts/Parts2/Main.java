package Parts8.Parts.Parts2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/*
Определите разность во времени подсчета длинных слов с помощью методов parallelStream() и stream().
Вызовите метод System.currentTimeMills() до и после этих методов и выведите разность. Если у вас
быстродействующий компьютер, выберите для подсчета длинных слов более длинный документ (например,
роман "Война и Мир").
 */
public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println(timeReadFileStream("./src/main/java/Parts8/Parts/Parts2/textVoinaMir.txt"));
        System.out.println(timeReadFileParallelStream("./src/main/java/Parts8/Parts/Parts2/textVoinaMir.txt"));
    }

    public static Long timeReadFileStream(String filePath) throws IOException {
        Long startTime = System.currentTimeMillis();
        String content = new String(Files.readAllBytes(Paths.get(filePath)));
        List<String> words = Arrays.asList(content.split("\\PL+"));
        //разбиваем строку на слова, небуквенные символы считаются разделителями.
        words.stream().filter(s -> s.length() > 10).count(); //слова больше 10 букв считаем длинными.
        Long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }

    public static Long timeReadFileParallelStream(String filePath) throws IOException {
        Long startTime = System.currentTimeMillis();
        String content = new String(Files.readAllBytes(Paths.get(filePath)));
        List<String> words = Arrays.asList(content.split("\\PL+"));
        //разбиваем строку на слова, небуквенные символы считаются разделителями.
        words.parallelStream().filter(s -> s.length() > 10).count(); //слова больше 10 букв считаем длинными.
        Long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }
}