package Parts8.Parts.Parts13;

import java.util.stream.Stream;

/*
Напишите метод public static <T> Stream <T> zip(Stream<T> first, Stream<T> second),
Изменяющий элементы из потоков данных first и second (или возвращающий пустое
значение null, если в потоке данных, черед которого настанет, исчерпываются элементы)
 */
public class Main {

    public static void main(String[] args) {
        zip(Stream.of(1, 2, 3, 4), Stream.of("1", "2")).forEach(System.out::println);
    }

    public static <T> Stream<T> zip(Stream<T> first, Stream<T> second) {
        Stream<T> stream = Stream.concat(first.map(t -> t = null), second.map(t -> t = null));
        // так как по заданию нужно изменить элементы из потоков Stream <T>, а тип неизвестен, то их обнуляю и объединяю потоки.
        return stream;
    }
}
