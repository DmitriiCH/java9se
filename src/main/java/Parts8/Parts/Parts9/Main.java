package Parts8.Parts.Parts9;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Прочитайте слова из файла в поток данных и получите массив всех слов, содержащих
пять отдельных гласных.
 */
public class Main {

    public static void main(String[] args) throws IOException {
        fiveWords("./src/main/java/Parts8/Parts/Parts2/textVoinaMir.txt").forEach(System.out::println);
    }

    public static List<String> fiveWords(String path) throws IOException {
        String[] words = new String(Files.readAllBytes(Paths.get(path))).split("\\PL+");
        return Arrays.stream(words).filter(Main::isWord).filter(Main::filter).distinct().collect(Collectors.toList());
    }

    private static boolean filter(String str) {
        char[] vowels = {'a', 'e', 'y', 'i', 'o'};
        //A», «E», «I», «O», «U», «Y
        Set<Character> set = new HashSet<>();
        for (char c : str.toLowerCase().toCharArray()) {
            if (contains(c, vowels) && !set.contains(c)) {
                set.add(c);
            }
        }
        ;
        return set.size() == 5;
    }

    private static boolean contains(char c, char[] chars) {
        for (char x : chars) {
            if (c == x) {
                return true;
            }
        }
        return false;
    }

    public static boolean isWord(String str) {
        return str.codePoints().allMatch(Character::isAlphabetic);
    }
}
