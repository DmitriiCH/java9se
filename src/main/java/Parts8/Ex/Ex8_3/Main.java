package Parts8.Ex.Ex8_3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        List<String> words = Arrays.asList("First", "Second", "Three", "Four", "Five", "Six");
        Stream<String> longWords = words.stream().filter(w -> w.length() > 12);
        //longWords.forEach(System.out::println);
        //
        Stream<String> lowercaseWords = words.stream().map(String::toLowerCase);
        //lowercaseWords.forEach(System.out::println);
        //
        Stream<String> firstLetters = words.stream().map(s -> s.substring(0, 1));
        //firstLetters.forEach(w -> System.out.println(w));
        //
        //
        Stream<String> result2 = codePoints("boat");
        //result2.forEach(w -> System.out.println(w));
        //
        Stream<Stream<String>> result = words.stream().map(w -> codePoints(w));
        //result.forEach(w -> System.out.println(w));
        //
        Stream<String> result1 = words.stream().flatMap(w -> codePoints(w));
        result1.forEach(w -> System.out.println(w));
    }

    public static Stream<String> codePoints(String s) {
        List<String> result = new ArrayList<>();
        int i = 0;
        while (i < s.length()) {
            int j = s.offsetByCodePoints(i, 1);
            result.add(s.substring(i, j));
            i = j;
        }
        return result.stream();
    }

}
