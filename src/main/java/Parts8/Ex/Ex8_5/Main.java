package Parts8.Ex.Ex8_5;

import java.util.Comparator;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        Stream<String> uniqueWords = Stream.of("merrily", "merrily", "merrily", "gently").distinct();
        //uniqueWords.forEach(s -> System.out.println(s));
        //
        Stream<String> longestFirst = Stream.of("merrily", "merrily", "merrily", "gentlyyy").distinct().sorted(Comparator.comparing(String::length).reversed());
        longestFirst.forEach(System.out::println);
        //
        Object[] powers = Stream.iterate(1.0, p -> p * 2).peek(e -> System.out.println("Fetching " + e)).limit(20).toArray();
    }
}
