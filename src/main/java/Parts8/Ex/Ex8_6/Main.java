package Parts8.Ex.Ex8_6;

import java.util.Optional;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        Optional<String> largest = Stream.of("merrily", "merrily", "merrily", "gently").distinct().max(String::compareToIgnoreCase);
        System.out.println("largest: " + largest.orElse(""));
        //
        Optional<String> startWithQ = Stream.of("Qmerrily", "merrily", "merrily", "gently").distinct().filter(s -> s.startsWith("Q")).findFirst();
        System.out.println(startWithQ.get());
        //
        Optional<String> startWithQ1 = Stream.of("Qmerrily", "merrily", "merrily", "Qgently").distinct().parallel().filter(s -> s.startsWith("Q")).findAny();
        System.out.println(startWithQ1.get());
        //
        boolean aWordsStartWithQ = Stream.of("Qmerrily", "merrily", "merrily", "Qgently").distinct().parallel().anyMatch(s -> s.startsWith("Q"));
        System.out.println(aWordsStartWithQ);
    }
}
