package Parts8.Ex.Ex8_1;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
        String contents =
                new String(Files.readAllBytes(Paths.get ("C:\\Users\\dmch1019\\IdeaProjects\\Java9SE\\src\\main\\java\\Parts8\\Ex\\Ex8_1\\text.txt")),
                        StandardCharsets. UTF_8);
        List<String> words = Arrays.asList(contents.split("\\PL+"));
        int count = 0;
        for (String w : words) {
            if(w.length() > 12 ) {
                count++;
            }
        }
        System.out.println(count);
        long count1 = words.stream().filter(w -> w.length() > 12).count();
        System.out.println(count1);
        long count2 = words.parallelStream().filter(w -> w.length() > 12).count();
        System.out.println(count2);
    }
}
