package Parts8.Ex.Ex8_7_3;

import java.util.Optional;

public class Main {

    public static void main(String[] args) {
        Optional<Double> optionalDouble = inverse(1.3);
        System.out.println(optionalDouble.get());
        System.out.println(optionalDouble.isPresent());
    }

    public static Optional<Double> inverse(Double d) {
        return d == 0 ? Optional.empty() : Optional.of(1 / d);
    }
}
