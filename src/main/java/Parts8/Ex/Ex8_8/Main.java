package Parts8.Ex.Ex8_8;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("Один", "Два", "Три");
        Stream<String> stream = list.stream();
        //метод toArray()
//        String[] result = stream.toArray(String[]::new);
//        System.out.println(Arrays.toString(result));
//        //метод collect()
//        List<String> resultList = stream.collect(Collectors.toList());
//        Set<String> resultSet = stream.collect(Collectors.toSet());
//        //Если требуется конкретная реализация множество, то
//        TreeSet<String> resultTreeSet = stream.collect(Collectors.toCollection(TreeSet::new));
//        //сцепление строк
//        String resultString = stream.collect(Collectors.joining());
//        System.out.println(resultString);
//        //
//        String resultString1 = stream.map(Object::toString).collect(Collectors.joining(","));
//        System.out.println(resultString1);
        //
        IntSummaryStatistics summary = stream.collect(Collectors.summarizingInt(String::length));
        double averageWordLength = summary.getAverage();
        double maxWordLength = summary.getMax();
        double minWordLength = summary.getMin();
        double countWordLength = summary.getCount();
        System.out.printf("getAverage() = %f, getMax() = %f, getMin() = %f, getCount() = %f",averageWordLength, maxWordLength, minWordLength, countWordLength);
    }
}
