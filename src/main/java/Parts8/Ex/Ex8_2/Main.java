package Parts8.Ex.Ex8_2;

import java.math.BigInteger;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        String contents = "Напишите программу для чтения всех слов из файла и вывода строк,\n" +
                "в которых каждое слово встречается в нем. Воспользуйтесь для этой\n" +
                "цели преобразованием из символьных строк в множества.";
        Stream<String> words = Stream.of(contents.split("\\PL+"));
        //
        Stream<String> song = Stream.of("gently", "down", "the", "stream");
        //
        Stream<String> silence = Stream.empty();
        //
        //Поток постоянных значений
        Stream<String> echos = Stream.generate(() -> "Echo");
        //Поток случайных чисел
        Stream<Double> randoms = Stream.generate(Math::random);
        //Бесконечная последовательность вроде 0 1 2 3 ...
        Stream<BigInteger> integers = Stream.iterate(BigInteger.ZERO, n -> n.add(BigInteger.ONE));
        //
        BigInteger limit = new BigInteger("10000000");
//        Stream<BigInteger> integers1 = Stream.iterate(BigInteger.ZERO,
//                n -> n.compareTo(limit) < 0,
//                n -> n.add(BigInteger.ONE));
    }
}
