package Parts8.Ex.Ex8_7_4;

import java.util.Optional;

public class Main {

    public static void main(String[] args) {
        Optional<Double> result = inverse(1.3).flatMap(Main::squareRoot);
        System.out.println(result.get());
        //
        Optional<Double> result1 = Optional.of(-4.0).flatMap(Main::inverse).flatMap(Main::squareRoot);
        System.out.println(result1.get());
    }

    public static Optional<Double> squareRoot(Double d) {
        return d < 0 ? Optional.empty() : Optional.of(Math.sqrt(d));
    }

    public static Optional<Double> inverse(Double d) {
        return d == 0 ? Optional.empty() : Optional.of(1 / d);
    }
}
