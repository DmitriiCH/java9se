package Parts8.Ex.Ex8_9;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        List<Person> list = Arrays.asList(new Person(1, "Вася"), new Person(2, "Петя"), new Person(3, "Толя"));
        Stream<Person> people = list.stream();
        Map<Integer, String> map = people.collect(Collectors.toMap(Person::getId, Person::getName));
        //
        Map<Integer, Person> map1 = people.collect(Collectors.toMap(Person::getId, Function.identity()));
        //
        //
        //
        Stream<Locale> locales = Stream.of(Locale.getAvailableLocales());
        Map<String, String> lamguageNames = locales.collect(Collectors.toMap(Locale::getDisplayLanguage, loc -> loc.getDisplayLanguage(loc), (existingValue,
                newValue) -> existingValue));
        //
        Map<String, Set<String>> countryLanguageSets = locales.collect(Collectors.toMap(
                Locale::getDisplayLanguage,
                loc -> Collections.singleton(loc.getDisplayLanguage()),
                (a, b) -> {
                    Set<String> union = new HashSet<>();
                    union.addAll(b);
                    return union;}));
        //
        Map<Integer, Person> idToPerson = people.collect(
                Collectors.toMap(
                        Person::getId,
                        Function.identity(),
                        (existingValue, newValue) ->
                        {throw  new IllegalStateException();},
                        TreeMap::new));
    }
}
