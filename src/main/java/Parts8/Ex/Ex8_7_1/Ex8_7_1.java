package Parts8.Ex.Ex8_7_1;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class Ex8_7_1 {

    public static void main(String[] args) {
        Optional<String> optionalString = null;
        String result = optionalString.orElse("");
        //
        result = optionalString.orElseGet(() -> System.getProperty("meapp.default"));
        //
        result = optionalString.orElseThrow(IllegalStateException::new);
        //
        //Optional<String> resultOptional = optionalString.or(() -> Optional.ofNullable(System.getProperty("myapp.default")));
        Set<String> results = new HashSet<>();
        optionalString.ifPresent(v -> results.add(v));
        optionalString.ifPresent(results::add);
        //
        Optional<Boolean> added = optionalString.map(results::add);

    }
}
