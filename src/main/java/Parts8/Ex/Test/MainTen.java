package Parts8.Ex.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainTen {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("iPhone 8", "HTC U12", "Huawei Nexus 6P",
                "Samsung Galaxy S9", "LG G6", "Xiaomi MI6", "ASUS Zenfone 2",
                "Sony Xperia Z5", "Meizu Pro 6", "Lenovo S850");
        //toList()
        //
        List<String> filtersPhone = list.stream().filter(s -> s.length() > 10).collect(Collectors.toList());
        filtersPhone.forEach(System.out::println);
        //toSet()
        //
        Set<String> filtersPhone1 = list.stream().filter(s -> s.length() < 10).collect(Collectors.toSet());
        filtersPhone1.forEach(s -> System.out.println(s));
        //to Map()
        //
        Stream<Phone> stream = Stream.of(new Phone("iPhone 8", 54000),
                new Phone("Nokia 9", 45000),
                new Phone("Samsung Galaxy S9", 40000),
                new Phone("LG G6", 32000));
        Map<String, Integer> map = stream.collect(Collectors.toMap(p -> p.getName(), p -> p.getPrice()));
        map.forEach((k, v) -> System.out.println(k + " " + v));
        //toCollection()
        //
        Stream<String> phones = Stream.of("iPhone 8", "HTC U12", "Huawei Nexus 6P",
                "Samsung Galaxy S9", "LG G6", "Xiaomi MI6", "ASUS Zenfone 2",
                "Sony Xperia Z5", "Meizu Pro 6", "Lenovo S850");
        HashSet<String> hashSet = phones.collect(Collectors.toCollection(HashSet::new));
    }
}
