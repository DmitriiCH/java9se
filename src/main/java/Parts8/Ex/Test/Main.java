package Parts8.Ex.Test;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("Dependency", "From", "Manual Task One", "To", "SyncPoint", "Type", "Finish to Start", "Lag", "0");
        String tooltip = list.get(0);
        list = list.subList(1,list.size());
        list.forEach(System.out::println);
        System.out.println("\n" + tooltip);
    }
}
