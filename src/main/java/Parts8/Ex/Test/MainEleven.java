package Parts8.Ex.Test;

import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainEleven {

    public static void main(String[] args) {
        Stream<Phone2> phone2Stream = Stream.of(new Phone2("iPhone X", "Apple", 600),
                new Phone2("Pixel 2", "Google", 500),
                new Phone2("iPhone 8", "Apple",450),
                new Phone2("Galaxy S9", "Samsung", 440),
                new Phone2("Galaxy S8", "Samsung", 340));

        Map<String, List<Phone2>> map = phone2Stream.collect(Collectors.groupingBy(Phone2::getCompany));
        for (Map.Entry<String, List<Phone2>> pair : map.entrySet()){
            System.out.println(pair.getKey());
            for (Phone2 phone : pair.getValue()){
                System.out.println(phone.getName());
            }
            System.out.println();
        }
    }
}
