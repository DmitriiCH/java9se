package Parts8.Ex.Test;

import java.util.stream.Stream;

public class MainThree {

    public static void main(String[] args) {
        //
        //filter()
        //
        Stream<String> stream = Stream.of("Париж", "Лондон", "Мадрид","Берлин", "Брюссель");
        //stream.filter(s -> s.length() == 6).forEach(System.out::println);
        ////////////////////////////////
        //Для фильтрации элементов в потоке применяется метод filter(), который представляет промежуточную операцию.
        //
        Stream<Phone> phoneStream = Stream.of(new Phone("iPhone 6 S", 54000), new Phone("Lumia 950", 45000),
                new Phone("Samsung Galaxy S 6", 40000));
        //phoneStream.filter(s -> s.getPrice() < 50000).forEach(s -> System.out.println(s.getName()));
        //
        //map()
        //Отображение или маппинг позволяет задать функцию преобразования одного объекта в другой, то есть получить из элемента одного типа элемент другого типа.
        //phoneStream.map(s -> s.getName()).filter(s -> s.startsWith("iPhone")).forEach(System.out::println);
        //
        //flatMap()
        //Плоское отображение выполняется тогда, когда из одного элемента нужно получить несколько.
        phoneStream.flatMap(p -> Stream.of(
                String.format("название: %s  цена без скидки: %d",p.getName(), p.getPrice()),
                String.format("название: %s  цена без скидки: %d", p.getName(), p.getPrice() - (int)(p.getPrice() * 0.1))))
                .forEach(System.out::println);
    }
}
