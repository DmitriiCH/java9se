package Parts8.Ex.Test;

import java.util.stream.Stream;

public class MainFive {

    public static void main(String[] args) {
        //
        //Метод takeWhile() выбирает из потока элементы, пока они соответствуют условию.
        //Если попадается элемент, который не соответствует условию, то метод завершает свою работу. Выбранные элементы возвращаются в виде потока.
        //
//        Stream<Integer> stream = Stream.of(-3, -2, -1, 0, 1, 2, 3, -4, -5);
//        stream.takeWhile(s -> s > 0)
//                .forEach(o -> System.out.println(o));
        //
        //Метод dropWhile() выполняет обратную задачу - он пропускает элементы потока,
        // которые соответствуют условию до тех пор, пока не встретит элемент, который НЕ соответствует условию:
        //
//        Stream<Integer> stream1 = Stream.of(-3, -2, -1, 0, 1, 2, 3, -4, -5);
//        stream1.dropWhile(n -> n < 0)
//                .forEach(s - > System.out.println(s));
        //
        //Статический метод concat() объединяет элементы двух потоков, возвращая объединенный поток:
        //
        Stream<String> people1 = Stream.of("Tom", "Bob", "Sam");
        Stream<String> people2 = Stream.of("Alice", "Kate", "Sam");
        Stream.concat(people1,people2).forEach(s -> System.out.println(s));
    }
}
