package Parts8.Ex.Test;

import java.util.stream.Stream;

public class MainSix {

    public static void main(String[] args) {
        //
        //Метод limit(long n) применяется для выборки первых n элементов потоков. Этот метод также возвращает модифицированный поток, в котором не более n
        // элементов.
        //
        Stream<String> stream1 = Stream.of("iPhone 6 S", "Lumia 950", "Samsung Galaxy S 6", "LG G 4", "Nexus 7");
        stream1.limit(3).forEach(s -> System.out.println(s));
        System.out.println();
        //
        //Метод skip(long n) используется для пропуска n элементов. Этот метод возвращает новый поток, в котором пропущены первые n элементов.
        //
        Stream<String> stream2 = Stream.of("iPhone 6 S", "Lumia 950", "Samsung Galaxy S 6", "LG G 4", "Nexus 7");
        stream2.skip(2).forEach(System.out::println);
    }
}
