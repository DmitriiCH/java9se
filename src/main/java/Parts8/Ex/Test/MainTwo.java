package Parts8.Ex.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class MainTwo {

    public static void main(String[] args) {
        List<String> cities = new ArrayList<>();
        Collections.addAll(cities, "Париж", "Лондон", "Мадрид");
        //cities.stream().filter(s -> s.length() == 6).forEach(s -> System.out.println(s));
        //
        Stream<String> citiesStream = Arrays.stream(new String[]{"Париж", "Лондон", "Мадрид"});
        citiesStream.forEach(System.out::println);
        //
        IntStream intStream1 = Arrays.stream(new int[]{1,2,3,4,5,6});
        IntStream intStream2 = IntStream.of(1,2,3,4,5,6);
        //
        DoubleStream doubleStream1 = Arrays.stream(new double[]{1.2,1.3,1.4,1.5,1.6});
        DoubleStream doubleStream2 = DoubleStream.of(1.2,1.3,1.4,1.5,1.6);
        //
        LongStream longStream1 = Arrays.stream(new long[]{100,200,300,400,500,600});
        LongStream longStream2 = LongStream.of(100,200,300,400,500,600);
    }
}
