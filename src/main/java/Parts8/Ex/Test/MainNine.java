package Parts8.Ex.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class MainNine {

    public static void main(String[] args) {
        //
        //Ряд операций сведения, такие как min, max, reduce, возвращают объект Optional<T>. Этот объект фактически обертывает результат операции. После
        // выполнения операции с помощью метода get() объекта Optional мы можем получить его значение.
        //
        List<Integer> list = new ArrayList<>();
        Optional<Integer> optionalInteger = list.stream().min(Integer::compareTo);
        if(optionalInteger.isPresent()){
            System.out.println(optionalInteger.get());
        }
        //Метод ifPresent() определяет действия со значением в Optional, если значение имеется.
        //
        optionalInteger.ifPresent(System.out::println);
        //Метод ifPresentOrElse() позволяет определить альтернативную логику на случай, если значение в Optional отсутствует.
        //
        //optionalInteger.ifPresentOrElse(v -> System.out.println(v), () -> System.out.println("Value not found");
        //Метод orElse() позволяет определить альтернативное значение, которое будет возвращаться, если Optional не получит из потока какого-нибудь значения.
        //
        System.out.println(optionalInteger.orElse(-1));
        //Метод orElseGet() позволяет задать функцию, которая будет возвращать значение по умолчанию.
        //
        System.out.println(optionalInteger.orElseGet(() -> new Random().nextInt(10)));
        //Еще один метод - orElseThrow позволяет сгенерировать исключение, если Optional не содержит значения.
        //
        System.out.println(optionalInteger.orElseThrow(IllegalStateException::new));
    }
}
