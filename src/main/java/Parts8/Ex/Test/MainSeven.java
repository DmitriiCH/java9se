package Parts8.Ex.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class MainSeven {

    public static void main(String[] args) {
        //
        //Метод count() возвращает количество элементов в потоке данных:
        List<String> list1 = new ArrayList<>();
        list1.addAll(Arrays.asList(new String[] {"Tom", "Sam", "Bob", "Alice"}));
        long count = list1.stream().count();
        System.out.println(count);
        //
        //Метод findFirst() извлекает из потока первый элемент, а findAny() извлекает случайный объект из потока (нередко так же первый):
        List<String> list2 = Arrays.asList("Tom", "Sam", "Bob", "Alice");
        Optional<String> optional1 = list2.stream().findFirst();
        System.out.println(optional1.get());
        //
        Optional<String> optional2 = list2.stream().findAny();
        System.out.println(optional2.get());
        //
        //allMatch, anyMatch, noneMatch
        //boolean allMatch(Predicate<? super T> predicate): возвращает true, если все элементы потока удовлетворяют условию в предикате
        //
        System.out.println(list2.stream().allMatch(s -> s.length() >= 3));
        //boolean anyMatch(Predicate<? super T> predicate): возвращает true, если хоть один элемент потока удовлетворяют условию в предикате
        //
        System.out.println(list2.stream().anyMatch(s -> s.length() == 3));
        //boolean noneMatch(Predicate<? super T> predicate): возвращает true, если ни один из элементов в потоке не удовлетворяет условию в предикате
        //
        System.out.println(list2.stream().noneMatch(s -> s == "Bobbi"));
        //
        //Методы min() и max() возвращают соответственно минимальное и максимальное значение.
        //
        List<Integer> list = Arrays.asList(1,2,3,4,5,6,7,8,9);

        Optional<Integer> min = list.stream().min(Integer::compareTo);
        System.out.println(min.get());
        Optional<Integer> max = list.stream().max(Integer::compareTo);
        System.out.println(max.get());
    }
}
