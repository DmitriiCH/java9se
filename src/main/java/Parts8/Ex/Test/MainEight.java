package Parts8.Ex.Test;

import java.util.Optional;
import java.util.stream.Stream;

public class MainEight {

    public static void main(String[] args) {
        //
        //Метод reduce выполняет терминальные операции сведения, возвращая некоторое значение - результат операции.
        //
        Stream<Integer> stream = Stream.of(1,2,3,4,5,6);
        Optional<Integer> optionalInteger = stream.reduce((x,y) -> x * y);
        System.out.println(optionalInteger.get());
    }
}
