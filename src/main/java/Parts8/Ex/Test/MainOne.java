package Parts8.Ex.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainOne {

    public static void main(String[] args) {
        long count = Stream.of(-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5).filter(i -> i >= 0).count();
        List<Integer> intValues = Stream.of(-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5).filter(i -> i >= 0).collect(Collectors.toList());
        System.out.println(count + "\n");
        intValues.forEach(System.out::println);
    }
}
