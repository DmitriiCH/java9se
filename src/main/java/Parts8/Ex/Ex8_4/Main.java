package Parts8.Ex.Ex8_4;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        Stream<Double> randoms = Stream.generate(Math::random).limit(10);
        //randoms.forEach(System.out::println);
        //
        Stream<Double> randoms1 = Stream.generate(Math::random).limit(10).skip(6);
        //randoms1.forEach(System.out::println);
        //
        Stream<String> combined = Stream.concat(codePoints("Hello"), codePoints("World"));
        combined.forEach(s -> System.out.println(s));
    }

    public static Stream<String> codePoints(String s) {
        List<String> result = new ArrayList<>();
        int i = 0;
        while (i < s.length()) {
            int j = s.offsetByCodePoints(i, 1);
            result.add(s.substring(i, j));
            i = j;
        }
        return result.stream();
    }
}
