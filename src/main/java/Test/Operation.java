package Test;


public class Operation {

    public static void main(String[] args) {
        inspectService(SimpleService.class);
        inspectService(LazyService.class);
        inspectService(String.class);
    }

    public static void inspectService(Class<?> service) {
        if (service.isAnnotationPresent(Service.class)) {
            System.out.println(service.getAnnotation(Service.class).name());
        } else {
            System.out.println("null");
        }
    }
}
