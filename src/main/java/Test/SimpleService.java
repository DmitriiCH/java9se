package Test;

import java.lang.annotation.Documented;

@Service(name = "SimpleService")
public class SimpleService {

    @Init
    public void initService() {
        System.out.println("initService");
    }

    public void myMethod() {
        System.out.println("myMethod");
    }
}
