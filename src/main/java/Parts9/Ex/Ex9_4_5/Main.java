package Parts9.Ex.Ex9_4_5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        String input = "1, 2, 3";
        Pattern pattern = Pattern.compile("\\s*, \\s*");
        String[] tokens = pattern.split(input);
        System.out.println(Arrays.toString(tokens));
        //
        Stream<String> stream = pattern.splitAsStream(input);
        stream.forEach(System.out::println);
        //
        String[] strings = input.split("\\s*, \\s*");
        System.out.println(Arrays.toString(strings));
        //
        Scanner scanner = new Scanner(new File("src\\main\\java\\Parts9\\Ex\\Ex9_2_3\\createTempFile3164958122691834934.txt"));
        scanner.useDelimiter("\\s*, \\s*");
//        Stream<String> stream1 = scanner.tokens();
    }
}
