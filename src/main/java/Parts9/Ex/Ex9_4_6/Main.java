package Parts9.Ex.Ex9_4_6;

import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        String result = "3:45".replaceAll("(\\d{1,2}):(?<minutes>\\d{2})","$1 hours and ${minutes} minutes");
        System.out.println(result);
        //
        //String result2 = Pattern.compile("pL{4,}").matcher("Mary had a little lamb").replaceAll(m -> m.group().toUpperCase());

    }
}
