package Parts9.Ex.Ex9_2_4;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) throws IOException {
//        try(Stream<Path> entries = Files.list(Paths.get("C:\\Users"))) {
//            entries.forEach(System.out::println);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        //
        try (Stream<Path> entries = Files.walk(Paths.get("C:\\Users"))) {
            entries.forEach(s -> System.out.println(s));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //В следующем фрагменте кода метод Files.walk() применяется для копирования одного каталога в другой
        Path sourse = Paths.get("C:\\Users");
        Path target = Paths.get("C:\\Users");
        Files.walk(sourse).forEach(p -> {
            try {
                Path q = target.resolve(sourse.relativize(p));
                if (Files.isDirectory(p)) {
                    Files.createDirectory(q);
                } else Files.copy(p, q);
            } catch (IOException ex) {
                throw new UncheckedIOException(ex);
            }
        });
    }
}
