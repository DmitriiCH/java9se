package Parts9.Ex.Ex9_1_8;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Main {

    public static void main(String[] args) throws IOException {

        RandomAccessFile file = new RandomAccessFile("src\\main\\java\\Parts9\\Ex\\Ex9_1_8\\test","rw");
        int value = file.readInt();
        file.seek(file.getFilePointer() - 4);
        file.writeInt(value + 2);
    }
}
