package Parts9.Ex.Ex9_4_3;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("\\w{5}");
        Matcher matcher = pattern.matcher("Start End");
        while (matcher.find()) {
            System.out.println(matcher.start() + " " + matcher.group() + " " + matcher.end());
        }
//        List<String> matchesList = pattern.matcher("Start End")
//                                    .results()
//                                    .map(Matcher::group)
//                                    .collect(Collectors.toList());
    }
}
