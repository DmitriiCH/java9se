package Parts9.Ex.Ex9_1_1;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) throws IOException {
        //InputStream in = Files.newInputStream(Paths.get(""));
        //OutputStream out = Files.newOutputStream(Paths.get(""));
        //
        URL url = new URL("http://horstmann.com/index.html");
        InputStream inURL = url.openStream();
        //
        //
        byte[] bytes = new byte[]{1,2,3,4,5,6,7,8,9,10};
        InputStream inputStream = new ByteArrayInputStream(bytes);
        while (inputStream.available() != 0) {
            System.out.println(inputStream.read());
        }
        System.out.println(new String(bytes, StandardCharsets.UTF_8));
        //
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] outBytes = outputStream.toByteArray();
    }
}
