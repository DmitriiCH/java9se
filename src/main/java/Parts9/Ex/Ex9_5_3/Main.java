package Parts9.Ex.Ex9_5_3;

import javafx.geometry.Point2D;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Path path = Paths.get("src\\main\\java\\Parts9\\Ex\\Ex9_5_3\\test");
        Employee employee = new Employee("Peter", 10.0);
        employee.point = new Point2D(1,2);
        //////
        ObjectOutputStream out = new ObjectOutputStream(Files.newOutputStream(path));
        out.writeObject(employee);
        out.close();
        //////
        ObjectInputStream in = new ObjectInputStream(Files.newInputStream(path));
        Employee em1 = (Employee) in.readObject();
        in.close();
        System.out.println(em1);
    }
}
