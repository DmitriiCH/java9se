package Parts9.Ex.Ex9_5_3;

import javafx.geometry.Point2D;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Employee implements Serializable {

    private String name;
    private double salary;
    protected transient Point2D point;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        System.out.println("Начал работу метод readObject()");
        in.defaultReadObject();
        double x = in.readDouble();
        double y = in.readDouble();
        point = new Point2D(x, y);
        System.out.println("Закончил работу метод readObject()");
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        System.out.println("Начал работу метод writeObject()");
        out.defaultWriteObject();
        out.writeDouble(point.getX());
        out.writeDouble(point.getY());
        System.out.println("Закончил работу метод writeObject()");
    }

    @Override
    public String toString() {
        return getClass() + "{ name=\'" + getName() +
                "\', salary=\'" + getSalary() + "\'}";
    }
}
