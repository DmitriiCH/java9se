package Parts9.Ex.Ex9_1_2;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) throws IOException {
        //InputStream in = Files.newInputStream("");
        //int b = in.read();
        /*
        классе InputStream имеется приведенный ниже метод для ввода одного байта.
        Этот метод возвращает байт в виде целочисленного значения в пределах от 0 до 255,
        а по достижении конца вводимых данных — значение -1.
         */

        //Читаем все байты из потока данных в байтовый массив
        //byte[] bytes1 = in.readAllBytes();
        //
        //Если из файла требуется прочитать все байты, то рекомендуется вызвать следующий вспомогательный метод
        byte[] bytes = Files.readAllBytes(Paths.get(""));
        //
        //
        byte[] bytes2 = new byte[10];
        //int bytesRead = in.readNBytes(bytes2, offset, n);
    }
}
