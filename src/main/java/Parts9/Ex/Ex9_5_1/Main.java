package Parts9.Ex.Ex9_5_1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Employee peter = new Employee("Peter", 90000);
        Employee paul = new Employee("Paul", 180000);
        Path path = Paths.get("src\\main\\java\\Parts9\\Ex\\Ex9_5_1\\test");
        ObjectOutputStream out = new ObjectOutputStream(Files.newOutputStream(path));
        out.writeObject(peter);//запись обьекта в файл
        out.writeObject(paul);//запись обьекта в файл
        out.close();
        /////////////////////
        ObjectInputStream in = new ObjectInputStream(Files.newInputStream(path));
        Employee e1 = (Employee) in.readObject();//восстановление обьекта из файла
        Employee e2 = (Employee) in.readObject();//восстановление обьекта из файла
        in.close();
        System.out.println("e1 " + e1);
        System.out.println("e1 " + e2);
        //////
        /*
        Чтобы предотвратить сериализацию переменной экземпляра, достаточно пометить ее модификатором доступа transient
         */
    }
}
