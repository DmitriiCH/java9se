package Parts9.Ex.Ex9_2_1;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) {
        Path absolute = Paths.get("/", "home", "cay");
        Path relative = Paths.get("myapp", "conf", "user.properties");
        Path homeDirectory = Paths.get("/home/cay");
    }
}
