package Parts9.Ex.Ex9_1_4;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class Main {

    public static void main(String[] args) throws UnsupportedEncodingException {
        Charset utf_8 = StandardCharsets.UTF_8;
        Charset utf_16 = StandardCharsets.UTF_16;
        Charset utf_16be = StandardCharsets.UTF_16BE;
        Charset utf_16le =StandardCharsets.UTF_16LE;
        Charset iso_8859_1 = StandardCharsets.ISO_8859_1;
        Charset us_ascii = StandardCharsets.US_ASCII;

        Charset shiftJIS = Charset.forName("Shift-JIS");

        //превратить массив байтов в символьную строку
        byte[] bytes = new byte[10];
        String str = new String(bytes, StandardCharsets.UTF_8);
        String str2 = new String(bytes, "Shift-JIS");
    }
}
