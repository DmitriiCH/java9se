package Parts9.Ex.Ex9_2_2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) throws IOException {
        /*
        Чтобы создать новый каталог, достаточно сделать следующий вызов
         */
        //Files.createDirectory(path);
        /*
        Все составляющие заданного пути, кроме последней, должны непременно существовать
         */
        //Files.createDirectory(Paths.get("C:\\Users\\dmch1019\\IdeaProjects\\Java9SE\\src\\main\\java\\Parts9\\Ex\\Ex9_2_3"));
        /*
        Чтобы создать и промежуточные каталоги, достаточно сделать такой вызов
         */
        //Files.createDirectories(Paths.get("C:\\Users\\dmch1019\\"));
        //
        //При вызове метода Files. exists (path) проверяется, существует ли заданный файл или каталог
        System.out.println(Files.exists(Paths.get("C:\\Users\\dmch1019\\IdeaProjects\\Java9SE\\src\\main\\java\\Parts9\\Ex\\Ex9_2_3")));

        /*
        Чтобы выяснить, является ли проверяемый объект каталогом или обычным файлом
        (т.е. он содержит данные, а не является каталогом или символической ссылкой),
        следует вызвать статический метод isDirectory () или isRegular File () из класса Files.
         */
        System.out.println(Files.isDirectory(Paths.get("C:\\Users\\dmch1019\\IdeaProjects\\Java9SE\\src\\main\\java\\Parts9\\Ex\\Ex9_2_3")));
        System.out.println(Files.isRegularFile(Paths.get("C:\\Users\\dmch1019\\IdeaProjects\\Java9SE\\src\\main\\java\\Parts9\\Ex\\Ex9_2_3")));

        /*
        Для создания временного файла или каталога в заданном или характерном для системы месте имеются соответствующие служебные методы
        Path tempFile = Files.createTempFile(dir, prefix, suffix);
        Path tempFile = Files.createTempFile(prefix, suffix);
        Path tempDir = Files.createTempFile(dir, prefix);
        Path tempDir = Files.createTempFile(prefix);
         */
        Path tempFile = Files.createTempFile(Paths.get("C:\\Users\\dmch1019\\IdeaProjects\\Java9SE\\src\\main\\java\\Parts9\\Ex\\Ex9_2_3"), "createTempFile", ".txt");
    }
}
