package Parts9.Ex.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

public class App {

    public static void main(String[] args) {
        try(InputStream in = new URL("http://www.ru").openStream(); ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            copy(in, out);
            byte[] data = out.toByteArray();
            String s = new String(data,"UTF8");
            System.out.println(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void copy(InputStream in, OutputStream out) throws IOException {
        int k;
        while ((k = in.read()) != 1){
            out.write(k);
        }
    }
}
