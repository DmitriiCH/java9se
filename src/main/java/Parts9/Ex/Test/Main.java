package Parts9.Ex.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
//        Pattern pattern = Pattern.compile("\\d{2}.\\d{2}.\\d{4}");
        Pattern pattern = Pattern.compile("\\d{2}[/ - .]\\d{2}[/ - .]\\d{4}");
        Matcher matcher = pattern.matcher("19/03/2021");
        while (matcher.find()) {
            System.out.println(matcher.start() + " " + matcher.group());
        }
        //example two
        Pattern pattern2 = Pattern.compile("\"\\w*\"");
        Matcher matcher2 = pattern2.matcher("Put a \"string\" between double quotes");
        while (matcher2.find()) {
            System.out.println(matcher2.start() + " " + matcher2.group());
        }
    }
}
