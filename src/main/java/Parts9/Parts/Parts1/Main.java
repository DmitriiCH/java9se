package Parts9.Parts.Parts1;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;

/*
Напишите служебный метод для копирования всего содержимого из потока
ввода InputStream в поток вывода OutputStream, не пользуясь временными файлами.
Предоставьте другое решение без цикла, используя методы из класса Files и временный файл.
 */
public class Main {

    public static void main(String[] args) throws IOException {
        String str = "testStr";
        ByteArrayOutputStream out = new ByteArrayOutputStream();
//        myCopy(new BufferedInputStream(new ByteArrayInputStream(str.getBytes())), out);
        myCopy2(new BufferedInputStream(new ByteArrayInputStream(str.getBytes())), out);
        System.out.println(out.toString());
    }
//    public static void myCopy(InputStream in, OutputStream out) throws IOException {
//        byte[] bytes = in.readAllBytes();
//        in.close();
//        out.write(bytes);
//        out.close();
//    }

    public static void myCopy2(InputStream in, OutputStream out) throws IOException {
        Path tempPath = Files.createTempFile(Paths.get("src\\main\\java\\Parts9\\Parts\\Parts1"), "text", ".txt");
        Files.copy(in, tempPath, StandardCopyOption.REPLACE_EXISTING);//скопировал байты из потока InputStream во временный файл
        in.close();
        Files.copy(tempPath, out);//скопировал байты из временного файла в поток OutputStream
        out.close();
        new File(tempPath.toString()).delete();
    }
}
