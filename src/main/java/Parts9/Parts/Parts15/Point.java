package Parts9.Parts.Parts15;

import java.io.Serializable;

public class Point implements Serializable {

    private static final long serialVersionUID = 4178002573146959393L;

    private double[] coordinates = new double[2];

    public Point(double x, double y) {
        coordinates[0] = x;
        coordinates[1] = y;
    }

    public String toString() {
        return "Point{x= " + coordinates[0] + ", y= " + coordinates[1] + "}";
    }
}
