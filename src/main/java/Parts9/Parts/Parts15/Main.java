package Parts9.Parts.Parts15;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Arrays;

/*
Продолжите предыдущее упражнение, но измените представление данных типа Point таким образом,
чтобы хранить координаты точки в массиве. Что произойдет при попытке прочитать в новой версии файл,
сформированный в предыдущей версии? И что произойдет, если исправить идентификатор serialVersionUID?
Что бы вы сделали, если бы ваша жизнь зависела от создания новой версии, совместимой с прежней?

Когда происходит сериализация объекта, имя класса и его идентификатор serialVersionUID направляются в поток вывода.
Этот однозначный идентификатор присваивается разработчиком класса, определяющим следующую переменную экземпляра:
private static final long serialVersionUID = 1L; // Версия 1

Если не присвоить идентификатор serialVersionUID,
он автоматически генерируется хешированием канонического описания переменных экземпляра,
методов и супертипов.

При попытке прочитать в новой версии файл, возникнет ошибка с описанием:
java.io.InvalidClassException: Parts9.Parts.Parts14.Point;
local class incompatible: stream classdesc serialVersionUID = -9169813312266775697,
local class serialVersionUID = 7035487653141973345

При попытке восстановления обьекта из файла, если изменить serialVersionUID на такой же как был в момент запись обьекта в файл, то возникнет исключение при
восстановлении обьекта из файла - ClassCastException
serialver -classpath "./" Parts9.Parts.Parts14.Point
serialver -classpath . Parts9.Parts.Parts14.Point
Если же требуется прочитать экземпляры прежней версии класса и есть уверенносгь,
что это можно сделать надежно, то утилиту serialver следует выполнить с прежней версией класса и добавить полученный результат к новой его версии.
 */
public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        System.out.println(Arrays.toString(readInFile(new ObjectInputStream(new FileInputStream("src\\main\\java\\Parts9\\Parts\\Parts14\\test.txt")))));
    }

    public static Point[] readInFile(ObjectInputStream in) throws IOException, ClassNotFoundException {
        Point[] points = (Point[]) in.readObject();
        in.close();
        return points;
    }
}
