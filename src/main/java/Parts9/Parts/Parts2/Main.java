package Parts9.Parts.Parts2;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Напишите программу для чтения текстового файла и создания файла с таким же именем,
но с расширением . toc. Этот файл должен содержать список всех слов из входного файла,
а также список номеров строк, в которых встречается каждое слово. Имейте в виду,
что содержимое входного файла представлено в кодировке UTF-8.
 */
public class Main {

    public static void main(String[] args) throws IOException {
        Path path = Paths.get("src\\main\\java\\Parts9\\Parts\\Parts2\\text.txt");
        File file = new File((path.getParent() + "/" + path.getFileName()).replace("txt","toc"));
        file.createNewFile();
        Stream<String> stream = Arrays.stream(new String(Files.readAllBytes(path)).split("\\PL+")).distinct();
        writeToFile(file, getMap(stream, path));
    }

    public static void writeToFile(File file, Map<String, String> map) throws FileNotFoundException {
        PrintWriter printWriter = new PrintWriter(file);
        for (Map.Entry<String, String> pair : map.entrySet()) {
            printWriter.println(pair.getKey() + " " + pair.getValue());
        }
        printWriter.close();
    }

    public static Map<String, String> getMap(Stream<String> stream, Path path) {
        return stream.collect(Collectors.toMap(key -> key, (key) -> {
            try {
                return searchString(key,Files.readAllLines(path));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }));
    }

    private static String searchString(String str, List<String> lines) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < lines.size(); i++) {
            String[] s = lines.get(i).split("\\PL+");
            for (int j = 0; j < s.length; j++) {
                if(s[j].equalsIgnoreCase(str)) {
                    stringBuilder.append((i + 1) + ", ");
                    break;
                }
            }
        }
        return stringBuilder.substring(0,stringBuilder.length() - 2);
    }
}
