package Parts9.Parts.Parts13;

import java.io.*;

/*
Реализуйте метод для получения клона любого объекта сереализуемого
сначала в массив байтов и затем десериализируемого оттуда
 */
public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Human human = new Human("Толя", 10);
        Human clone = myClone(human);
        System.out.println(clone);
        System.out.println(clone.equals(human));
    }

    public static <T> T myClone(T object) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(byteArray);
        out.writeObject(object);
        out.close();
        ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(byteArray.toByteArray()));
        return (T) in.readObject();
    }
}
