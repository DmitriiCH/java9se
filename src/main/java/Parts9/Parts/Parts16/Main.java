package Parts9.Parts.Parts16;

import java.io.Externalizable;

/*
Какие классы из стандартной библиотеки Java реализуют интерфейс Externalizable
и в каких из них применяются методы writeReplace () и readResolve()?

Следующие классы из стандартной библиотеки Java реализуют интерфейс:
BitArray
DataFlavor
MimeType
Ser

Из них применяются методы writeReplace() и readResolve() в классе:
Ser
 */
public class Main {

    public static void main(String[] args) {
//        Externalizable
    }
}
