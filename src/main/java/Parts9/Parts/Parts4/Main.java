package Parts9.Parts.Parts4;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/*
Пользоваться классом Scanner удобно, но он действует чуть медленнее, чем класс BufferedReader.
Организуйте построчное чтение длинного файла, подсчитывая количество вводимых строк,
во-первых, с помощью класса Scanner и методов hasNextLine () и nextLine ();
во-вторых, с помощью класса BufferedReader и метода readLine();
а в-третьих, с помощью класса BufferedReader и метода lines ().
Каким из способов читать из файла быстрее и удобнее всего?
 */
public class Main {

    public static void main(String[] args) throws IOException {
        System.out.println("Work time method nextLine() - " + timeWorkScanner(Paths.get("./src/main/java/Parts8/Parts/Parts2/textVoinaMir.txt")) + "mc");
        System.out.println("Work time method readLine() - " + timeWorkBufferedReader("C:\\Users\\dmch1019\\IdeaProjects\\Java9SE\\src\\main\\java\\Parts8" +
                "\\Parts\\Parts2\\textVoinaMir.txt") + "mc");
        System.out.println("Work time method lines() - " + timeWorkBufferedReader2("C:\\Users\\dmch1019\\IdeaProjects\\Java9SE\\src\\main\\java\\Parts8" +
                "\\Parts\\Parts2\\textVoinaMir.txt") + "mc");
    }

    public static long timeWorkScanner(Path path) throws IOException {
        Long startTime = System.currentTimeMillis();
        Scanner scanner = new Scanner(path);
        int stringsCount = 0;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            stringsCount += 1;
        }
        System.out.println(stringsCount);
        return System.currentTimeMillis() - startTime;
    }

    public static long timeWorkBufferedReader(String path) throws IOException {
        Long startTime = System.currentTimeMillis();
        BufferedReader br = new BufferedReader(new FileReader(path));
        int count = 0;
        String line = "";
        while ((line = br.readLine()) != null) {
            count += 1;
        }
        System.out.println(count);
        return System.currentTimeMillis() - startTime;
    }

    public static long timeWorkBufferedReader2(String path) throws IOException {
        Long startTime = System.currentTimeMillis();
        BufferedReader br = new BufferedReader(new FileReader(path));
        long count = br.lines().count();
        System.out.println(count);
        return System.currentTimeMillis() - startTime;
    }
}
