package Parts9.Parts.Parts14;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.util.Arrays;

/*
Реализуйте сериализируемый класс Point с переменными экземпляра
для хранения координат точки х и у. Напишите одну программу
для сериализации массива объектов типа Point в файл,
а другую — для чтения из файла.
 */
public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        File file = new File("src\\main\\java\\Parts9\\Parts\\Parts14\\test.txt");
        file.createNewFile();
        Point[] points = new Point[]{new Point(12.01,1.3), new Point(545.26,26.03), new Point(12.65,8980.3), new Point(1.3,5.06)};
        writeToFile(points,new ObjectOutputStream(Files.newOutputStream(file.toPath())));
        Point[] point1 = readInFile(new ObjectInputStream(Files.newInputStream(file.toPath())));
        System.out.println(Arrays.toString(point1));
    }

    public static void writeToFile(Object o, ObjectOutputStream out) throws IOException {
        out.writeObject(o);
        out.close();
    }

    public static Point[] readInFile(ObjectInputStream in) throws IOException, ClassNotFoundException {
        Point[] points = (Point[]) in.readObject();
        in.close();
        return points;
    }
}
