package Parts9.Parts.Parts14;

import java.io.Serializable;

public class Point implements Serializable {

    private static final long serialVersionUID = 4178002573146959393L;

    private double x, y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public String toString() {
        return "Point{x= " + x + ", y= " + y + "}";
    }
}