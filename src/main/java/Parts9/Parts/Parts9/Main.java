package Parts9.Parts.Parts9;

//import sun.net.www.http.HttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.Map;

/*
Используя класс URLConnection, введите данные с веб-страницы, защищенной паролем
с элементарной аутентификацией. Соедините вместе имя пользователя, двоеточие,
пароль и определите кодировку Base64 полученного результата следующим обра-
зом:
String input = username + ":" + password;
String encoding = Base64.getEncoder().encodeToString(input.getBytes(StandardCharsets.UTF_8));
Установите в HTTP-заголовке Authorization значение "Basic " + encoding, а затем введите содер-
жимое страницы и выведите его на экран.

логин Dmch1019
пароль dmch10193
Base64 — стандарт кодирования двоичных данных при помощи только 64 символов ASCII
 */
public class Main {
    public static void main(String[] args) throws IOException {
        URL url = new URL("https://ru.wikipedia.org/wiki/Заглавная_страница");
        URLConnection connection = url.openConnection();
        String input = "Dmch1019:dmch10193";
        String encoding = Base64.getEncoder().encodeToString(input.getBytes(StandardCharsets.UTF_8));


        connection.setRequestProperty("Authorization","Basic " + encoding);
        connection.setDoOutput(true);
        Map<String, List<String>> headers = connection.getHeaderFields();

        InputStream in  = connection.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
        String result = "", sb;
        while ((sb = bufferedReader.readLine()) != null) {
            result += sb + "\n";
        }
        System.out.println(result);
    }
}
