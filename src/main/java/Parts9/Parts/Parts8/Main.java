package Parts9.Parts.Parts8;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipFile;

/*
Напишите служебный метод для создания архивного ZIP-файла,
содержащего все файлы из выбранного каталога и его подкаталогов.
 */
public class Main {

    public static void main(String[] args) throws URISyntaxException, IOException {
        createZip("src\\main\\java\\Parts9\\Parts\\Parts8\\TestFiles", "src\\main\\java\\Parts9\\Parts\\Parts8\\result.zip");
    }

    private static void createZip(String root, String zipPath) throws IOException, URISyntaxException {
        Path rootPath = Paths.get(root);
        Path zippath = Paths.get(zipPath);
        URI zip = new URI("jar", zippath.toUri().toString(), null);
        try (FileSystem zipf = FileSystems.newFileSystem(zip, Collections.singletonMap("create", "true"))) {
            Files.walk(rootPath).forEach(f -> {
                try {
                    String tempFile = replace(f.toString(), rootPath.getFileName().toString());
                    Files.copy(f, zipf.getPath("/" + tempFile), StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    //Чтобы в архиве дерево папок шло от нашей рутовой папки, удаляем все что до нее.
    private static String replace(String str, String root) {
        return str.substring(str.indexOf(root));
    }
}
