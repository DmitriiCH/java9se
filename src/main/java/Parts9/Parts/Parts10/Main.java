package Parts9.Parts.Parts10;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
Используя регулярное выражение, извлеките все десятичные целые числа (в том числе и отрицательные)
из символьной строки в списочный массив типа ArrayList<Integer>, используя во первых метод find(),
а во вторых - метод split(). Имейте в виду,  что знак + или -, после которого не следует цифра,
является разделителем.
 */
public class Main {
    public static void main(String[] args) {
        String str = "sald,msakjd[ahdqawd10[asd[-10[asda+10[sad[+d-10- 10";
        getListIntSplit(str).forEach(System.out::println);
        //getListIntFind(str).forEach(System.out::println);
    }

    public static List<Integer> getListIntFind(String str) {
        ArrayList<Integer> result = new ArrayList<>();
        Matcher math = Pattern.compile("[+-]?\\d{1,2}").matcher(str);
        while (math.find()) {
            result.add(Integer.parseInt(math.group())); // +10 парсится в 10.
        }
        return result;
    }

    public static List<Integer> getListIntSplit(String str) {
        String[] numbers = str.split("([A-Za-z]|[,\\]\\[ ';<>()^%$#@!]|[-+]\\D)+");
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i < numbers.length; i++) {
            list.add(Integer.parseInt(numbers[i]));
        }
        return list;
    }
}
