package Parts9.Parts.Parts11;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
Используя регулярные выражения, извлеките имена каталогов
(в виде массива символьных строк),имя и расширение файла
из абсолютного или относительного пути вроде /home/cay/myfile.txt
 */
public class Main {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(nameFolders("src/main/java/Parts9/Parts/Parts11/myfile.txt")));
        System.out.println(Arrays.toString(nameFolders("C:/Users/dmch1019/IdeaProjects/Java9SE/src/main/java/Parts9/Parts/Parts11/myfile.txt")));
    }

    public static String[] nameFolders(String path) {
        return path.split(":?\\/");
    }
}
