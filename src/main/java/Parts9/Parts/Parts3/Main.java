package Parts9.Parts.Parts3;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.*;
import java.util.Arrays;

/*
Напишите программу для чтения тестового файла, содержащего слова в
основном на английском языке, и определения типа кодировки:
ASCII, ISO 8859-1, UTF-8 или UTF-16. Если это кодировка UTF-16, то
программа должна определить порядок следования байтов.

ASCII - диапозон букв: 65 - 90 и 97 - 122
UTF-16 - диапозон букв: 1040 - 1103
UTF-8 - диапозон букв: 208 144 - 209 143
ISO-8859-1 - диапозон: 160 - 247;
 */
public class Main {

    public static void main(String[] args) throws IOException {
        System.out.println(detected(Paths.get(".\\src\\main\\java\\Parts9\\Parts\\Parts3\\UTF16.txt")));
        System.out.println(detected(Paths.get(".\\src\\main\\java\\Parts9\\Parts\\Parts3\\ASCII.txt")));
        System.out.println(detected(Paths.get(".\\src\\main\\java\\Parts9\\Parts\\Parts3\\UTF8.txt")));
    }

    public static String detected(Path path) throws IOException {
        Reader in = new InputStreamReader(Files.newInputStream(path));
        while (in.ready()) {
            int x = in.read();
            if (isUTF16(x)) {
                return "Кодировка UTF16\n" + Arrays.toString(Files.readAllBytes(path));
            }
            if (isUTF8(x)) {
                return "Кодировка UTF8";
            }
            if (isISO(x)) {
                return "ISO-8859-1";
            }

            if (isASCII(x)) {
                return "ISO-8859-1";
            }
        }
        return "Кодировка неопределена";
    }

    private static boolean isISO(int x) {
        if (x >= 160 && x <= 247) {
            return true;
        } else return false;
    }

    private static boolean isUTF8(int x) {
        if (x >= 208144 && x <= 209143) {
            return true;
        } else return false;
    }

    private static boolean isASCII(int x) {
        if (x >= 65 && x <= 90 || x >= 97 && x <= 122) {
            return true;
        } else return false;
    }

    private static boolean isUTF16(int x) {
        if (x >= 1040 && x <= 1103) {
            return true;
        } else return false;
    }
}