import Parts2.Parts.Parts5.Point;
import junit.framework.TestCase;
import org.junit.Assert;

public class TestPointEx5 extends TestCase {

    public void testTranslate() {
        Point point1 = new Point();
        point1 = point1.translate(1, 3);
        Assert.assertEquals(1.0, point1.getX(), 0.01);
        Assert.assertEquals(3.0, point1.getY(), 0.01);
        ///
        Point point2 = new Point(3, 4);
        point2 = point2.translate(1, 3);
        Assert.assertEquals(4.0, point2.getX(), 0.01);
        Assert.assertEquals(7.0, point2.getY(), 0.01);
    }

    public void testScale() {
        Point point1 = new Point();
        point1 = point1.scale(0.5);
        Assert.assertEquals(0.0, point1.getX(), 0.01);
        Assert.assertEquals(0.0, point1.getY(), 0.01);
        ///
        Point point2 = new Point(3, 4);
        point2 = point2.scale(0.5);
        Assert.assertEquals(1.5, point2.getX(), 0.01);
        Assert.assertEquals(2.0, point2.getY(), 0.01);
    }
}